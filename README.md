# README #

This is the AI War mod for [Starsector](http://fractalsoftworks.com), based on the 4X game [AI War](http://arcengames.com/ai-war/) by Arcen Games.

Current release version: v0.3.1b

### Setup instructions ###
Check out the repo to Starsector/mods/AI War (or some other folder name) and it can be played immediately. 

Create a project with /jars/src as a source folder to compile the Java files.

### License ###
AI War graphical assets [used with permission](http://www.arcengames.com/forums/index.php/topic,13666.0.html).

Music is copyrighted to [Pablo Vega](http://arcenmusic.bandcamp.com), all rights reserved.

All code is licensed under the [MIT License (Expat License version)](https://opensource.org/licenses/MIT) except where otherwise specified; see MITlicense for details.

All other assets are released as [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/) unless otherwise specified.