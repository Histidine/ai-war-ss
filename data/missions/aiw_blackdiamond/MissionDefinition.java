package data.missions.aiw_blackdiamond;

import com.fs.starfarer.api.campaign.CargoAPI.CrewXPLevel;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;

public class MissionDefinition implements MissionDefinitionPlugin {

	public void defineMission(MissionDefinitionAPI api) {

		// Set up the fleets so we can add ships and fighter wings to them.
		api.initFleet(FleetSide.PLAYER, "SIS", FleetGoal.ATTACK, false, 3);
		api.initFleet(FleetSide.ENEMY, "TTS", FleetGoal.ATTACK, true, 5);

		// Set a small blurb for each fleet that shows up on the mission detail and
		// mission results screens to identify each side.
		api.setFleetTagline(FleetSide.PLAYER, "Imperial Spire battleship Carbonado");
		api.setFleetTagline(FleetSide.ENEMY, "Tri-Tachyon Detachment");
		
		// These show up as items in the bulleted list under 
		// "Tactical Objectives" on the mission detail screen
		api.addBriefingItem("Defeat all enemy forces");
		api.addBriefingItem("Both you and the enemy flagship captain are highly skilled");
		api.addBriefingItem("The SIS Carbonado must survive");
		
		// Set up the player's fleet.  Variant names come from the
		// files in data/variants and data/variants/fighters
		FleetMemberAPI flagship = api.addToFleet(FleetSide.PLAYER, "spire_battleship_Assault", FleetMemberType.SHIP, "SIS Carbonado", true, CrewXPLevel.ELITE);
		
		api.defeatOnShipLoss("SIS Carbonado");

		// Set up the enemy fleet.
		FleetMemberAPI flagship2 = api.addToFleet(FleetSide.ENEMY, "paragon_Elite", FleetMemberType.SHIP, "TTS Infinity", false, CrewXPLevel.ELITE);
		api.addToFleet(FleetSide.ENEMY, "astral_Elite", FleetMemberType.SHIP, false, CrewXPLevel.VETERAN);
		api.addToFleet(FleetSide.ENEMY, "apogee_Balanced", FleetMemberType.SHIP, false, CrewXPLevel.VETERAN);
		api.addToFleet(FleetSide.ENEMY, "doom_Strike", FleetMemberType.SHIP, false, CrewXPLevel.VETERAN);
		api.addToFleet(FleetSide.ENEMY, "medusa_CS", FleetMemberType.SHIP, false, CrewXPLevel.VETERAN);
		api.addToFleet(FleetSide.ENEMY, "medusa_PD", FleetMemberType.SHIP, false, CrewXPLevel.VETERAN);
		api.addToFleet(FleetSide.ENEMY, "scarab_Experimental", FleetMemberType.SHIP, false, CrewXPLevel.ELITE);
		api.addToFleet(FleetSide.ENEMY, "tempest_Attack", FleetMemberType.SHIP, false, CrewXPLevel.VETERAN);
		api.addToFleet(FleetSide.ENEMY, "afflictor_Strike", FleetMemberType.SHIP, false, CrewXPLevel.VETERAN);
		api.addToFleet(FleetSide.ENEMY, "wolf_Assault", FleetMemberType.SHIP, false, CrewXPLevel.VETERAN);
		api.addToFleet(FleetSide.ENEMY, "wolf_CS", FleetMemberType.SHIP, false, CrewXPLevel.VETERAN);
		api.addToFleet(FleetSide.ENEMY, "xyphos_wing", FleetMemberType.FIGHTER_WING, false, CrewXPLevel.ELITE);
		api.addToFleet(FleetSide.ENEMY, "xyphos_wing", FleetMemberType.FIGHTER_WING, false, CrewXPLevel.ELITE);
		api.addToFleet(FleetSide.ENEMY, "wasp_wing", FleetMemberType.FIGHTER_WING, false, CrewXPLevel.VETERAN);
		api.addToFleet(FleetSide.ENEMY, "wasp_wing", FleetMemberType.FIGHTER_WING, false, CrewXPLevel.VETERAN);
		api.addToFleet(FleetSide.ENEMY, "wasp_wing", FleetMemberType.FIGHTER_WING, false, CrewXPLevel.VETERAN);
		api.addToFleet(FleetSide.ENEMY, "trident_wing", FleetMemberType.FIGHTER_WING, false, CrewXPLevel.VETERAN);
		api.addToFleet(FleetSide.ENEMY, "trident_wing", FleetMemberType.FIGHTER_WING, false, CrewXPLevel.VETERAN);
		
		// skills
		PersonAPI captain = flagship.getCaptain();
		//captain.getStats().setAptitudeLevel("combat", 5f);
		captain.getStats().setSkillLevel("missile_specialization", 5f);
		captain.getStats().setSkillLevel("ordnance_expert", 5f);
		captain.getStats().setSkillLevel("damage_control", 5f);
		captain.getStats().setSkillLevel("target_analysis", 5f);
		//captain.getStats().setSkillLevel("evasive_action", 5f);
		captain.getStats().setSkillLevel("helmsmanship", 5f);
		captain.getStats().setSkillLevel("flux_modulation", 5f);
		
		PersonAPI captain2 = flagship2.getCaptain();
		//captain2.getStats().setAptitudeLevel("combat", 5f);
		captain2.getStats().setSkillLevel("missile_specialization", 5f);
		captain2.getStats().setSkillLevel("ordnance_expert", 5f);
		captain2.getStats().setSkillLevel("damage_control", 5f);
		captain2.getStats().setSkillLevel("target_analysis", 5f);
		//captain2.getStats().setSkillLevel("evasive_action", 5f);
		captain2.getStats().setSkillLevel("helmsmanship", 5f);
		captain2.getStats().setSkillLevel("flux_modulation", 5f);
		
		// Set up the map.
		float width = 20000f;
		float height = 24000f;
		api.initMap((float)-width/2f, (float)width/2f, (float)-height/2f, (float)height/2f);
		
		float minX = -width/2;
		float minY = -height/2;
		
		// All the addXXX methods take a pair of coordinates followed by data for
		// whatever object is being added.
		
		// Add two big nebula clouds
		api.addNebula(minX + width * 0.65f, minY + height * 0.4f, 1600);
		api.addNebula(minX + width * 0.35f, minY + height * 0.65f, 1200);
		
		// And a few random ones to spice up the playing field.
		// A similar approach can be used to randomize everything
		// else, including fleet composition.
		for (int i = 0; i < 12; i++) {
			float x = (float) Math.random() * width - width/2;
			float y = (float) Math.random() * height - height/2;
			float radius = 200f + (float) Math.random() * 600f; 
			api.addNebula(x, y, radius);
		}
		
		// Add objectives
		api.addObjective(minX + width * 0.5f, minY + height * 0.35f, "sensor_array");
		//api.addObjective(minX + width * 0.7f, minY + height * 0.6f, "nav_buoy");
		
		// Add asteroid field
		api.addAsteroidField(minX, minY + height * 0.5f, 15, height, 30f, 90f, 180);
		
		// Add planet
		//api.addPlanet(0, 0, 400f, "water", 350f, true);
	}

}
