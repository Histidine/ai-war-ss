package data.missions.aiw_contact;

import com.fs.starfarer.api.campaign.CargoAPI.CrewXPLevel;
import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;

public class MissionDefinition implements MissionDefinitionPlugin {

	public void defineMission(MissionDefinitionAPI api) {

		// Set up the fleets so we can add ships and fighter wings to them.
		// In this scenario, the fleets are attacking each other, but
		// in other scenarios, a fleet may be defending or trying to escape
		api.initFleet(FleetSide.PLAYER, "SIS", FleetGoal.ATTACK, false, 5);
		api.initFleet(FleetSide.ENEMY, "SDS", FleetGoal.ATTACK, true);

		// Set a small blurb for each fleet that shows up on the mission detail and
		// mission results screens to identify each side.
		api.setFleetTagline(FleetSide.PLAYER, "Imperial Spire patrol");
		api.setFleetTagline(FleetSide.ENEMY, "Sindrian Diktat force");
		
		// These show up as items in the bulleted list under 
		// "Tactical Objectives" on the mission detail screen
		api.addBriefingItem("Defeat all enemy forces");
		
		// Set up the player's fleet.  Variant names come from the
		// files in data/variants and data/variants/fighters
		api.addToFleet(FleetSide.PLAYER, "spire_destroyer_Support", FleetMemberType.SHIP, "SIS Jade", true, CrewXPLevel.VETERAN);
		api.addToFleet(FleetSide.PLAYER, "spire_destroyer_Support", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "spire_frigate_Standard", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "spire_frigate_Skirmisher", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "spire_frigate_Skirmisher", FleetMemberType.SHIP, false);
		
		// Set up the enemy fleet.
		api.addToFleet(FleetSide.ENEMY, "eagle_Balanced", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "venture_Balanced", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "enforcer_Assault", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "mule_Standard", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "wolf_CS", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "vigilance_Standard", FleetMemberType.SHIP, false);
		
                api.addToFleet(FleetSide.ENEMY, "broadsword_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "talon_wing", FleetMemberType.FIGHTER_WING, false);
		
		// Set up the map.
		float width = 18000f;
		float height = 12000f;
		api.initMap((float)-width/2f, (float)width/2f, (float)-height/2f, (float)height/2f);
		
		float minX = -width/2;
		float minY = -height/2;
		
		// All the addXXX methods take a pair of coordinates followed by data for
		// whatever object is being added.
		
		// Add two big nebula clouds
		api.addNebula(minX + width * 0.75f, minY + height * 0.5f, 2000);
		api.addNebula(minX + width * 0.25f, minY + height * 0.5f, 1000);
		
		// And a few random ones to spice up the playing field.
		// A similar approach can be used to randomize everything
		// else, including fleet composition.
		for (int i = 0; i < 8; i++) {
			float x = (float) Math.random() * width - width/2;
			float y = (float) Math.random() * height - height/2;
			float radius = 150f + (float) Math.random() * 650f; 
			api.addNebula(x, y, radius);
		}
		
		// Add objectives
		//api.addObjective(minX + width * 0.15f + 3000, minY + height * 0.3f + 1000, "nav_buoy");
		//api.addObjective(minX + width * 0.4f + 1000, minY + height * 0.4f, "sensor_array");
		//api.addObjective(minX + width * 0.8f - 2000, minY + height * 0.3f + 1000, "comm_relay");
		
		//api.addObjective(minX + width * 0.85f - 3000, minY + height * 0.7f - 1000, "nav_buoy");
		//api.addObjective(minX + width * 0.6f - 1000, minY + height * 0.6f, "sensor_array");
		//api.addObjective(minX + width * 0.2f + 2000, minY + height * 0.7f - 1000, "comm_relay");
		
		// Add asteroid field
		api.addAsteroidField(minX, minY + height * 0.5f, 0, height,
							20f, 70f, 50);
		
		// Add planet
		//api.addPlanet(0, 0, 350f, "barren", 200f, true);
	}

}
