package data.missions.aiw_diplomatic;

import com.fs.starfarer.api.campaign.CargoAPI.CrewXPLevel;
import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;

public class MissionDefinition implements MissionDefinitionPlugin {

	public void defineMission(MissionDefinitionAPI api) {

		// Set up the fleets so we can add ships and fighter wings to them.
		api.initFleet(FleetSide.PLAYER, "SIS", FleetGoal.ATTACK, false, 3);
		api.initFleet(FleetSide.ENEMY, "ISS", FleetGoal.ATTACK, true, 2);

		// Set a small blurb for each fleet that shows up on the mission detail and
		// mission results screens to identify each side.
		api.setFleetTagline(FleetSide.PLAYER, "Imperial Spire cruiser Peridot and escorts");
		api.setFleetTagline(FleetSide.ENEMY, "Dagger of Winter mercenary group");
		
		// These show up as items in the bulleted list under 
		// "Tactical Objectives" on the mission detail screen
		api.addBriefingItem("Defeat all enemy forces");
		api.addBriefingItem("The SIS Peridot must survive");
		
		// Set up the player's fleet.  Variant names come from the
		// files in data/variants and data/variants/fighters
		api.addToFleet(FleetSide.PLAYER, "spire_cruiser_Siege", FleetMemberType.SHIP, "SIS Peridot", true, CrewXPLevel.ELITE);
		api.addToFleet(FleetSide.PLAYER, "spire_corvette_Standoff", FleetMemberType.SHIP, "SIS Opal", false, CrewXPLevel.VETERAN);
		api.addToFleet(FleetSide.PLAYER, "spire_corvette_Missile", FleetMemberType.SHIP, "SIS Celestine", false, CrewXPLevel.VETERAN);
		api.addToFleet(FleetSide.PLAYER, "spire_colonyship_Standard", FleetMemberType.SHIP, "SIS Graphite", false);
		
		api.defeatOnShipLoss("SIS Peridot");

		// Set up the enemy fleet.
		api.addToFleet(FleetSide.ENEMY, "aurora_Balanced", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "heron_Standard", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "hammerhead_Elite", FleetMemberType.SHIP, false);
		//api.addToFleet(FleetSide.ENEMY, "sunder_Assault", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "enforcer_CS", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "wolf_Assault", FleetMemberType.SHIP, false);
		//api.addToFleet(FleetSide.ENEMY, "omen_PD", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "vigilance_FS", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "lasher_Strike", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "hound_Standard", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "thunder_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "gladius_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "dagger_wing", FleetMemberType.FIGHTER_WING, false);
		
		// Set up the map.
		float width = 16000f;
		float height = 16000f;
		api.initMap((float)-width/2f, (float)width/2f, (float)-height/2f, (float)height/2f);
		
		float minX = -width/2;
		float minY = -height/2;
		
		// All the addXXX methods take a pair of coordinates followed by data for
		// whatever object is being added.
		
		// Add two big nebula clouds
		api.addNebula(minX + width * 0.75f, minY + height * 0.7f, 1600);
		api.addNebula(minX + width * 0.25f, minY + height * 0.4f, 1200);
		
		// And a few random ones to spice up the playing field.
		// A similar approach can be used to randomize everything
		// else, including fleet composition.
		for (int i = 0; i < 7; i++) {
			float x = (float) Math.random() * width - width/2;
			float y = (float) Math.random() * height - height/2;
			float radius = 200f + (float) Math.random() * 600f; 
			api.addNebula(x, y, radius);
		}
		
		// Add objectives
		api.addObjective(minX + width * 0.35f, minY + height * 0.4f, "sensor_array");
		api.addObjective(minX + width * 0.7f, minY + height * 0.6f, "nav_buoy");
		
		// Add asteroid field
		//api.addAsteroidField(minX, minY + height * 0.5f, 0, height, 20f, 70f, 50);
		
		// Add planet
		api.addPlanet(0, 0, 400f, "water", 350f, true);
	}

}
