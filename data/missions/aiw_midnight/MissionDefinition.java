package data.missions.aiw_midnight;

import com.fs.starfarer.api.campaign.CargoAPI.CrewXPLevel;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.impl.campaign.ids.Skills;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;

public class MissionDefinition implements MissionDefinitionPlugin {

	public void defineMission(MissionDefinitionAPI api) {

		// Set up the fleets so we can add ships and fighter wings to them.
		// In this scenario, the fleets are attacking each other, but
		// in other scenarios, a fleet may be defending or trying to escape
		api.initFleet(FleetSide.PLAYER, "PLS", FleetGoal.ATTACK, false, 5);
		api.initFleet(FleetSide.ENEMY, "DSV", FleetGoal.ATTACK, true, 3);

		// Set a small blurb for each fleet that shows up on the mission detail and
		// mission results screens to identify each side.
		api.setFleetTagline(FleetSide.PLAYER, "League defense force");
		api.setFleetTagline(FleetSide.ENEMY, "Dark Spire Vengeance Fleet");
		
		// These show up as items in the bulleted list under 
		// "Tactical Objectives" on the mission detail screen
		api.addBriefingItem("Defeat all enemy forces");
		api.addBriefingItem("All units have skilled captains");
		api.addBriefingItem("The PLS Windswept must survive");
		
		// Set up the player's fleet.  Variant names come from the
		// files in data/variants and data/variants/fighters
		addToFleetAndAddSkills(api, FleetSide.PLAYER, "gryphon_Standard", "PLS Windswept", CrewXPLevel.ELITE, true);
		addToFleetAndAddSkills(api, FleetSide.PLAYER, "falcon_CS", null, CrewXPLevel.VETERAN, false);
		addToFleetAndAddSkills(api, FleetSide.PLAYER, "heron_Standard", null, CrewXPLevel.VETERAN, false);
		addToFleetAndAddSkills(api, FleetSide.PLAYER, "harbinger_Strike", "PLS Direct Control", CrewXPLevel.ELITE, false);
		addToFleetAndAddSkills(api, FleetSide.PLAYER, "hammerhead_Elite", null, CrewXPLevel.VETERAN, false);
		addToFleetAndAddSkills(api, FleetSide.PLAYER, "sunder_CS", null, CrewXPLevel.VETERAN, false);
		addToFleetAndAddSkills(api, FleetSide.PLAYER, "centurion_Assault", null, CrewXPLevel.VETERAN, false);
		addToFleetAndAddSkills(api, FleetSide.PLAYER, "vigilance_Strike", null, CrewXPLevel.VETERAN, false);
		//addToFleetAndAddSkills(api, FleetSide.PLAYER, "tempest_Balanced", null, CrewXPLevel.VETERAN, false);
		addToFleetAndAddSkills(api, FleetSide.PLAYER, "kite_Standard", null, null, false);
		addToFleetAndAddSkills(api, FleetSide.PLAYER, "kite_Standard", null, null, false);
		addToFleetAndAddSkills(api, FleetSide.PLAYER, "wayfarer_Standard", null, null, false);
		addToFleetAndAddSkills(api, FleetSide.PLAYER, "gladius_wing", null, CrewXPLevel.VETERAN, false);
		addToFleetAndAddSkills(api, FleetSide.PLAYER, "warthog_wing", null, CrewXPLevel.VETERAN, false);
		addToFleetAndAddSkills(api, FleetSide.PLAYER, "trident_wing", null, CrewXPLevel.VETERAN, false);
		
		// Set up the enemy fleet.
		addToFleetAndAddSkills(api, FleetSide.ENEMY, "spire_battleship_dark_Command", null, CrewXPLevel.VETERAN, false);
		addToFleetAndAddSkills(api, FleetSide.ENEMY, "spire_cruiser_dark_Line", null, null, false);
		addToFleetAndAddSkills(api, FleetSide.ENEMY, "spire_destroyer_dark_Assault", null, null, false);
		addToFleetAndAddSkills(api, FleetSide.ENEMY, "spire_destroyer_dark_Support", null, null, false);
		//addToFleetAndAddSkills(api, FleetSide.ENEMY, "spire_frigate_dark_Skirmisher", null, null, false);
		addToFleetAndAddSkills(api, FleetSide.ENEMY, "spire_frigate_dark_Standard", null, null, false);
		addToFleetAndAddSkills(api, FleetSide.ENEMY, "spire_corvette_dark_Lance", null, null, false);
		addToFleetAndAddSkills(api, FleetSide.ENEMY, "spire_corvette_dark_Beam", null, null, false);
		addToFleetAndAddSkills(api, FleetSide.ENEMY, "nzl_enclave_Standard", null, null, false);
		addToFleetAndAddSkills(api, FleetSide.ENEMY, "spire_stealthship_dark_Beam", null, CrewXPLevel.VETERAN, false);
		addToFleetAndAddSkills(api, FleetSide.ENEMY, "spire_armorrot_dark_wing", null, null, false);
		addToFleetAndAddSkills(api, FleetSide.ENEMY, "nzl_yngcommando_wing", null, null, false);
		addToFleetAndAddSkills(api, FleetSide.ENEMY, "nzl_yngcommando_wing", null, null, false);
		addToFleetAndAddSkills(api, FleetSide.ENEMY, "nzl_yngvulture_wing", null, null, false);
		
		api.defeatOnShipLoss("PLS Windswept");
		
		// Set up the map.
		float width = 26000f;
		float height = 18000f;
		api.initMap((float)-width/2f, (float)width/2f, (float)-height/2f, (float)height/2f);
		
		float minX = -width/2;
		float minY = -height/2;
		
		// All the addXXX methods take a pair of coordinates followed by data for
		// whatever object is being added.
		
		// Add two big nebula clouds
		api.addNebula(minX + width * 0.64f, minY + height * 0.39f, 1600);
		api.addNebula(minX + width * 0.32f, minY + height * 0.57f, 1900);
		
		// And a few random ones to spice up the playing field.
		// A similar approach can be used to randomize everything
		// else, including fleet composition.
		for (int i = 0; i < 24; i++) {
			float x = (float) Math.random() * width - width/2;
			float y = (float) Math.random() * height - height/2;
			float radius = 150f + (float) Math.random() * 650f;
			if (i >= 16) radius = radius + 100f + (float) Math.random() * 250f;
			api.addNebula(x, y, radius);
		}
		api.setNebulaTex("graphics/terrain/nebula512_blue.png");
		api.setNebulaMapTex("graphics/terrain/nebula_blue_map.png");
		
		// Add objectives
		//api.addObjective(minX + width * 0.15f + 3000, minY + height * 0.3f + 1000, "nav_buoy");
		api.addObjective(minX + width * 0.32f, minY + height * 0.38f, "sensor_array");		
		api.addObjective(minX + width * 0.68f, minY + height * 0.7f + 1000, "nav_buoy");
		api.addObjective(minX + width * 0.5f, minY + height * 0.5f, "comm_relay");
		
		// Add asteroid field
		//api.addAsteroidField(minX, minY + height * 0.5f, 0, height,	20f, 70f, 50);
		
		// Add planet
		//api.addPlanet(0, 0, 350f, "barren", 200f, true);
	}
	
	protected void addToFleetAndAddSkills(MissionDefinitionAPI api, FleetSide fleetSide, String variantId, String name, CrewXPLevel level, boolean isFlagship) {
		if (level == null) level = CrewXPLevel.REGULAR;
		FleetMemberType type = FleetMemberType.SHIP;
		if (variantId.endsWith("_wing")) type = FleetMemberType.FIGHTER_WING;
		FleetMemberAPI member = null;
		if (name != null)
			member = api.addToFleet(fleetSide, variantId, type, name, isFlagship, level);
		else
			member = api.addToFleet(fleetSide, variantId, type, isFlagship, level);
		
		if (type != FleetMemberType.FIGHTER_WING) {
			PersonAPI captain = member.getCaptain();
			//captain.getStats().setAptitudeLevel("combat", 5f);
			captain.getStats().setSkillLevel(Skills.MISSILE_SPECIALIZATION, 5);
			captain.getStats().setSkillLevel(Skills.ORDNANCE_EXPERT, 5);
			captain.getStats().setSkillLevel(Skills.DAMAGE_CONTROL, 5);
			captain.getStats().setSkillLevel(Skills.TARGET_ANALYSIS, 5);
			//captain.getStats().setSkillLevel(Skills.EVASIVE_ACTION, 5);
			captain.getStats().setSkillLevel(Skills.HELMSMANSHIP, 5f);
			captain.getStats().setSkillLevel(Skills.FLUX_MODULATION, 5);
			if (isFlagship) 
				captain.getStats().setSkillLevel(Skills.GUNNERY_IMPLANTS, 5);
		}
	}

}
