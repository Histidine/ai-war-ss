package data.missions.aiw_random_vs_spire;

import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import data.missions.BaseRandomSpireMissionDefinition;

public class MissionDefinition extends BaseRandomSpireMissionDefinition
{
    @Override
    public void defineMission(MissionDefinitionAPI api)
    {
        String faction = "spire";
        if (Math.random() > 0.5) faction = "darkspire";
        chooseFactions(null, faction);
        super.defineMission(api);
    }
}