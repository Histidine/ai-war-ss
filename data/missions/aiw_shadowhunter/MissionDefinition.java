package data.missions.aiw_shadowhunter;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CargoAPI.CrewXPLevel;
import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.BattleCreationContext;
import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.impl.combat.EscapeRevealPlugin;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;
import java.util.List;

public class MissionDefinition implements MissionDefinitionPlugin {

	public void defineMission(MissionDefinitionAPI api) {

		// Set up the fleets so we can add ships and fighter wings to them.
		// In this scenario, the fleets are attacking each other, but
		// in other scenarios, a fleet may be defending or trying to escape
		api.initFleet(FleetSide.PLAYER, "DSV", FleetGoal.ATTACK, false, 3);
		api.initFleet(FleetSide.ENEMY, "CGR", FleetGoal.ESCAPE, true, 3);

		// Set a small blurb for each fleet that shows up on the mission detail and
		// mission results screens to identify each side.
		api.setFleetTagline(FleetSide.PLAYER, "Dark Spire Pestilence Fleet");
		api.setFleetTagline(FleetSide.ENEMY, "Luddic convoy with escort");
		
		// These show up as items in the bulleted list under 
		// "Tactical Objectives" on the mission detail screen
		api.addBriefingItem("Disable as many enemy ships as you can");
		api.addBriefingItem("Use your Matrix Hardening ship system to defend against missiles and other HE weapons");
		
		// Set up the player's fleet.  Variant names come from the
		// files in data/variants and data/variants/fighters
		api.addToFleet(FleetSide.PLAYER, "spire_frigate_dark_Skirmisher", FleetMemberType.SHIP, "DSV Misery", true, CrewXPLevel.VETERAN);
		//api.addToFleet(FleetSide.PLAYER, "spire_frigate_dark_Skirmisher", FleetMemberType.SHIP, "DSV Pain", true, CrewXPLevel.VETERAN);
		api.addToFleet(FleetSide.PLAYER, "spire_corvette_dark_Lance", FleetMemberType.SHIP, "DSV Agony", false);
		api.addToFleet(FleetSide.PLAYER, "spire_corvette_dark_ModZ", FleetMemberType.SHIP, "DSV Suffering", false);
		api.addToFleet(FleetSide.PLAYER, "spire_lcarrier_dark_Standard", FleetMemberType.SHIP, "DSV Torment", false, CrewXPLevel.VETERAN);
		
		api.addToFleet(FleetSide.PLAYER, "spire_armorrot_dark_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.PLAYER, "spire_armorrot_dark_wing", FleetMemberType.FIGHTER_WING, false);
		
		// Set up the enemy fleet.
		api.addToFleet(FleetSide.ENEMY, "enforcer_Balanced", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "enforcer_CS", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "condor_FS", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "hound_luddic_church_Standard", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "hound_luddic_church_Standard", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "lasher_CS", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "cerberus_Standard", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "phaeton_Standard", FleetMemberType.SHIP, false, CrewXPLevel.GREEN);
		api.addToFleet(FleetSide.ENEMY, "tarsus_Standard", FleetMemberType.SHIP, false, CrewXPLevel.GREEN);
		api.addToFleet(FleetSide.ENEMY, "buffalo_luddic_church_Standard", FleetMemberType.SHIP, false, CrewXPLevel.GREEN);
		api.addToFleet(FleetSide.ENEMY, "buffalo_luddic_church_Standard", FleetMemberType.SHIP, false, CrewXPLevel.GREEN);
		
		api.addToFleet(FleetSide.ENEMY, "broadsword_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "talon_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "talon_wing", FleetMemberType.FIGHTER_WING, false);
		
		
		// Set up the map.
		float width = 14000f;
		float height = 24000f;
		api.initMap((float)-width/2f, (float)width/2f, (float)-height/2f, (float)height/2f);
		
		float minX = -width/2;
		float minY = -height/2;
		
		api.addObjective(minX + width * 0.24f, minY + height * 0.32f, "nav_buoy");
		api.addObjective(minX + width * 0.7f, minY + height * 0.69f, "nav_buoy");
		
				
		api.addObjective(minX + width * 0.32f, minY + height * 0.55f, "comm_relay");
		api.addObjective(minX + width * 0.71f, minY + height * 0.44f, "sensor_array");

		api.setHyperspaceMode(true);
		
		BattleCreationContext context = new BattleCreationContext(null, null, null, null);
		context.setInitialEscapeRange(8000f);
		api.addPlugin(new EscapeRevealPlugin(context));
		
		// set flank distance
		
		api.addPlugin(new BaseEveryFrameCombatPlugin() {
			boolean done = false;
			
			@Override
			public void advance(float amount, List events) {
				if (!done)
				{
					Global.getCombatEngine().getContext().setFlankDeploymentDistance(10000);
					done = true;
				}
			}
		});
		
		
	}

}
