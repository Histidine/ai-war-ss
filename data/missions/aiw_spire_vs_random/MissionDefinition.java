package data.missions.aiw_spire_vs_random;

import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import data.missions.BaseRandomSpireMissionDefinition;

public class MissionDefinition extends BaseRandomSpireMissionDefinition
{
    @Override
    public void defineMission(MissionDefinitionAPI api)
    {
        String faction = "spire";
        if (Math.random() > 0.5) faction = "darkspire";
        chooseFactions(faction, null);
        super.defineMission(api);
    }
}