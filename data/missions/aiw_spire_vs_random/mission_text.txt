Location: Somewhere near the Gemstone system
Date: Unknown

Command a randomly sized Spire or Dark Spire fleet and battle a random equally matched fleet from a different vanilla / mod faction. Factions cannot fight themselves.
Features an improved battlespace terrain generator and fleet balance.

Click on the mission in the list to re-roll.

Powered by ExiRandom v1.1®