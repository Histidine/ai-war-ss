package data.missions.aiw_spiretest;

import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import data.missions.BaseRandomSpireMissionDefinition;

public class MissionDefinition extends BaseRandomSpireMissionDefinition
{
    @Override
    public void defineMission(MissionDefinitionAPI api)
    {
        chooseFactions("spire", null);
		usePredefinedFleet(true, false);
		
		super.defineMission(api);
		// Set up the player's fleet.  Variant names come from the
		// files in data/variants and data/variants/fighters
		
		api.addToFleet(FleetSide.PLAYER, "spire_battleship_Command", FleetMemberType.SHIP, true);
		api.addToFleet(FleetSide.PLAYER, "spire_champion_Elite", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "spire_cruiser_Line", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "spire_destroyer_Assault", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "spire_destroyer_Support", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "spire_lcarrier_Standard", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "spire_frigate_Standard", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "spire_frigate_Skirmisher", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "spire_corvette_Standoff", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "spire_corvette_Lance", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "spire_armorrot_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.PLAYER, "spire_armorrot_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.PLAYER, "spire_colonyship_Standard", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "spire_colonyship_Hammer", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "spire_stealthship_Strike", FleetMemberType.SHIP, false);
	}

}
