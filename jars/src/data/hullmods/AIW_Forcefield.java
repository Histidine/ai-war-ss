package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.plugins.AIW_ForcefieldPlugin;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Histidine (AI modified from Dark.Revenant's Aegis Shield AI)
 */
public class AIW_Forcefield extends BaseHullMod{
    
    private static final float SECONDS_TO_LOOK_AHEAD = 3f;
    private static final Set<String> BLOCKED_HULLMODS = new HashSet<>();
    
    static
    {
        // These hullmods will automatically be removed
        // Not as elegant as blocking them in the first place, but
        // this method doesn't require editing every hullmod's script
        BLOCKED_HULLMODS.add("adaptiveshields");
        BLOCKED_HULLMODS.add("extendedshieldemitter");
    }
    
    /*
    private final CollectionUtils.CollectionFilter<DamagingProjectileAPI> filterMisses = new CollectionUtils.CollectionFilter<DamagingProjectileAPI>()
    {
        @Override
        public boolean accept(DamagingProjectileAPI proj)
        {
            // Exclude missiles and our own side's shots
            if (proj instanceof MissileAPI || proj.getOwner() == ship.getOwner())
            {
                return false;
            }

            // Only include shots that are on a collision path with us
            // Also ensure they aren't travelling AWAY from us ;)
            return (CollisionUtils.getCollides(proj.getLocation(), Vector2f.add(proj.getLocation(), (Vector2f) new Vector2f(proj.getVelocity()).scale(
                    SECONDS_TO_LOOK_AHEAD), null), ship.getLocation(), ship.getCollisionRadius())
                    && Math.abs(MathUtils.getShortestRotation(proj.getFacing(), VectorUtils.getAngle(proj.getLocation(), ship.getLocation()))) <= 90f);
        }
    };
    */
    
    private ShipAPI ship;
    private final IntervalUtil tracker = new IntervalUtil(0.2f, 0.35f);
    private float offDelay = 0;

    protected float getFluxProportion(ShipAPI ship)
    {
        return ship.getFluxTracker().getCurrFlux()/ship.getFluxTracker().getMaxFlux();
    }
    
    protected float getFluxRemaining(ShipAPI ship)
    {
        return ship.getFluxTracker().getMaxFlux() - ship.getFluxTracker().getCurrFlux();
    }
    
    /*
    @Override
    public void advanceInCombat(ShipAPI ship, float amount)
    {
        if (ship.getShipAI() == null) return;
        
        tracker.advance(amount);
        Vector2f shipLoc = ship.getLocation();
        if (offDelay > 0) offDelay -= amount;

        if (tracker.intervalElapsed())
        {
            // Can we even use the system right now?
            if (!canUseAltSystemThisFrame(ship))
            {
                return;
            }
            if (AIW_ForcefieldPlugin.isForcefieldDead(ship))
            {
                return;
            }
            if (AIW_ForcefieldPlugin.isForcefieldWantActive(ship) && offDelay > 0)
            {
                return;
            }
            if (ship.getSystem().getId().equals("aiw_blossomfield") && ship.getSystem().isActive())
            {
                return;
            }
            
            ShipwideAIFlags flags = ship.getAIFlags();
            this.ship = ship;
            
            boolean shouldUseSystem = false;
            
            if (flags.hasFlag(ShipwideAIFlags.AIFlags.KEEP_SHIELDS_ON)) {
                //decisionLevel *= 2f;
                shouldUseSystem = true;
                offDelay = 4;
            }
            else if (getFluxProportion(ship) > 0.95)
                shouldUseSystem = false;
            else if (getFluxRemaining(ship) < 100)
                shouldUseSystem = false;
            else
            {
                List<DamagingProjectileAPI> nearbyThreats = new ArrayList<>(500);
                for (DamagingProjectileAPI tmp : Global.getCombatEngine().getProjectiles()) {
                    if (tmp.getOwner() != ship.getOwner() && MathUtils.isWithinRange(tmp.getLocation(), shipLoc, 250 + ship.getCollisionRadius() * 10f)) {
                        nearbyThreats.add(tmp);
                    }
                }
                //nearbyThreats = CollectionUtils.filter(nearbyThreats, filterMisses);
                List<MissileAPI> nearbyMissiles = AIUtils.getNearbyEnemyMissiles(ship, 250 + ship.getCollisionRadius() * 5f);
                for (MissileAPI missile : nearbyMissiles) {
                    nearbyThreats.add(missile);
                }


                float decisionLevel = 0f;
                for (DamagingProjectileAPI threat : nearbyThreats) {
                    if (threat.getDamageType() == DamageType.FRAGMENTATION) {
                        decisionLevel += Math.pow((float) (threat.getDamageAmount() + threat.getEmpAmount() * 0.25f) / 400f, 1.3f);
                    } else {
                        decisionLevel += Math.pow((float) (threat.getDamageAmount() + threat.getEmpAmount() * 0.25f) / 100f, 1.3f);
                    }
                }

                decisionLevel *= (1 - ship.getFluxTracker().getCurrFlux()) / ship.getFluxTracker().getMaxFlux() + 1f;
                decisionLevel *= (float) Math.sqrt(2f - ship.getHullLevel());
                if (flags.hasFlag(ShipwideAIFlags.AIFlags.PURSUING)) {
                    decisionLevel *= 0.5f;
                }
                if (flags.hasFlag(ShipwideAIFlags.AIFlags.RUN_QUICKLY)) {
                    decisionLevel *= 1.5;
                } else if (flags.hasFlag(ShipwideAIFlags.AIFlags.BACK_OFF)) {
                    decisionLevel *= 1.25;
                }
                if (!flags.hasFlag(ShipwideAIFlags.AIFlags.HAS_INCOMING_DAMAGE)) {
                    decisionLevel *= 0.75f;
                }
                
                if (decisionLevel >= 0.5 || decisionLevel >= (float) Math.sqrt(ship.getMaxHitpoints() / 80f)) {
                //if (decisionLevel >= (float) Math.sqrt(ship.getMaxHitpoints() / 40f)) {
                    shouldUseSystem = true;
                    offDelay = 4;
                }
            }
            // If system is inactive and should be active, enable it
            // If system is active and shouldn't be, disable it
            boolean currentlyActive = AIW_ForcefieldPlugin.isForcefieldWantActive(ship);
            //Global.getCombatEngine().addFloatingText(new Vector2f(ship.getLocation().x - 60, ship.getLocation().y), currentlyActive + "", 24, Color.yellow, ship, 0, 0);
            //Global.getCombatEngine().addFloatingText(new Vector2f(ship.getLocation().x + 60, ship.getLocation().y), shouldUseSystem + "", 24, Color.yellow, ship, 0, 0);
            if ((currentlyActive && !shouldUseSystem && offDelay <= 0) || (!currentlyActive && shouldUseSystem))
            {
                //ship.giveCommand(ShipCommand.TOGGLE_SHIELD_OR_PHASE_CLOAK, null, 0);
                //AIW_ForcefieldPlugin.toggleForcefield(ship);
                ship.getShield().toggleOff();
            }
        }
    }
    */
    
    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id)
    {
        //Global.getLogger(this.getClass()).info("Adding forcefield for ship hull " + ship.getHullSpec().getHullName());
        AIW_ForcefieldPlugin.addForcefieldShip(ship);
        for (String tmp : BLOCKED_HULLMODS)
        {
            if (ship.getVariant().hasHullMod(tmp))
            {
                ship.getVariant().removeMod(tmp);
            }
        }
    }
    
    @Override
    public void applyEffectsBeforeShipCreation(ShipAPI.HullSize hullSize, MutableShipStatsAPI stats, String id) {
        //stats.getShieldUnfoldRateMult().modifyMult("forcefield", 10);
    }
    
}
