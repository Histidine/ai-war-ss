package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import data.scripts.util.AIW_StringHelper;
import java.util.HashMap;
import java.util.Map;

public class AIW_LargeVessel extends BaseHullMod{
    
    protected static final Map<HullSize, Float> SENSOR_STRENGTH_INCREASE = new HashMap<>();
    protected static final Map<HullSize, Float> SENSOR_PROFILE_INCREASE = new HashMap<>();
    
    static {
        SENSOR_STRENGTH_INCREASE.put(HullSize.FIGHTER, 1f);
        SENSOR_STRENGTH_INCREASE.put(HullSize.FRIGATE, 1f);
        SENSOR_STRENGTH_INCREASE.put(HullSize.DESTROYER, 1f);
        SENSOR_STRENGTH_INCREASE.put(HullSize.CRUISER, 1f);
        SENSOR_STRENGTH_INCREASE.put(HullSize.CAPITAL_SHIP, 1f);
        
        SENSOR_PROFILE_INCREASE.put(HullSize.FIGHTER, 1f);
        SENSOR_PROFILE_INCREASE.put(HullSize.FRIGATE, 1f);
        SENSOR_PROFILE_INCREASE.put(HullSize.DESTROYER, 1f);
        SENSOR_PROFILE_INCREASE.put(HullSize.CRUISER, 1f);
        SENSOR_PROFILE_INCREASE.put(HullSize.CAPITAL_SHIP, 1f);
    }
    
    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id)
    {
        float strengthIncrease = SENSOR_STRENGTH_INCREASE.get(hullSize);
        float profileIncrease = SENSOR_PROFILE_INCREASE.get(hullSize);
        String desc = AIW_StringHelper.getString("aiw_hullmods", "largeVesselEffect");
        stats.getSensorStrength().modifyFlat(id, strengthIncrease, desc);
        stats.getSensorProfile().modifyFlat(id, profileIncrease, desc);
    }
    
    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id)
    {
    }
    
    @Override
    public String getDescriptionParam(int index, HullSize hullSize)
    {
        if (index == 0) return "1";
        return null;
    }
    
}
