package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import java.util.HashSet;
import java.util.Set;

public class AIW_OrganicSystems extends BaseHullMod{
    
	protected static final float REPAIR_BONUS = 0.4f;
    protected static final float BEAM_RESISTANCE = 0.5f;
    protected static final float EMP_RESISTANCE = 0.6f;
	protected static final float COMBAT_REPAIR_FIGHTER = 2f;
	protected static final float COMBAT_REPAIR_FRIGATE = 1f;
	protected static final float COMBAT_REPAIR_DESTROYER = 0.5f;
	protected static final float COMBAT_REPAIR_CRUISER = 0.25f;
	protected static final float COMBAT_REPAIR_CAPITAL = 0.12f;
    
    protected static final Set<String> BLOCKED_HULLMODS = new HashSet<>();
    
    static
    {
        // These hullmods will automatically be removed
        // Not as elegant as blocking them in the first place, but
        // this method doesn't require editing every hullmod's script
        BLOCKED_HULLMODS.add("frontshield");  
    }
    
    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id)
    {
        stats.getBeamDamageTakenMult().modifyMult(id, 1f - BEAM_RESISTANCE);
        stats.getEmpDamageTakenMult().modifyMult(id, 1f - EMP_RESISTANCE);
		stats.getRepairRatePercentPerDay().modifyMult(id, 1 + REPAIR_BONUS);
		
		switch (hullSize) {
			case FIGHTER:
				stats.getHullCombatRepairRatePercentPerSecond().modifyFlat(id, COMBAT_REPAIR_FIGHTER);
				break;
			case FRIGATE:
				stats.getHullCombatRepairRatePercentPerSecond().modifyFlat(id, COMBAT_REPAIR_FRIGATE);
				break;
			case DESTROYER:
				stats.getHullCombatRepairRatePercentPerSecond().modifyFlat(id, COMBAT_REPAIR_DESTROYER);
				break;
			case CRUISER:
				stats.getHullCombatRepairRatePercentPerSecond().modifyFlat(id, COMBAT_REPAIR_CRUISER);
				break;
			case CAPITAL_SHIP:
				stats.getHullCombatRepairRatePercentPerSecond().modifyFlat(id, COMBAT_REPAIR_CAPITAL);
				break;
		}
		stats.getMaxCombatHullRepairFraction().modifyFlat(id, 1);
    }
    
    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id)
    {
        for (String tmp : BLOCKED_HULLMODS)
        {
            if (ship.getVariant().hasHullMod(tmp))
            {
                ship.getVariant().removeMod(tmp);
            }
        }
    }
    
	protected String convertToPercentStr(float value)
	{
		return (int) (value * 100) + "";
	}
	
    @Override
    public String getDescriptionParam(int index, HullSize hullSize)
    {
		switch (index)
		{
			case 0:
				return convertToPercentStr(REPAIR_BONUS);
			case 1:
				return "" + COMBAT_REPAIR_FRIGATE;
			case 2:
				return "" + COMBAT_REPAIR_DESTROYER;
			case 3:
				return "" + COMBAT_REPAIR_CRUISER;
			case 4:
				return "" + COMBAT_REPAIR_CAPITAL;
			case 5:
				return convertToPercentStr(EMP_RESISTANCE);
			case 6:
				return convertToPercentStr(BEAM_RESISTANCE);
			default:
				return null;
		}
    }
    
}
