package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import java.util.HashSet;
import java.util.Set;

public class AIW_ReflectiveShell extends BaseHullMod{
    
    private static final float BEAM_RESISTANCE = 0.5f;
    private static final float EMP_RESISTANCE = 0.6f;
    
    private static final Set<String> BLOCKED_HULLMODS = new HashSet<>();
    
    static
    {
        // These hullmods will automatically be removed
        // Not as elegant as blocking them in the first place, but
        // this method doesn't require editing every hullmod's script
        BLOCKED_HULLMODS.add("frontshield");  
    }
    
    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id)
    {
        stats.getBeamDamageTakenMult().modifyMult(id, 1f - BEAM_RESISTANCE);
        stats.getEmpDamageTakenMult().modifyMult(id, 1f - EMP_RESISTANCE);
    }
    
    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id)
    {
        for (String tmp : BLOCKED_HULLMODS)
        {
            if (ship.getVariant().hasHullMod(tmp))
            {
                ship.getVariant().removeMod(tmp);
            }
        }
    }
    
    @Override
    public String getDescriptionParam(int index, HullSize hullSize)
    {
        if (index == 0)
        {
            return "" + (int) (BEAM_RESISTANCE * 100);
        }
        else if (index == 1)
        {
            return "" + (int) (EMP_RESISTANCE * 100);
        }
        return null;
    }
    
}
