package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import data.scripts.util.AIW_StringHelper;

public class NZL_Youngling extends BaseHullMod{
    
    protected static final float CR_BONUS = 0.2f;
    
    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id)
    {
        stats.getMaxCombatReadiness().modifyFlat(id, CR_BONUS, AIW_StringHelper.getString("aiw_hullmods", "neinzulYounglingEffect"));
    }
    
    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id)
    {

    }
    
    @Override
    public String getDescriptionParam(int index, HullSize hullSize)
    {
        if (index == 0)
        {
            return "" + (int) (CR_BONUS * 100);
        }
        return null;
    }
    
}
