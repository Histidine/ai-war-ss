package data.scripts;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import com.thoughtworks.xstream.XStream;
import data.scripts.campaign.AIW_CampaignManager;
import data.scripts.world.AIW_SectorGen;
import data.scripts.world.AIW_VengeanceFleetAI;
import data.scripts.world.AIW_VengeanceGeneratorManager;
import data.scripts.world.systems.ZEN_ZenithTrader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.dark.shaders.light.LightData;
import org.dark.shaders.util.ShaderLib;
import org.dark.shaders.util.TextureData;

public class AIWarModPlugin extends BaseModPlugin
{
    public static final boolean hasTwigLib = Global.getSettings().getModManager().isModEnabled("ztwiglib");
    public static final boolean hasExerelin = Global.getSettings().getModManager().isModEnabled("nexerelin");
    public static final boolean templarsExists = Global.getSettings().getModManager().isModEnabled("Templars");
    
    @Override
    public void beforeGameSave()
    {
    }

    @Override
    public void onGameLoad(boolean newGame)
    {
    }

    @Override
    public void onNewGame()
    {
        if (!hasExerelin)
        {
            new AIW_SectorGen().generate(Global.getSector());
        }
        else
        {
            try {
                Class<?> def = Global.getSettings().getScriptClassLoader().loadClass("exerelin.campaign.SectorManager");
                Method method;
                method = def.getMethod("getCorvusMode");
                Object result = method.invoke(def);
                if ((boolean)result == true)
                {
                    new AIW_SectorGen().generate(Global.getSector());
                }
            } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException |
                     InvocationTargetException | ClassNotFoundException ex) {
                // check failed, do nothing
            }
        }
    }
    
    @Override
    public void onApplicationLoad() {  
        ShaderLib.init();  
        LightData.readLightDataCSV("data/lights/aiw_lights.csv");  
        TextureData.readTextureDataCSV("data/lights/aiw_textures.csv");  
    }  

    @Override
    public void onNewGameAfterEconomyLoad()
    {
        boolean useRandom = hasExerelin;
        try {
            Class<?> def = Global.getSettings().getScriptClassLoader().loadClass("exerelin.campaign.SectorManager");
            Method method;
            method = def.getMethod("getCorvusMode");
            Object result = method.invoke(def);
            if ((boolean)result == true)
            {
                useRandom = false;
            }
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException |
                 InvocationTargetException | ClassNotFoundException ex) {
            // check failed, do nothing
        }
        
        AIW_VengeanceGeneratorManager vengenManager = new AIW_VengeanceGeneratorManager();
        
        //Global.getSector().addListener(vengenManager);    //no need, already registered once on creation or something?
        vengenManager.generate(Global.getSector(), useRandom);
    }

	@Override
	public void onNewGameAfterTimePass() {
		ZEN_ZenithTrader.clearPeople();
	}
    
    @Override
    public void onEnabled(boolean wasEnabledBefore) {
        if (!wasEnabledBefore) new AIW_CampaignManager();
    }
    
    @Override
    public void configureXStream(XStream x) {
        x.alias("AIW_VengeanceGeneratorManager", AIW_VengeanceGeneratorManager.class);
		x.alias("AIW_VengeanceFleetAI", AIW_VengeanceFleetAI.class);
        x.alias("AIW_CampaignManager", AIW_CampaignManager.class);
    }
}