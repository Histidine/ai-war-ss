package data.scripts.campaign;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.campaign.BaseCampaignEventListener;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.FleetEncounterContextPlugin;
import com.fs.starfarer.api.campaign.FleetEncounterContextPlugin.FleetMemberData;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;
import java.util.ArrayList;
import java.util.List;
import org.lazywizard.lazylib.MathUtils;

public class AIW_CampaignManager extends BaseCampaignEventListener implements EveryFrameScript {

	public static final float SPIRE_CRYSTALS_PER_HULL_POINT = 0.001f;
	public static final float NEINZUL_ORGANICS_PER_HULL_POINT = 0.002f;
	public static final float SHARD_REACTORS_PER_FP = 0.1f;
	
	public AIW_CampaignManager() {
		super(true);
	}
	
	// add drops like Spire plates, etc.
	@Override
	public void reportEncounterLootGenerated(FleetEncounterContextPlugin plugin, CargoAPI loot) {	
		CampaignFleetAPI loser = plugin.getLoser();
		if (loser == null) return;
		
		int totalFP = 0;
		int totalHull = 0;
		int spireFP = 0;
		int spireHull = 0;
		int neinzulFP = 0;
		int neinzulHull = 0;
		List<FleetMemberAPI> deadShips = new ArrayList<>(); 
		List<FleetEncounterContextPlugin.FleetMemberData> casualties = plugin.getLoserData().getOwnCasualties();
		for (FleetMemberData memberData : casualties) {
			FleetEncounterContextPlugin.Status status = memberData.getStatus();
			if (status == FleetEncounterContextPlugin.Status.DESTROYED || status == FleetEncounterContextPlugin.Status.NORMAL) continue;
			FleetMemberAPI member = memberData.getMember();
			deadShips.add(member);
			int fp = member.getFleetPointCost();
			float hull = member.getHullSpec().getHitpoints() + member.getHullSpec().getArmorRating() * 4;
			if (member.isFighterWing())
				hull *= member.getNumFightersInWing();
			
			totalFP += fp;
			totalHull += hull;
			if (member.getHullId().startsWith("spire_"))
			{
				spireFP += fp;
				spireHull += hull;
			}
			else if (member.getHullId().startsWith("nzl_"))
			{
				neinzulFP += fp;
				neinzulHull += hull;
			}
		}
		
		float contrib = plugin.computePlayerContribFraction();
		spireFP *= contrib;
		spireHull *= contrib;
		neinzulFP *= contrib;
		neinzulHull *= contrib;		
		
		if (spireFP != 0 || spireHull != 0) 
		{
			int numCrystruct = (int)(spireHull * SPIRE_CRYSTALS_PER_HULL_POINT * MathUtils.getRandomNumberInRange(0.75f, 1.25f));
			loot.addCommodity("spire_crystruct", numCrystruct);
			loot.addCommodity(Commodities.METALS, -numCrystruct);
			int numShardReactors = (int)(spireFP * SHARD_REACTORS_PER_FP * MathUtils.getRandomNumberInRange(0.1f, 1.5f));
			if (numShardReactors > 0) loot.addCommodity("spire_shardreactor", numShardReactors);
		}
		if (neinzulHull != 0)
		{
			int numOrganics = (int)(spireHull * NEINZUL_ORGANICS_PER_HULL_POINT * MathUtils.getRandomNumberInRange(0.75f, 1.25f));
			loot.addCommodity(Commodities.ORGANICS, numOrganics);
			loot.addCommodity(Commodities.METALS, -numOrganics);
		}
	}
	
	@Override
	public boolean isDone() {
		return false;
	}

	@Override
	public boolean runWhilePaused() {
		return false;
	}

	@Override
	public void advance(float amount) {
		
	}
	
}
