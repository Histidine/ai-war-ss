package data.scripts.campaign.econ;

import com.fs.starfarer.api.impl.campaign.econ.BaseMarketConditionPlugin;
import com.fs.starfarer.api.impl.campaign.econ.ConditionData;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;

public class AIW_InorganicPopulace extends BaseMarketConditionPlugin {
    
    public static final float INORGANIC_FOOD_MULT = 0.05f;
    public static final float INORGANIC_ORE_MULT = 0.1f;
    public static final float INORGANIC_RARE_ORE_MULT = 0.005f;
    public static final float INORGANIC_VOLATILES_MULT = 0.1f;
    public static final float INORGANIC_ORGANICS_MULT = 0.05f;
    public static final float INORGANIC_NON_CONSUMING_FRACTION = 0.8f;

    @Override
    public void apply(String id) {
        
        float pop = getPopulation(market);
        float foodAmount = pop * ConditionData.POPULATION_FOOD_MULT;
        float resAmount = foodAmount * (1 - INORGANIC_FOOD_MULT);
        market.getDemand(Commodities.FOOD).getDemand().modifyFlat(id, -resAmount);
        
        //if (pop > 6) pop = 6;
        //foodAmount = pop * ConditionData.POPULATION_FOOD_MULT;
        //resAmount = foodAmount * (1 - INORGANIC_FOOD_MULT);

        market.getDemand(Commodities.ORE).getDemand().modifyFlat(id, resAmount * INORGANIC_ORE_MULT);
        market.getDemand(Commodities.ORE).getNonConsumingDemand().modifyFlat(id, resAmount * INORGANIC_ORE_MULT * INORGANIC_NON_CONSUMING_FRACTION);

        market.getDemand(Commodities.RARE_ORE).getDemand().modifyFlat(id, resAmount * INORGANIC_RARE_ORE_MULT);
        market.getDemand(Commodities.RARE_ORE).getNonConsumingDemand().modifyFlat(id, resAmount * INORGANIC_RARE_ORE_MULT * INORGANIC_NON_CONSUMING_FRACTION);
        
        market.getDemand(Commodities.VOLATILES).getDemand().modifyFlat(id, resAmount * INORGANIC_VOLATILES_MULT);
        market.getDemand(Commodities.VOLATILES).getNonConsumingDemand().modifyFlat(id, resAmount * INORGANIC_VOLATILES_MULT * INORGANIC_NON_CONSUMING_FRACTION);
        
        market.getDemand(Commodities.ORGANICS).getDemand().modifyFlat(id, resAmount * INORGANIC_ORGANICS_MULT);
        market.getDemand(Commodities.ORGANICS).getNonConsumingDemand().modifyFlat(id, resAmount * INORGANIC_ORGANICS_MULT * INORGANIC_NON_CONSUMING_FRACTION);
    }

    @Override
    public void unapply(String id) {
        market.getDemand(Commodities.FOOD).getDemand().unmodify(id);
        
        market.getDemand(Commodities.ORE).getDemand().unmodify(id);
        market.getDemand(Commodities.ORE).getNonConsumingDemand().unmodify(id);

        market.getDemand(Commodities.RARE_ORE).getDemand().unmodify(id);
        market.getDemand(Commodities.RARE_ORE).getNonConsumingDemand().unmodify(id);
        
        market.getDemand(Commodities.VOLATILES).getDemand().unmodify(id);
        market.getDemand(Commodities.VOLATILES).getNonConsumingDemand().unmodify(id);
        
        market.getDemand(Commodities.ORGANICS).getDemand().unmodify(id);
        market.getDemand(Commodities.ORGANICS).getNonConsumingDemand().unmodify(id);
    }
}
