package data.scripts.campaign.submarkets;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.CargoStackAPI;
import com.fs.starfarer.api.campaign.CoreUIAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.FleetDataAPI;
import com.fs.starfarer.api.campaign.RepLevel;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.econ.SubmarketAPI;
import com.fs.starfarer.api.combat.WeaponAPI.AIHints;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.fleet.ShipRolePick;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.ShipRoles;
import com.fs.starfarer.api.impl.campaign.submarkets.BaseSubmarketPlugin;
import com.fs.starfarer.api.loading.WeaponSlotAPI;
import com.fs.starfarer.api.loading.WeaponSpecAPI;
import com.fs.starfarer.api.util.Highlights;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.util.AIW_StringHelper;
import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ZEN_ZenithTraderPlugin extends BaseSubmarketPlugin {

    //public static final int NUMBER_OF_SHIPS = 16;
    //public static final int MAX_WEAPONS = 27;
    public static final RepLevel MIN_STANDING = RepLevel.NEUTRAL;
    public static final String ILLEGAL_TRANSFER_MESSAGE = AIW_StringHelper.getString("aiw_zenithtrader", "noSale");
    public static final String SHIPS_BLACKLIST = "data/config/prism/prism_ships_blacklist.csv";
    public static final String WEAPONS_BLACKLIST = "data/config/prism/prism_weapons_blacklist.csv";
    
    public static Logger log = Global.getLogger(ZEN_ZenithTraderPlugin.class);
    
    protected static Set<String> restrictedWeapons;
    protected static Set<String> restrictedShips;
    
    protected FactionAPI faction1 = null;
    protected FactionAPI faction2 = null;
    protected WeightedRandomPicker<FactionAPI> factionPicker = new WeightedRandomPicker<>();
    
    @Override
    public void init(SubmarketAPI submarket) {
        super.init(submarket);
    }

    @Override
    public void updateCargoPrePlayerInteraction() {

        if (!okToUpdateCargo()) return;
        sinceLastCargoUpdate = 0f;
        
        //Setup blacklists
        try {
            setupLists();
        } catch (JSONException | IOException ex) {
            log.error(ex);
        }
        
        CargoAPI cargo = getCargo();

        // set factions
        SectorAPI sector = Global.getSector();
        if (factionPicker.getItems().size() <= 1)
        {
            for (FactionAPI faction: Global.getSector().getAllFactions()) {
                if (!faction.isShowInIntelTab()) continue;
                //if (faction.isNeutralFaction()) continue;
                //if (faction.isPlayerFaction()) continue;
                String factionId = faction.getId();
                if (!factionId.contains("templars") && !factionId.contains("pirates")) 
                {
                    factionPicker.add(sector.getFaction(factionId));
                }
            }
        }
        faction1 = factionPicker.pickAndRemove();
        faction2 = factionPicker.pickAndRemove();
        if (faction1 == null) faction1 = Global.getSector().getFaction(Factions.INDEPENDENT);
        if (faction2 == null) faction2 = faction1;
        
        //add weapons
        pruneWeapons(1f);
        pruneWeapons(0f);
        
        float variation=(float)Math.random()*0.5f+0.75f;
        int numWeps = (int)(20*variation + 0.5);
        
        WeightedRandomPicker<FactionAPI> weaponFactionPicker = new WeightedRandomPicker<>();
        weaponFactionPicker.add(faction1);
        weaponFactionPicker.add(faction2);
        
        for ( float i=0f; i < numWeps; i = (float) cargo.getStacksCopy().size()) {
            addWeaponsForRolePicks(8, 4, weaponFactionPicker,
                ShipRoles.COMBAT_FREIGHTER_SMALL,
                ShipRoles.COMBAT_FREIGHTER_MEDIUM,
                ShipRoles.COMBAT_FREIGHTER_LARGE,
                
                ShipRoles.FAST_ATTACK,
                ShipRoles.ESCORT_SMALL,
                ShipRoles.ESCORT_MEDIUM,
                ShipRoles.COMBAT_SMALL,
                ShipRoles.COMBAT_MEDIUM,
                ShipRoles.COMBAT_LARGE,
                ShipRoles.COMBAT_CAPITAL,
                ShipRoles.FAST_ATTACK,
                ShipRoles.ESCORT_SMALL,
                ShipRoles.ESCORT_MEDIUM,
                ShipRoles.COMBAT_SMALL,
                ShipRoles.COMBAT_MEDIUM,
                ShipRoles.COMBAT_LARGE,
                ShipRoles.COMBAT_CAPITAL,
                
                ShipRoles.CARRIER_SMALL,
                ShipRoles.CARRIER_MEDIUM,
                ShipRoles.CARRIER_LARGE
            );
            for (CargoStackAPI s : cargo.getStacksCopy()) {
                //remove all low tier weapons
                if (s.isWeaponStack() 
                        && (s.getWeaponSpecIfWeapon().getWeaponId().startsWith("tem_") //templars are forbiden anyway
                            || restrictedWeapons.contains(s.getWeaponSpecIfWeapon().getWeaponId()) //blacklist check
                        )
                    ){
                    float qty = s.getSize();
                    cargo.removeItems(s.getType(), s.getData(), qty );
                    cargo.removeEmptyStacks();
                }
            }
        }
        
        addShips();
        cargo.sort();
    }

    protected void addShips() {
        
        CargoAPI cargo = getCargo();
        FleetDataAPI data = cargo.getMothballedShips();
        
        //remove all the current stock
        for (FleetMemberAPI member : data.getMembersListCopy()) {
            data.removeFleetMember(member);                
        }

        WeightedRandomPicker<String> rolePicker = new WeightedRandomPicker<>();
        rolePicker.add(ShipRoles.CIV_RANDOM, 1f);
        rolePicker.add(ShipRoles.FREIGHTER_SMALL, 1f);
        rolePicker.add(ShipRoles.FREIGHTER_MEDIUM, 1f);
        rolePicker.add(ShipRoles.FREIGHTER_LARGE, 5f);
        rolePicker.add(ShipRoles.TANKER_SMALL, 1f);
        rolePicker.add(ShipRoles.TANKER_MEDIUM, 1f);
        rolePicker.add(ShipRoles.TANKER_LARGE, 1f);
        rolePicker.add(ShipRoles.COMBAT_FREIGHTER_SMALL, 1f);
        rolePicker.add(ShipRoles.COMBAT_FREIGHTER_MEDIUM, 1f);
        rolePicker.add(ShipRoles.COMBAT_FREIGHTER_LARGE, 5f);
        rolePicker.add(ShipRoles.COMBAT_SMALL, 25f);
        rolePicker.add(ShipRoles.COMBAT_MEDIUM, 30f);
        rolePicker.add(ShipRoles.COMBAT_LARGE, 25f);
        rolePicker.add(ShipRoles.COMBAT_CAPITAL, 15f);
        rolePicker.add(ShipRoles.CARRIER_SMALL, 5f);
        rolePicker.add(ShipRoles.CARRIER_MEDIUM, 5f);
        rolePicker.add(ShipRoles.CARRIER_LARGE, 5f);
        rolePicker.add(ShipRoles.INTERCEPTOR, 20f);
        rolePicker.add(ShipRoles.FIGHTER, 20f);
        rolePicker.add(ShipRoles.BOMBER, 20f);
        
        //renew the stock
        float variation=(float)Math.random()*0.5f+0.75f;
        
        for (int i=0; i<8*variation; i=cargo.getMothballedShips().getNumMembers()){
            //pick the role and faction
            FactionAPI faction = Math.random() > 0.5 ? faction1 : faction2;
            String role = rolePicker.pick();            
            //pick the random ship
            List<ShipRolePick> picks = faction.pickShip(role, 1, rolePicker.getRandom());
            for (ShipRolePick pick : picks) {
                FleetMemberType type = FleetMemberType.SHIP;
                String variantId = pick.variantId; 
                                
                //set the ID
                if (pick.isFighterWing()) {
                    type = FleetMemberType.FIGHTER_WING;
                } else {
                    FleetMemberAPI member = Global.getFactory().createFleetMember(type, pick.variantId);
                    variantId = member.getHullId() + "_Hull";
                }                
                //create the ship
                FleetMemberAPI member = Global.getFactory().createFleetMember(type, variantId);
                
                //if the variant is not prohibited, add it. Else start over
                if (member.getHullSpec().getBaseHullId() != null
                    && !restrictedShips.contains(member.getHullId())) //blacklist check
                {
                    member.getRepairTracker().setMothballed(true);
                    getCargo().getMothballedShips().addFleetMember(member);
                } else {
                    i-=1;
                }
            }
        }
    }
    
    //BLACKLISTS
    protected void setupLists() throws JSONException, IOException {

        // Restricted goods
        JSONArray csv = Global.getSettings().getMergedSpreadsheetDataForMod("id",
                WEAPONS_BLACKLIST, "aiwar");
        restrictedWeapons = new HashSet<>();
        for (int x = 0; x < csv.length(); x++)
        {
            JSONObject row = csv.getJSONObject(x);
            restrictedWeapons.add(row.getString("id"));
        }

        // Restricted ships
        csv = Global.getSettings().getMergedSpreadsheetDataForMod("id",
                SHIPS_BLACKLIST, "aiwar");
        restrictedShips = new HashSet<>();
        for (int x = 0; x < csv.length(); x++)
        {
            JSONObject row = csv.getJSONObject(x);
            restrictedShips.add(row.getString("id"));
        }
    }
    
    @Override
    public boolean isIllegalOnSubmarket(CargoStackAPI stack, TransferAction action) {
        if (action == TransferAction.PLAYER_SELL) return true;
        return false;
    }

    @Override
    public boolean isIllegalOnSubmarket(String commodityId, TransferAction action) {
        if (action == TransferAction.PLAYER_SELL) return true;
        return false;
    }
    
    @Override
    public boolean isIllegalOnSubmarket(FleetMemberAPI member, TransferAction action) {
        if (action == TransferAction.PLAYER_SELL) return true;
        return false;
    }
    
    @Override
    public float getTariff() {
        return 1f;
    }
        
    @Override
    public String getIllegalTransferText(FleetMemberAPI member, TransferAction action) {
        return ILLEGAL_TRANSFER_MESSAGE;
    }
    
    @Override
    public String getIllegalTransferText(CargoStackAPI stack, TransferAction action)
    {
        return ILLEGAL_TRANSFER_MESSAGE;
    }
    
    protected String[] getFactionString()
    {
        String first = AIW_StringHelper.getString("aiw_zenithtrader", "factionsString");
        String f1 = faction1.getDisplayName();
        if (faction1 == faction2) {
            return new String[]{first, f1};
        }
        return new String[]{first, f1, faction2.getDisplayName()};
    }
    
    @Override
    public String getTooltipAppendix(CoreUIAPI ui)
    {
        String[] strings = getFactionString();
        String str = strings[0] + ": " + strings[1];
        if (strings.length == 3)
            str += ", " + strings[2];
        return str;
    }
    
    @Override
    public Highlights getTooltipAppendixHighlights(CoreUIAPI ui) {
        String[] strings = getFactionString();
        
        Highlights h = new Highlights();
        Color color1 = Misc.getHighlightColor();
        Color color2 = faction1.getBaseUIColor();
        String str1 = strings[0];
        String str2 = strings[1];
        if (strings.length == 3) {
            h.setText(str1, str2, strings[2]);
            h.setColors(color1, color2, faction2.getBaseUIColor());
        }
        else {
            h.setText(str1, str2);
            h.setColors(color1, color2);
        }
        return h;
    }
    
    @Override
    public boolean isEnabled(CoreUIAPI ui)
    {
        //return ui.getTradeMode() == CoreUITradeMode.OPEN;
        return true;
    }

    @Override
    public boolean isBlackMarket() {
        return false;
    }
    
    // override broken vanilla method in 0.7.2a (faction picker isn't actually used)
    @Override
    protected List<WeaponSpecAPI> getWeaponsOnRolePick(String role, WeightedRandomPicker<FactionAPI> factionPicker) {
        float qf = market.getShipQualityFactor();
        
        List<WeaponSpecAPI> result = new ArrayList<WeaponSpecAPI>();
        
        FactionAPI faction = submarket.getFaction();
        if (factionPicker != null && !factionPicker.isEmpty()) {
            faction = factionPicker.pick();
        }
        List<ShipRolePick> picks = faction.pickShip(role, qf, itemGenRandom);
        for (ShipRolePick pick : picks) {
            FleetMemberType type = FleetMemberType.SHIP;
            if (pick.isFighterWing()) {
                //type = FleetMemberType.FIGHTER_WING;
                continue;
            }
            
            FleetMemberAPI member = Global.getFactory().createFleetMember(type, pick.variantId);
            
            for (String slotId : member.getVariant().getFittedWeaponSlots()) {
                WeaponSlotAPI slot = member.getVariant().getSlot(slotId);
                if (slot.isBuiltIn() || slot.isSystemSlot() || slot.isDecorative()) continue;
                WeaponSpecAPI spec = member.getVariant().getWeaponSpec(slotId);
                if (spec.getAIHints().contains(AIHints.SYSTEM)) continue;
                result.add(spec);
            }
        }
        return result;
    }
}