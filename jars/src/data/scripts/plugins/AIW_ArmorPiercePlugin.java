package data.scripts.plugins;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CollisionClass;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ViewportAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.input.InputEventAPI;
import data.scripts.hullmods.TEM_LatticeShield;
import data.scripts.AIWarModPlugin;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.lazywizard.lazylib.CollisionUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

/**
 * Handles projectiles that go through hull
 * Borrowed wholesale from MShadowy's Shadowyards mod
 */
public class AIW_ArmorPiercePlugin extends BaseEveryFrameCombatPlugin {

    private static CombatEngineAPI engine;
    private static final Map<String, CollisionClass> originalCollisionClasses = new HashMap<>();
    private static final Map<HullSize, Float> mag = new HashMap<>();
    
    private static final float NONSHIELD_DAMAGE_MULT = 1.5f;
    // Sound to play while piercing a target's armor (should be loopable!)
    private static final String PIERCE_SOUND = "explosion_missile"; // TEMPORARY
    // Projectile ID (String), pierces shields (boolean)
    // Keep track of the original collision class (used for shield hits)
    private static final Color effectColor = new Color(165, 215, 145, 150);

    private static final Set<String> PROJ_IDS = new HashSet();

    static {
        // Add all projectiles that should pierce armor here
        // Format: Projectile ID (String), pierces shields (boolean)
        PROJ_IDS.add("zenith_heatray_shot");
    }
    
    /*static {
        mag.put(HullSize.FIGHTER, 1.0f);
        mag.put(HullSize.FRIGATE, 0.7f);
        mag.put(HullSize.DESTROYER, 0.5f);
        mag.put(HullSize.CRUISER, 0.3f);
        mag.put(HullSize.CAPITAL_SHIP, 0.25f);
    }*/

    @Override
    public void advance(float amount, List<InputEventAPI> events) {
        if (engine != Global.getCombatEngine()) {
            engine = Global.getCombatEngine();
            originalCollisionClasses.clear();
        }

        if (engine.isPaused()) {
            return;
        }

        // Scan all shots on the map for armor piercing projectiles
        for (DamagingProjectileAPI proj : engine.getProjectiles()) {
            String spec = proj.getProjectileSpecId();

            // Is this projectile armor piercing?
            if (!PROJ_IDS.contains(spec)) {
                continue;
            }

            // Register the original collision class (used for shield hits)
            if (!originalCollisionClasses.containsKey(spec)) {
                originalCollisionClasses.put(spec, proj.getCollisionClass());
            }

            // We'll do collision checks manually
            proj.setCollisionClass(CollisionClass.NONE);

            // Find nearby ships, missiles and asteroids
            List<CombatEntityAPI> toCheck = new ArrayList<>();
            toCheck.addAll(CombatUtils.getShipsWithinRange(proj.getLocation(), proj.getCollisionRadius() + 5f));
            toCheck.addAll(CombatUtils.getMissilesWithinRange(proj.getLocation(), proj.getCollisionRadius() + 5f));
            toCheck.addAll(CombatUtils.getAsteroidsWithinRange(proj.getLocation(), proj.getCollisionRadius() + 5f));

            // Don't include the ship that fired this projectile!
            toCheck.remove(proj.getSource());
            for (CombatEntityAPI entity : toCheck) {
                // Check for an active phase cloak
                if (entity instanceof ShipAPI) {
                    ShipSystemAPI cloak = ((ShipAPI) entity).getPhaseCloak();

                    if (cloak != null && cloak.isActive()) {
                        continue;
                    }
                }

                // Check for a shield hit
                if ((entity.getShield() != null && entity.getShield().isOn() && entity.getShield().isWithinArc(proj.getLocation()))) {
                    // If we hit a shield, enable collision
                    proj.setCollisionClass(originalCollisionClasses.get(spec));
                    // Stop the projectile (ensures a hit for fast projectiles)
                    proj.getVelocity().set(entity.getVelocity());
                    // Then move the projectile inside the ship's shield bounds
                    Vector2f.add((Vector2f) VectorUtils.getDirectionalVector(proj.getLocation(), entity.getLocation()).scale(5f), proj.getLocation(), proj.getLocation());
                } 
                // Check if the projectile is inside the entity's bounds
                else if (CollisionUtils.isPointWithinBounds(proj.getLocation(), entity)) {
                    // Calculate projectile speed
                    float speed = proj.getVelocity().length();

                    // Change how damamge is applied -- preferably a set % based on hull size as opposed to collission radius
                    // Damage per frame is based on how long it would take
                    // the projectile to travel through the entity
                    float modifier = 1.0f / ((entity.getCollisionRadius() * 2f) / speed);
                    
                    boolean hasLattice = false;
                    
                    if (entity instanceof ShipAPI)
                    {
                        ShipAPI ship = (ShipAPI)entity;
                        if (AIWarModPlugin.templarsExists && ship.getVariant().getHullMods().contains("tem_latticeshield") && TEM_LatticeShield.shieldLevel(ship) > 0f) {
                            hasLattice = true;
                        }
                        
                        if (proj.getDamageType() == DamageType.KINETIC) {
                            modifier *= ship.getMutableStats().getKineticShieldDamageTakenMult().getModifiedValue();
                        }
                        else if (proj.getDamageType() == DamageType.HIGH_EXPLOSIVE) {
                            modifier *= ship.getMutableStats().getEnergyShieldDamageTakenMult().getModifiedValue();
                        }
                        else if (proj.getDamageType() == DamageType.FRAGMENTATION) {
                            modifier *= ship.getMutableStats().getFragmentationShieldDamageTakenMult().getModifiedValue();
                        }
                        else {
                            modifier *= ship.getMutableStats().getEnergyShieldDamageTakenMult().getModifiedValue();
                        }
                        
                        ShipAPI source = proj.getSource();

                        if (source != null)
                        {
                            WeaponAPI weapon = proj.getWeapon();
                            WeaponAPI.WeaponType type = WeaponAPI.WeaponType.UNIVERSAL;
                            if (weapon != null) type = weapon.getSlot().getWeaponType();
                            if (type == WeaponAPI.WeaponType.ENERGY) {
                                modifier *= source.getMutableStats().getEnergyWeaponDamageMult().getModifiedValue();
                            }
                            else if (type == WeaponAPI.WeaponType.BALLISTIC) {
                                modifier *= source.getMutableStats().getBallisticWeaponDamageMult().getModifiedValue();
                            }
                            else if (type == WeaponAPI.WeaponType.MISSILE) {
                                modifier *= source.getMutableStats().getMissileWeaponDamageMult().getModifiedValue();
                            }
                        }
                    }
                    float damage = (proj.getDamageAmount() * amount) * modifier * NONSHIELD_DAMAGE_MULT;
                    float emp = (proj.getEmpAmount() * amount) * modifier;

                    DamageType dmgType = DamageType.HIGH_EXPLOSIVE; //proj.getDamageType();
                    if (hasLattice) dmgType = DamageType.ENERGY;
                    
                    // Apply damage and slow the projectile
                    // Note: BALLISTIC_AS_BEAM projectiles won't be slowed!
                    engine.applyDamage(entity, proj.getLocation(), damage, dmgType, emp, true, true, proj.getSource());
                    proj.getVelocity().scale(1.0f - (amount * 1.5f));

                    // Render the hit
                    engine.spawnExplosion(proj.getLocation(), entity.getVelocity(), effectColor, speed * amount * 2f, .5f);

                    // Play piercing sound (only one sound active per projectile)
                    Global.getSoundPlayer().playLoop(PIERCE_SOUND, proj, 1f, 1f, proj.getLocation(), entity.getVelocity());
                }
            }
        }
    }

    @Override
    public void init(CombatEngineAPI engine) {
    }
    
    @Override
    public void renderInUICoords(ViewportAPI viewport) {
    }
     
    @Override
    public void renderInWorldCoords(ViewportAPI viewport) {  
    }
}
