package data.scripts.plugins;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.BeamAPI;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.CombatFleetManagerAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.ShieldAPI;
import com.fs.starfarer.api.combat.ShieldAPI.ShieldType;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ViewportAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.graphics.SpriteAPI;
import com.fs.starfarer.api.input.InputEventAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

public class AIW_ForcefieldPlugin extends BaseEveryFrameCombatPlugin {
    
    protected static final String DATA_KEY = "AIW_ForcefieldUnits";
    protected static final String STATS_CSV = "data/config/AIWar/forcefield_stats.csv";
    protected static final String HARD_FLUX_CSV = "data/config/AIWar/hard_flux_defs.csv";
    protected static final float FORCEFIELD_SPRITE_SIZE = 1024;
    protected static final float FORCEFIELD_ROTATION_SPEED = 10;
    protected static final float FORCEFIELD_CONTRACT_SPEED_MULT = 3f;
    protected static final float FORCEFIELD_BEAM_DAMAGE_MULT = 1.0f/30f;
    protected static final Color FORCEFIELD_COLOR = new Color(1,1,1,0.75f);
    protected static final float DAMAGE_CONTRACTION_MULT = 0.01f;
    protected static final float BLOSSOM_FIELD_DAMAGE_MULT = 0.5f;
    protected static final float BLOSSOM_FIELD_RADIUS_MULT = 3f;
    protected static final float SHIELD_RADIUS_MULT = 0.95f;    // makes the boundaries line up
    protected static final float AI_SHUTDOWN_AT_RADIUS_MULT = 1.2f;
    
    protected static final Vector2f ZERO = new Vector2f();
    
    protected static Map<String, ForcefieldDef> forcefieldDefs;
    protected static Map<String, Float> hardFluxDefs;
    
    protected final IntervalUtil tracker = new IntervalUtil(0.2f, 0.3f);
    
    public static Logger log = Global.getLogger(AIW_ForcefieldPlugin.class);
    protected CombatEngineAPI engine;
    
    protected boolean firstRun = true;
    
    @Override
    public void advance(float amount, List<InputEventAPI> events)
    {
        if (engine == null)
        {
            return;
        }

        if (engine.isPaused())
        {
            return;
        }
        Map<ShipAPI, ForcefieldInfo> forcefieldData = (HashMap<ShipAPI, ForcefieldInfo>)engine.getCustomData().get(DATA_KEY);
        if (forcefieldData.isEmpty()) return;
        
        // any ship with no shield gets removed from the list
        if (firstRun)
        {
            List<ShipAPI> toRemove = new ArrayList<>();
            Iterator<Map.Entry<ShipAPI, ForcefieldInfo>> iter = forcefieldData.entrySet().iterator();
            while (iter.hasNext())
            {
                Map.Entry<ShipAPI, ForcefieldInfo> tmp = iter.next();
                ShipAPI ship = tmp.getKey();
                ShieldAPI shield = ship.getShield();

                if (shield == null) // shield bypass hullmod
                {
                    toRemove.add(ship);
                }    
            }
            for (ShipAPI ship : toRemove)
                forcefieldData.remove(ship);
            
            firstRun = false;
        }
        
        // handle beams
        // buggy; fan beams will wtfomg it alive
        List<BeamAPI> beams = engine.getBeams();
        for (BeamAPI beam : beams)
        {
            //if (!beam.didDamageThisFrame()) return;
            CombatEntityAPI target = beam.getDamageTarget();
            if (!(target instanceof ShipAPI)) continue;
            ShipAPI ship = (ShipAPI)target;
            
            ForcefieldInfo info = forcefieldData.get(ship);
            if (info == null) continue;
            ShieldAPI shield = ship.getShield();
            if (shield == null) continue;
            if (shield.isOff()) continue;
            if (!shield.isWithinArc(beam.getTo())) continue;
            
            ForcefieldDef def = forcefieldDefs.get(ship.getHullSpec().getHullId());
            /*
            WeaponAPI weapon = beam.getWeapon();
            DamageAPI damage = weapon.getDamage();
            MutableShipStatsAPI attackerStats = damage.getStats();
            MutableShipStatsAPI shipStats = ship.getMutableStats();
            
            float damageAmount = damage.getDamage() * attackerStats.getBeamWeaponDamageMult().modified * FORCEFIELD_BEAM_DAMAGE_MULT;
            engine.addFloatingText(ship.getLocation(), "" + damage.getMultiplier(), 24, Color.YELLOW, ship, 0, 0);
            if (damage.getType() == DamageType.KINETIC) {
                damageAmount *= 2f * shipStats.getKineticShieldDamageTakenMult().getModifiedValue();
            }
            else if (damage.getType() == DamageType.HIGH_EXPLOSIVE) {
                damageAmount *= 0.5f * shipStats.getKineticShieldDamageTakenMult().getModifiedValue();
            }
            else if (damage.getType() == DamageType.FRAGMENTATION) {
                damageAmount *= 0.25f * shipStats.getFragmentationShieldDamageTakenMult().getModifiedValue();
            }
            else {
                damageAmount *= shipStats.getEnergyShieldDamageTakenMult().getModifiedValue();
            }

            if (info.blossomActive) damageAmount *= BLOSSOM_FIELD_DAMAGE_MULT;
            
            WeaponType type = WeaponType.UNIVERSAL;
            if (weapon != null) type = weapon.getSlot().getWeaponType();
            if (type == WeaponType.ENERGY) {
                damageAmount *= attackerStats.getEnergyWeaponDamageMult().getModifiedValue();
            }
            else if (type == WeaponType.BALLISTIC) {
                damageAmount *= attackerStats.getBallisticWeaponDamageMult().getModifiedValue();
            }
            else if (type == WeaponType.MISSILE) {
                damageAmount *= attackerStats.getMissileWeaponDamageMult().getModifiedValue();
            }
            
            damageAmount *= attackerStats.getDamageToTargetShieldsMult().getModifiedValue();
            damageAmount *= shipStats.getShieldDamageTakenMult().getModifiedValue();

            float contraction = damageAmount * def.radiusEfficiency * DAMAGE_CONTRACTION_MULT;
            info.wantedRadius -= contraction;
            info.radius -= contraction;
            */
            info.regrowDelay = def.regrowDelay;
        }
        
        // do most forcefield stuff
        Iterator<Map.Entry<ShipAPI, ForcefieldInfo>> iter2 = forcefieldData.entrySet().iterator();
        while (iter2.hasNext())
        {
            Map.Entry<ShipAPI, ForcefieldInfo> tmp = iter2.next();
            ShipAPI ship = tmp.getKey();
            if (!engine.isEntityInPlay(ship)) continue;
            if (!ship.isAlive()) continue;

            ForcefieldInfo info = tmp.getValue();
            ForcefieldDef def = forcefieldDefs.get(ship.getHullSpec().getHullId());
            ShieldAPI shield = ship.getShield();
            
            float growTime = def.growTime / ship.getMutableStats().getShieldUnfoldRateMult().modified;
            
            info.rotAngle += amount * FORCEFIELD_ROTATION_SPEED;
            
            if (ship.getFluxTracker().isOverloadedOrVenting())
            {
                info.radius = 0;
                info.active = false;
                info.wantActive = false;
            }
            
            info.deadTime -= amount;
            if (info.deadTime <= 0) {
                info.deadTime = 0;
                shield.setType(ShieldType.FRONT);
            }
            
            // make shield bigger if applicable
            float maxRadius = def.radius;
            if (info.blossomActive) maxRadius *= BLOSSOM_FIELD_RADIUS_MULT;
            
            info.regrowDelay -= amount;
            if (info.regrowDelay <= 0)
            {
                info.wantedRadius += (maxRadius/growTime) * amount;
                if (info.wantedRadius > maxRadius) info.wantedRadius = maxRadius;
                info.regrowDelay = 0;
            }
            
            if (shield.isOn())
            {
                if (info.deadTime > 0)
                    shield.toggleOff();
                else if (!info.wantActive)
                {
                    activateForcefield(ship);
                    //engine.addFloatingText(ship.getLocation(), "Forcefield activating", 24, Color.YELLOW, ship, 0, 0);
                }
            }
            else if (info.wantActive)
            {
                deactivateForcefield(ship);
                //shield.toggleOff();    // keep it on till the forcefield actually reaches its deactivated state
                //engine.addFloatingText(ship.getLocation(), "Forcefield deactivating", 24, Color.YELLOW, ship, 0, 0);
            }
            
            if (info.active)
            {
                // estimate shield contraction based on damage taken
                float currHardFlux = ship.getFluxTracker().getHardFlux();
                float hardFluxDelta = currHardFlux - info.lastHardFlux;
                if (hardFluxDelta > 0)
                {
                    float damage = hardFluxDelta/shield.getFluxPerPointOfDamage();
                    float contraction = damage * def.radiusEfficiency * DAMAGE_CONTRACTION_MULT;
                    info.wantedRadius -= contraction;
                    info.radius -= contraction;
                    info.regrowDelay = def.regrowDelay;
                    
                }
                
                if (info.wantedRadius < def.failureRadius)    // shield failure
                {
                    info.radius = 0;
                    info.active = false;
                    info.wantActive = false;
                    info.deadTime = def.deadTime;

                    String sound = "disabled_small_crit";
                    if (ship.isCapital()) sound = "disabled_large_crit";
                    else if (ship.isCruiser()) sound = "disabled_medium_crit";
                    //Global.getSoundPlayer().playSound(sound, 2, 4, ship.getLocation(), ship.getVelocity());
                    Global.getSoundPlayer().playUISound(sound, 2, 2);

                    shield.setType(ShieldType.NONE);
                    engine.spawnExplosion(ship.getLocation(), ZERO, Color.CYAN, def.radius * 0.5f, def.deadTime * 0.25f);
                }
                
                // shrink shield (if we're currently collapsing it)
                if (!info.wantActive)
                {
                    info.radius -= (def.radius/growTime) * amount * FORCEFIELD_CONTRACT_SPEED_MULT;
                    if (info.radius <= 0) 
                    {
                        info.radius = 0;
                        info.active = false;
                    }
                }
                else if (info.radius < info.wantedRadius)
                {
                    info.radius += (maxRadius/growTime) * amount;
                    if (info.radius > info.wantedRadius) info.radius = info.wantedRadius;
                }
                else
                {
                    info.radius -= (maxRadius/growTime) * amount * FORCEFIELD_CONTRACT_SPEED_MULT;
                    if (info.radius < info.wantedRadius) info.radius = info.wantedRadius;
                }
            }
            info.lastHardFlux = ship.getFluxTracker().getHardFlux();
            if (info.active != shield.isOn()) 
            {
                shield.toggleOff();
            }
            if (info.active) 
            {
                if (shield.getActiveArc() < 360) shield.setActiveArc(360);
                shield.setRadius(info.radius * SHIELD_RADIUS_MULT);
            }
        }
    }
       
    @Override
    public void renderInWorldCoords(ViewportAPI viewport)
    {
        if (engine == null)
        {
            return;
        }
        Map<ShipAPI, ForcefieldInfo> forcefieldData = (HashMap<ShipAPI, ForcefieldInfo>)engine.getCustomData().get(DATA_KEY);
        
        Iterator<Map.Entry<ShipAPI, ForcefieldInfo>> iter = forcefieldData.entrySet().iterator();
        while (iter.hasNext())
        {
            Map.Entry<ShipAPI, ForcefieldInfo> tmp = iter.next();
            ShipAPI ship = tmp.getKey();
            if (!engine.isEntityInPlay(ship)) continue;
            if (!ship.isAlive()) continue;
            if (ship.getFluxTracker().isOverloadedOrVenting()) continue;
            
            ForcefieldInfo info = tmp.getValue();
            Vector2f pos = ship.getLocation();
            if (!viewport.isNearViewport(pos, info.radius)) continue;
            if (!info.active) continue;
            
            ForcefieldDef def = forcefieldDefs.get(ship.getHullSpec().getHullId());
            
            SpriteAPI sprite = Global.getSettings().getSprite("AIW_forcefield", "forcefield");
            sprite.setSize(info.radius*2, info.radius*2);
            sprite.setAngle(info.rotAngle);
            //sprite.setAdditiveBlend();
            sprite.setColor(FORCEFIELD_COLOR);
            if (!info.wantActive)
            {
                sprite.setColor(new Color(1,1,1, 0.25f + 0.25f*info.radius/def.radius));
            }
            sprite.renderAtCenter(pos.x, pos.y);
        }
    }
    
    @Override
    public void init(CombatEngineAPI engine)
    {
        this.engine = engine;
        engine.getCustomData().put(DATA_KEY, new HashMap<ShipAPI, ForcefieldInfo>());  
        forcefieldDefs = new HashMap<>();
        hardFluxDefs = new HashMap<>();
        
        try {
            JSONArray csv = Global.getSettings().getMergedSpreadsheetDataForMod("hull_id", STATS_CSV, "aiwar");
            for(int x = 0; x < csv.length(); x++)
            {
                JSONObject row = csv.getJSONObject(x);
                ForcefieldDef def = new ForcefieldDef();
                def.radius = (float)row.optDouble("radius", 250);
                def.fluxEfficiency = (float)row.optDouble("flux_efficiency", 0.25);
                def.radiusEfficiency = (float)row.optDouble("radius_efficiency", 1);
                def.growTime = (float)row.optDouble("growTime", 3);
                def.regrowDelay = (float)row.optDouble("regrowDelay", 5);
                def.failureRadius = (float)row.optDouble("failureRadius", 75);
                def.deadTime = (float)row.optDouble("deadTime", 8);
                def.upkeep = (float)row.optDouble("upkeep", 0);
                forcefieldDefs.put(row.getString("hull_id"), def);
            }
            
            JSONArray csvHardFlux = Global.getSettings().getMergedSpreadsheetDataForMod("weapon_id", HARD_FLUX_CSV, "aiwar");
            for(int x = 0; x < csvHardFlux.length(); x++)
            {
                JSONObject row = csvHardFlux.getJSONObject(x);
                hardFluxDefs.put(row.getString("weapon_id"), (float)row.getDouble("flux"));
            }
        } catch (Exception ex) {
            log.error(ex);
        }
        
        log.info("Forcefield plugin initialized");
    }
    
    // TODO
    protected void handleAI(float amount)
    {
        // AI
        tracker.advance(amount);
        if (!tracker.intervalElapsed()) return;
        
        Map<ShipAPI, ForcefieldInfo> forcefieldData = (HashMap<ShipAPI, ForcefieldInfo>)engine.getCustomData().get(DATA_KEY);
        Iterator<Map.Entry<ShipAPI, ForcefieldInfo>> iter = forcefieldData.entrySet().iterator();
        while (iter.hasNext())
        {
            Map.Entry<ShipAPI, ForcefieldInfo> tmp = iter.next();
            ShipAPI ship = tmp.getKey();
            if (!engine.isEntityInPlay(ship)) continue;
            if (!ship.isAlive()) continue;
            if (ship.getShipAI() == null) return;

            ForcefieldInfo info = tmp.getValue();
            ForcefieldDef def = forcefieldDefs.get(ship.getHullSpec().getHullId());
            ShieldAPI shield = ship.getShield();
            
            if (info.radius < def.failureRadius * AI_SHUTDOWN_AT_RADIUS_MULT)
            {
                if (shield.isOn()) shield.toggleOff();
            }
        }
    }
    
    protected void playImpactSound(Vector2f point, float damage, DamageType damageType)
    {
        String typeString = "gun";
        if (damageType == DamageType.ENERGY) typeString = "energy";
        String strengthString = "light";
        if (damage >= 450) strengthString = "heavy";
        else if (damage >= 200) strengthString = "solid";
        
        Global.getSoundPlayer().playSound("hit_shield_" + strengthString + "_" + typeString, 1f, 1f, point, ZERO);
    }
    
    public static void addForcefieldShip(ShipAPI ship)
    {
        if (Global.getCombatEngine() == null) return;
        HashMap<ShipAPI, ForcefieldInfo> data = (HashMap<ShipAPI, ForcefieldInfo>)Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (data == null) return;
        
        // TODO
        ForcefieldDef def = forcefieldDefs.get(ship.getHullSpec().getHullId());
        ForcefieldInfo newFF = new ForcefieldInfo(def.radius);
        data.put(ship, newFF);
    }
    
    public static boolean isForcefieldDead(ShipAPI ship)
    {
        if (Global.getCombatEngine() == null) return false;
        HashMap<ShipAPI, ForcefieldInfo> data = (HashMap<ShipAPI, ForcefieldInfo>)Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (data == null) return false;
        if (!data.containsKey(ship)) return false;
        
        return data.get(ship).deadTime > 0;
    }
    
    public static boolean isForcefieldActive(ShipAPI ship)
    {
        if (Global.getCombatEngine() == null) return false;
        HashMap<ShipAPI, ForcefieldInfo> data = (HashMap<ShipAPI, ForcefieldInfo>)Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (data == null) return false;
        if (!data.containsKey(ship)) return false;
        
        return data.get(ship).active;
    }
    
    public static boolean isForcefieldWantActive(ShipAPI ship)
    {
        if (Global.getCombatEngine() == null) return false;
        HashMap<ShipAPI, ForcefieldInfo> data = (HashMap<ShipAPI, ForcefieldInfo>)Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (data == null) return false;
        if (!data.containsKey(ship)) return false;
        
        return data.get(ship).wantActive;
    }
    
    public static float getForcefieldRegrowDelay(ShipAPI ship)
    {
        if (Global.getCombatEngine() == null) return 0;
        HashMap<ShipAPI, ForcefieldInfo> data = (HashMap<ShipAPI, ForcefieldInfo>)Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (data == null) return 0;
        if (!data.containsKey(ship)) return 0;
        
        return data.get(ship).regrowDelay;
    }
    
    
    static void activateForcefield(ShipAPI ship, ForcefieldInfo info)
    {
        if (info.deadTime > 0) return;
        
        //if (ship.controlsLocked()) return;
        
        CombatFleetManagerAPI fleetManager = Global.getCombatEngine().getFleetManager(ship.getOriginalOwner());
        if (fleetManager != null)
        {
            List<FleetMemberAPI> fleetMembers = fleetManager.getDeployedCopy();
            for (FleetMemberAPI member : fleetMembers)
            {
                if (member.getId().equals(ship.getFleetMemberId()) && member.isMothballed())
                    return;
            }
        }
        
        //if (!info.wantActive) Global.getSoundPlayer().playUISound("shield_raise", 1, 1);
        info.active = true;
        info.wantActive = true;
    }
    
    public static void activateForcefield(ShipAPI ship)
    {
        if (Global.getCombatEngine() == null) return;
        if (ship.getFluxTracker().isOverloadedOrVenting()) return;
        
        HashMap<ShipAPI, ForcefieldInfo> data = (HashMap<ShipAPI, ForcefieldInfo>)Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (data == null) return;
        
        ForcefieldInfo info = data.get(ship);
        activateForcefield(ship, info);
    }
    
    static void deactivateForcefield(ShipAPI ship, ForcefieldInfo info)
    {
        //if (info.blossomActive) return; // can't turn off shield while blossoming
        //if (info.wantActive) Global.getSoundPlayer().playUISound("shield_lower", 1, 1);
        info.wantActive = false;
    }
    
    public static void deactivateForcefield(ShipAPI ship)
    {
        if (Global.getCombatEngine() == null) return;
        if (ship.getFluxTracker().isOverloadedOrVenting()) return;
        
        HashMap<ShipAPI, ForcefieldInfo> data = (HashMap<ShipAPI, ForcefieldInfo>)Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (data == null) return;
        
        ForcefieldInfo info = data.get(ship);
        deactivateForcefield(ship, info);
    }
    
    public static void toggleForcefield(ShipAPI ship)
    {
        if (Global.getCombatEngine() == null) return;
        if (ship.getFluxTracker().isOverloadedOrVenting()) return;
        
        HashMap<ShipAPI, ForcefieldInfo> data = (HashMap<ShipAPI, ForcefieldInfo>)Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (data == null) return;
        
        ForcefieldInfo info = data.get(ship);
        float currTime = Global.getCombatEngine().getTotalElapsedTime(false);
        if (currTime < info.lastToggled + 0.1f) return; // prevents flicker
        
        if (info.wantActive)
        {
            deactivateForcefield(ship, info);
        }
        else
        {
            activateForcefield(ship, info);
        }
        info.lastToggled = currTime;
    }
    
    public static void setBlossomFieldState(ShipAPI ship, boolean state)
    {
        if (Global.getCombatEngine() == null) return;
        if (ship == null) return;   // yeah... seriously
        if (ship.getFluxTracker().isOverloadedOrVenting())
            state = false;
        
        HashMap<ShipAPI, ForcefieldInfo> data = (HashMap<ShipAPI, ForcefieldInfo>)Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (data == null) return;
        
        ForcefieldInfo info = data.get(ship);
        info.blossomActive = state;
        if (state == true)
        {
            if (!info.active) activateForcefield(ship);
            info.wantedRadius = forcefieldDefs.get(ship.getHullSpec().getHullId()).radius * BLOSSOM_FIELD_RADIUS_MULT;
            info.lastHardFlux = ship.getFluxTracker().getHardFlux();
        }
        else
        {
            info.wantedRadius = Math.min(info.wantedRadius, forcefieldDefs.get(ship.getHullSpec().getHullId()).radius);
        }
    }
    
    public static float getForcefieldDefRadius(String hullId)
    {
        if (forcefieldDefs == null) return 0;
        ForcefieldDef def = forcefieldDefs.get(hullId);
        if (def == null) return 0;
        return def.radius;
    }
    
    
    public static class ForcefieldInfo
    {
        boolean active = false;
        boolean wantActive = false;
        boolean blossomActive = false;
        float radius = 0;
        float wantedRadius = 0;
        float rotAngle = 0;
        float regrowDelay = 0;
        float deadTime = 0;
        float lastHardFlux = 0;
        float lastToggled = 0;
        
        public ForcefieldInfo(float radius)
        {
            //this.radius = radius;
            wantedRadius = radius;
            rotAngle = MathUtils.getRandomNumberInRange(0f, 360f);
        }
    }
    
    public static class ForcefieldDef
    {
        float radius;
        float fluxEfficiency;
        float radiusEfficiency;
        float growTime;
        float regrowDelay;
        float failureRadius;
        float deadTime;
        float upkeep;
    }
    
    public static class ProjectileToKillInfo
    {
        DamagingProjectileAPI projectile;
        float damage;
        ShipAPI ship;
        ForcefieldInfo forcefield;
        
        public ProjectileToKillInfo(DamagingProjectileAPI projectile, float damage, ShipAPI ship, ForcefieldInfo forcefield)
        {
            this.projectile = projectile;
            this.damage = damage;
            this.ship = ship;
            this.forcefield = forcefield;
        }
    }
}
