package data.scripts.plugins;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ViewportAPI;
import com.fs.starfarer.api.graphics.SpriteAPI;
import com.fs.starfarer.api.input.InputEventAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import static data.scripts.plugins.AIW_GravityDrainPlugin.DATA_KEY;
import data.scripts.util.AIWTwig;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.dark.shaders.distortion.DistortionShader;
import org.dark.shaders.distortion.RippleDistortion;
import org.dark.shaders.distortion.WaveDistortion;
import org.dark.shaders.util.ShaderLib;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

public class AIW_GravityDrainPlugin extends BaseEveryFrameCombatPlugin {
    
    protected static final String DATA_KEY = "AIW_GravityDrainUnits";
    protected static final String DATA_FX_KEY = "AIW_GravityDrainRings";
    protected static final Vector2f ZERO = new Vector2f();
    public static float DRAIN_RADIUS = 900;
    protected static float DRAIN_FX_RADIUS = 900;
    protected static float DRAIN_FX_ROTATION_SPEED = 20;
    public static float DRAIN_SLOW_FACTOR = 0.5f;   // 1f = completely immobilized
    protected static float DRAIN_DISTORTION_INTENSITY = 6;
    protected static float DRAIN_RING_PERIOD = 0.5f;
	protected static float DISTORTION_PERIOD = 0.75f;
    protected static float DRAIN_RING_TTL = 2f;
    protected static final Color HIT_COLOR = new Color(0.25f, 0.25f, 0.25f, 0.25f);
    
    public static Logger log = Global.getLogger(AIW_GravityDrainPlugin.class);
    protected CombatEngineAPI engine;
        
    protected IntervalUtil ringInterval = new IntervalUtil(0.5f, 0.5f);
	protected IntervalUtil distortInterval = new IntervalUtil(0.95f, 1.05f);
	private boolean addGrav = false;
           
    @Override
    public void advance(float amount, List<InputEventAPI> events)
    {
        if (engine == null)
        {
            return;
        }

        if (engine.isPaused())
        {
            return;
        }
                
        List<RingInfo> ringData = (List<RingInfo>)engine.getCustomData().get(DATA_FX_KEY);
        Map<ShipAPI, DrainInfo> drainData = (HashMap<ShipAPI, DrainInfo>)engine.getCustomData().get(DATA_KEY);
        if (drainData == null) return;
        
        boolean addRing = false;
        ringInterval.advance(amount);
        if (ringInterval.intervalElapsed())
        {
            addRing = true;
        }
		
		boolean addDistort = false;
		distortInterval.advance(amount);
        if (distortInterval.intervalElapsed())
        {
            addDistort = true;
        }
        
        List<RingInfo> ringsToRemove = new ArrayList<>();
        for (RingInfo ring : ringData)
        {
            ring.ttl -= amount;
            if (ring.ttl <= 0) ringsToRemove.add(ring);
        }
        for (RingInfo toRemove : ringsToRemove)
        {
            ringData.remove(toRemove);
        }

        Iterator<Map.Entry<ShipAPI, DrainInfo>> iter0 = drainData.entrySet().iterator();
        while (iter0.hasNext())
        {
            Map.Entry<ShipAPI, DrainInfo> tmp = iter0.next();
            ShipAPI ship = tmp.getKey();
            DrainInfo info = tmp.getValue();
            
            List<ShipAPI> entities = CombatUtils.getShipsWithinRange(ship.getLocation(), DRAIN_RADIUS);
            int size = entities.size();
            for (int i = 0; i < size; i++) {
                ShipAPI entity = entities.get(i);
                if (entity.getOwner() == ship.getOwner()) {
                    // Don't affect friendly entities
                    continue;
                }
                if (!AIWTwig.isRoot((ShipAPI) entity)) {
                    continue;
                }

                Vector2f velocity = entity.getVelocity();
                float speed = (float)Math.sqrt(velocity.x * velocity.x + velocity.y * velocity.y);
                float maxSpeed = entity.getMutableStats().getMaxSpeed().getModifiedValue();
                float maxSpeedNew = maxSpeed * (1f - (info.effectLevel * DRAIN_SLOW_FACTOR));
                if (speed > maxSpeedNew) 
                {
                    velocity.scale(maxSpeedNew/speed);
                    //Global.getCombatEngine().addHitParticle(entity.getLocation(), ZERO, entity.getCollisionRadius() * 0.75f, .4f, 0.25f, HIT_COLOR);
                }
            }
            
            //info.alpha = (float)Math.cos(elapsed) * 0.25f;
            //if (info.alpha < 0) info.alpha = -info.alpha;
            //info.alpha += 0.25f;
            info.alpha = 0.4f;
            info.alpha *= info.effectLevel;
            info.rotation += amount * DRAIN_FX_ROTATION_SPEED;
            
            if (addRing)
            {
                ringData.add(new RingInfo(ship.getLocation().x, ship.getLocation().y));
			}
			if (addDistort)
			{
				RippleDistortion distortion = new RippleDistortion(ship.getLocation(), ship.getVelocity());
                distortion.setIntensity(DRAIN_DISTORTION_INTENSITY);
				//distortion.flip(Math.random() > 0.5f);
                distortion.setSize(DRAIN_RADIUS);
				distortion.flip(true);
				distortion.setFrameRate(60f/DRAIN_RING_TTL);
				distortion.fadeOutIntensity(DRAIN_RING_TTL * 1.4f);
				distortion.fadeInSize(DRAIN_RING_TTL);
                DistortionShader.addDistortion(distortion);
            }
            //engine.addFloatingText(ship.getLocation(), info.effectLevel + "", 24, Color.YELLOW, ship, 0, 0);
        }
    }
        
    @Override
    public void renderInWorldCoords(ViewportAPI viewport)
    {
        if (engine == null)
        {
            return;
        }
        Map<ShipAPI, DrainInfo> drainData = (HashMap<ShipAPI, DrainInfo>)engine.getCustomData().get(DATA_KEY);
        if (drainData == null) return;
        
        Iterator<Map.Entry<ShipAPI, DrainInfo>> iter0 = drainData.entrySet().iterator();
        while (iter0.hasNext())
        {
            Map.Entry<ShipAPI, DrainInfo> tmp = iter0.next();
            ShipAPI ship = tmp.getKey();
            DrainInfo info = tmp.getValue();
            
            Vector2f pos = ship.getLocation();
            SpriteAPI sprite = Global.getSettings().getSprite("AIW_gravityDrain", "gravity");
            sprite.setSize(DRAIN_FX_RADIUS*2, DRAIN_FX_RADIUS*2);
            sprite.setColor(new Color(1, 1, 1, info.alpha));
            sprite.setAngle(info.rotation);
            sprite.renderAtCenter(pos.x, pos.y);
        }
        
        List<RingInfo> ringData = (List<RingInfo>)engine.getCustomData().get(DATA_FX_KEY);
        for (RingInfo ring : ringData)
        {
            float strength = ring.ttl / DRAIN_RING_TTL;
            float alpha = (1 - strength);
            if (alpha > 0.5f) alpha = 1 - alpha;
            alpha *= 0.5f;
            SpriteAPI sprite = Global.getSettings().getSprite("AIW_gravityDrain", "gravring");
            sprite.setAdditiveBlend();
            sprite.setSize(DRAIN_FX_RADIUS*2*strength, DRAIN_FX_RADIUS*2*strength);
            sprite.setColor(new Color(1, 1, 1, alpha));
            sprite.renderAtCenter(ring.x, ring.y);
        }
    }

    @Override
    public void init(CombatEngineAPI engine)
    {
        this.engine = engine;
        engine.getCustomData().put(DATA_KEY, new HashMap<ShipAPI, DrainInfo>());
        engine.getCustomData().put(DATA_FX_KEY, new ArrayList<RingInfo>());  
        
        log.info("Gravity drain plugin initialized");
    }
        
    public static void addOrUpdateGravityDrain(ShipAPI ship, float effectLevel)
    {
        if (Global.getCombatEngine() == null) return;
        HashMap<ShipAPI, DrainInfo> data = (HashMap<ShipAPI, DrainInfo>)Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (data == null) return;
        
        if (!data.containsKey(ship)) 
        {
            DrainInfo newDrain = new DrainInfo();
            data.put(ship, newDrain);
        }
        else
        {
            data.get(ship).effectLevel = effectLevel;
        }
    }
    
    public static void removeGravityDrain(ShipAPI ship)
    {
        if (Global.getCombatEngine() == null) return;
        HashMap<ShipAPI, DrainInfo> data = (HashMap<ShipAPI, DrainInfo>)Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (data == null) return;
        
        if (data.containsKey(ship)) 
        {
            data.remove(ship);
        }
    }
        
    public static class DrainInfo
    {
        public float rotation = 0;
        //public float radius = 0;
        public float effectLevel = 0;
        public float alpha = 0;
    }
    
    public static class RingInfo
    {
        public float x = 0;
        public float y = 0;
        public float ttl = DRAIN_RING_TTL;
        
        public RingInfo(float x, float y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
