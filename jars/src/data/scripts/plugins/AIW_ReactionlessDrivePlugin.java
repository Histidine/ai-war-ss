package data.scripts.plugins;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.ViewportAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.graphics.SpriteAPI;
import com.fs.starfarer.api.input.InputEventAPI;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import org.lwjgl.util.vector.Vector2f;

@Deprecated
public class AIW_ReactionlessDrivePlugin implements EveryFrameCombatPlugin {
	
	protected static final DriveFXSpriteDef DRIVE_FX_DEF;	// FIXME allow more than one
	protected static AIW_ReactionlessDrivePlugin plugin;
	
	protected List<DriveFXSprite> sprites = new ArrayList<>();
	protected CombatEngineAPI engine;
	
	static {
		DRIVE_FX_DEF = new DriveFXSpriteDef();
		DRIVE_FX_DEF.image = "driveRing";
	}
	
	public static AIW_ReactionlessDrivePlugin getPlugin()
	{
		return plugin;
	}
	
	public void spawnSprite(WeaponAPI weapon, boolean isZeroFlux)
	{
		sprites.add(new DriveFXSprite(DRIVE_FX_DEF, weapon, isZeroFlux));
	}
	
	@Override
	public void advance(float amount, List<InputEventAPI> events) {
		if (engine == null) return;
		if (engine.isPaused()) return;
				
		List<DriveFXSprite> toRemove = new ArrayList<>();
		
		// update sprite details
		for (DriveFXSprite sprite : sprites)
		{
			sprite.age += amount;
			if (sprite.age > sprite.ttl)
			{
				toRemove.add(sprite);
				continue;
			}
			sprite.currentDistance = sprite.distance * sprite.age/sprite.ttl;
						
			if (sprite.age <= sprite.def.peakTime)
			{
				float t1 = sprite.def.peakTime - sprite.age;
				float t2 = sprite.age;
				sprite.alpha = (sprite.def.startAlpha*t1 + sprite.def.peakAlpha*t2)/(t1+t2);
				sprite.sizeMult = (sprite.def.startSizeMult*t1 + sprite.def.peakSizeMult*t2)/(t1+t2);
			}
			else if (sprite.age > sprite.def.peakTime)
			{
				float t1 = sprite.ttl * sprite.age;
				float t2 = sprite.age - sprite.def.peakTime;
				sprite.alpha = (sprite.def.peakAlpha*t1 + sprite.def.endAlpha*t2)/(t1+t2);
				sprite.sizeMult = (sprite.def.peakSizeMult*t1 + sprite.def.endSizeMult*t2)/(t1+t2);
			}
			
			if (sprite.alpha > 1) sprite.alpha = 1;
			if (sprite.alpha < 0) sprite.alpha = 0;
			
			sprite.rotation = sprite.weapon.getCurrAngle();
		}
		
		for (DriveFXSprite sprite : toRemove)
		{
			sprites.remove(sprite);
		}
	}

	@Override
	public void renderInWorldCoords(ViewportAPI vapi) {
		for (DriveFXSprite spriteEntry : sprites)
		{
			SpriteAPI sprite = Global.getSettings().getSprite("AIW_reactionlessDrive", spriteEntry.def.image);
            //sprite.setAdditiveBlend();
			float sizeX = spriteEntry.def.sizeX * spriteEntry.sizeMult;
			float sizeY = spriteEntry.def.sizeY * spriteEntry.sizeMult;
			if (spriteEntry.isZeroFlux)
			{
				sizeX *= 1.5f;
				sizeY *= 1.5f;
			}			
			
			sprite.setSize(sizeX, sizeY);
            sprite.setColor(new Color(1, 1, 1, spriteEntry.alpha));
			float rotation = spriteEntry.rotation;
			sprite.setAngle(rotation + 90);
			
			rotation = (float)Math.toRadians(rotation);
			Vector2f wepPosition = spriteEntry.weapon.getLocation();
			float x = wepPosition.x;
			float y = wepPosition.y;
			x += (float)Math.sin(rotation) * spriteEntry.currentDistance;
			y += (float)Math.cos(rotation) * spriteEntry.currentDistance;
			sprite.renderAtCenter(y, y);
		}
	}

	@Override
	public void renderInUICoords(ViewportAPI vapi) {
		
	}

	@Override
	public void init(CombatEngineAPI engine) {
		this.engine = engine;
		plugin = this;
	}
	
	public class DriveFXSprite
	{
		DriveFXSpriteDef def;
		WeaponAPI weapon;
		float distance = 256;
		float currentDistance = 0;
		float rotation = 0;
		float age = 0;
		float ttl = 1;
		float alpha = 1;
		float sizeMult = 1;
		boolean isZeroFlux = false;
		boolean reverse = false;
		
		public DriveFXSprite(DriveFXSpriteDef def, WeaponAPI weapon, boolean isZeroFlux)
		{
			this.weapon = weapon;
			this.def = def;
			this.ttl = def.ttl;
			this.isZeroFlux = isZeroFlux;
			this.distance = def.baseDistance;
			if (isZeroFlux) this.distance *= 1.5f;
		}
	}
	
	public static class DriveFXSpriteDef
	{
		String image;
		float sizeX = 128;
		float sizeY = 32;
		float baseDistance = 300;
		float ttl = 1.5f;
		float startAlpha = 0.25f;
		float peakAlpha = 0.7f;
		float endAlpha = 0;
		float startSizeMult = 0.5f;
		float peakSizeMult = 1f;
		float endSizeMult = 0.75f;
		float peakTime = 0.5f;
	}
}
