package data.scripts.plugins;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.input.InputEventAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.AIUtils;

public class NZL_WeaselDronePlugin extends BaseEveryFrameCombatPlugin {
	
	protected static final float MISSILE_SEARCH_RANGE = 650;
	
	protected CombatEngineAPI engine;
	protected final IntervalUtil tracker = new IntervalUtil(0.3f, 0.3f);
	
	protected List<MissileAPI> playerMissiles = new ArrayList<>();
	protected List<MissileAPI> enemyMissiles = new ArrayList<>();
	
	public static Logger log = Global.getLogger(NZL_WeaselDronePlugin.class);
	
	// owners: 0 = player, 1 = enemy
	public void updateMissiles()
	{
		playerMissiles.clear();
		enemyMissiles.clear();
		for (MissileAPI tmp : Global.getCombatEngine().getMissiles())
		{
			//engine.addFloatingText(tmp.getLocation(), tmp.getOwner() + "", 24, Color.yellow, tmp, 0, 0);
			if (tmp.isFizzling()) continue;
			if (tmp.isFlare()) continue;
			if (tmp.getOwner() == 1) enemyMissiles.add(tmp);
			else playerMissiles.add(tmp);
		}
	}
	
	// basically the LazyLib one except we pre-filtered and cached the list of all enemy missiles
	public List<MissileAPI> getNearbyEnemyMissiles(CombatEntityAPI entity, float range)
	{
		List<MissileAPI> missiles = new ArrayList<>();
		List<MissileAPI> toCheck = enemyMissiles;
		if (entity.getOwner() == 1) toCheck = playerMissiles;

		for (MissileAPI enemy : toCheck)
		{
			if (MathUtils.isWithinRange(entity, enemy, range))
			{
				missiles.add(enemy);
			}
		}

		return missiles;
	}
	
	public void doMissileCheck(ShipAPI ship, boolean isDrone)
	{
		if (isDrone && !AIUtils.canUseSystemThisFrame(ship)) {
			//engine.addFloatingText(ship.getLocation(), (ship.getSystem() == null) + "", 24, Color.yellow, ship, 0, 0);
			return;
		}
		
		List<MissileAPI> threats = new ArrayList<>();
		List<MissileAPI> nearbyMissiles = AIUtils.getNearbyEnemyMissiles(ship, MISSILE_SEARCH_RANGE);
		for (MissileAPI missile : nearbyMissiles) {
			CombatEntityAPI target = missile.getDamageTarget();
			if (target == null) {
				//engine.addFloatingText(missile.getLocation(), "null target", 24, Color.yellow, missile, 0, 0);
				continue;
			}
			if (target instanceof MissileAPI)
			{
				if (((MissileAPI)target).isFlare()) continue;
			}
			threats.add(missile);
		}
		if (threats.isEmpty()) return;
		//engine.addFloatingText(ship.getLocation(), "Want flares!", 24, Color.yellow, ship, 0, 0);
		
		if (isDrone)
		{
			//engine.addFloatingText(ship.getLocation(), "Using system", 24, Color.red, ship, 0, 0);
			ship.useSystem();
		}
		else {	// TODO
			//List<ShipAPI> drones = ship.getDeployedDrones();
		}
	}
	
	@Override
	public void advance(float amount, List<InputEventAPI> events) {
		if (engine == null) {
			return;
		}
		if (engine.isPaused()) {
			return;
		}
		tracker.advance(amount);

		if (tracker.intervalElapsed()) 
		{
			updateMissiles();
			List<ShipAPI> ships = engine.getShips();
			for (ShipAPI ship : ships)
			{
				if (!engine.isEntityInPlay(ship)) continue;
				if (!ship.isAlive()) continue;
				//engine.addFloatingText(ship.getLocation(), "check", 24, Color.GREEN, ship, 0, 0);
				ShipSystemAPI system = ship.getSystem();
				if (system == null) continue;
				if (!system.getId().equals("nzl_drone_weasel")) continue;
				if (ship.getDeployedDrones() == null) continue;
				
				//doMissileCheck(ship, false);
				for (ShipAPI drone : ship.getDeployedDrones())
					doMissileCheck(drone, true);
			}
		}
	}
	
	@Override
	public void init(CombatEngineAPI engine) {
		this.engine = engine;
		//log.info("Weasel Drone plugin initialized");
	}
}
