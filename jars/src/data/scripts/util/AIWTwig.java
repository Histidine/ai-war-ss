package data.scripts.util;

import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipEngineControllerAPI.ShipEngineAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.AIWarModPlugin;
import db.twiglib.TwigUtils;
import java.util.List;

public class AIWTwig {

    public static ShipAPI empTargetTwig(ShipAPI ship) {
        if (AIWarModPlugin.hasTwigLib) {
            if (!TwigUtils.isMultiShip(ship)) {
                return ship;
            }

            ShipAPI root = TwigUtils.getRoot(ship);
            List<ShipAPI> children = TwigUtils.getChildNodesAsShips(root);
            children.add(root);

            WeightedRandomPicker<ShipAPI> targetPicker = new WeightedRandomPicker<>();
            for (ShipAPI child : children) {
                if (!child.isAlive()) {
                    continue;
                }
                int weight = 0;
                for (WeaponAPI weapon : child.getAllWeapons()) {
                    if (!weapon.getSlot().isHidden() && !weapon.getSlot().isDecorative()) {
                        weight++;
                    }
                }
                for (ShipEngineAPI engine : child.getEngineController().getShipEngines()) {
                    if (!engine.isSystemActivated()) {
                        weight++;
                    }
                }
                targetPicker.add(child, weight);
            }

            if (targetPicker.getItems().size() > 0) {
                return targetPicker.pick();
            } else {
                return ship;
            }
        } else {
            return ship;
        }
    }

    public static boolean isRoot(ShipAPI ship) {
        if (AIWarModPlugin.hasTwigLib) {
            return TwigUtils.getRoot(ship) == ship;
        } else {
            return true;
        }
    }
}
