package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageAPI;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnHitEffectPlugin;
import java.awt.Color;
import org.lazywizard.lazylib.CollisionUtils;
import org.lwjgl.util.vector.Vector2f;

public class AIW_PlasmaSiegeOnHitEffect implements OnHitEffectPlugin {
    public static float FEEDBACK_DAMAGE_MULT = 0.25f;
    protected static final Vector2f ZERO = new Vector2f();
    protected static final Color FEEDBACK_EXPLOSION_COLOR = new Color(144, 192, 255, 255);
    
    @Override
    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, CombatEngineAPI engine) {
        if (target == null || point == null) {
            return;
        }
        explode(projectile, target, point, shieldHit, engine);
    }
    
    public static void explode(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, CombatEngineAPI engine)
    {
        String weaponId = "";
        if (projectile.getWeapon() != null) weaponId = projectile.getWeapon().getId();
        if (weaponId.equals("aiw_plasmasiegegun")) Global.getSoundPlayer().playSound("aiw_plasmasiegegun_impact", 1f, 4f, point, ZERO);
        else if (weaponId.equals("aiw_plasmasiegecannon")) Global.getSoundPlayer().playSound("aiw_plasmasiegecannon_impact", 1f, 4f, point, ZERO);
        
        if (!shieldHit) return; //done here
        
        DamageAPI damage = projectile.getDamage();
        float damageMult = 1;
        if (projectile.getSource() != null) damageMult =  projectile.getSource().getMutableStats().getEnergyWeaponDamageMult().getModifiedValue();
        //float damageAmount = projectile.getWeapon().getDerivedStats().getDamagePerShot();
        float damageAmount = damage.getDamage() * damageMult;
        //float angle = Vector2f.angle(target.getLocation(), point);
        //float distance = target.getCollisionRadius() * 0.5f;
        //engine.addFloatingText(target.getLocation(), damageAmount + "", 24, Color.yellow, target, 0, 0);
        Vector2f damagePoint = CollisionUtils.getCollisionPoint(point, target.getLocation(), target);
        if (damagePoint == null) damagePoint = target.getLocation();
        engine.applyDamage(target, damagePoint, damageAmount*FEEDBACK_DAMAGE_MULT, damage.getType(), 0, true, false, projectile.getSource());
        engine.spawnExplosion(damagePoint, ZERO, FEEDBACK_EXPLOSION_COLOR, (float)Math.sqrt(damageAmount)*4f, 3);
        //damage.setDamage(projectile.getDamageAmount() * (1 - FEEDBACK_DAMAGE_MULT));  // doesn't work
    }
}
