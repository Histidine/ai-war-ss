package data.scripts.weapons;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipEngineControllerAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import java.awt.Color;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

public class AIW_ReactionlessDriveEffect implements EveryFrameWeaponEffectPlugin {
	
	protected final IntervalUtil tracker = new IntervalUtil(0.4f, 0.4f);
	protected final Color COLOR_NORMAL = new Color(0.5f, 0.7f, 1f, 0.5f);
	protected final Color COLOR_REVERSE = new Color(1f, 0.3f, 0.3f, 0.5f);
	//protected final float BASE_SPEED = 100f;
	protected final float BASE_DISTANCE = 200f;
	protected final float PARTICLE_LIFE = 2f;
	protected final float PARTICLE_SIZE = 18;
	
	@Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
		if (engine.isPaused()) return;
		/*
		tracker.advance(amount);

		if (tracker.intervalElapsed()) 
		{
			ShipAPI ship = weapon.getShip();
			if (engine.isPaused()) return;
			if (ship.isHulk()) return;
			// TODO
			
			AIW_ReactionlessDrivePlugin plugin = AIW_ReactionlessDrivePlugin.getPlugin();
			if (plugin != null)
				plugin.spawnSprite(weapon, weapon.getShip().getFluxTracker().isEngineBoostActive());
		}
		*/
		ShipAPI ship = weapon.getShip();
		if (ship.isHulk()) return;
		
		float rotation = weapon.getCurrAngle() + MathUtils.getRandomNumberInRange(-10, 10);
		rotation = (float)Math.toRadians(rotation);
		float distance = BASE_DISTANCE;
		float size = PARTICLE_SIZE;
		float life = PARTICLE_LIFE * MathUtils.getRandomNumberInRange(0.9f, 1.1f);
		boolean reversing = false;
		boolean zeroFlux = ship.getFluxTracker().isEngineBoostActive();
		ShipEngineControllerAPI engineController = ship.getEngineController();
		if (engineController.isAcceleratingBackwards() || engineController.isDecelerating())
			reversing = true;
		else if (!engineController.isAccelerating() && !engineController.isStrafingLeft() && !engineController.isStrafingRight())
		{
			distance *= 0.4f;
			if (!zeroFlux) size *= 0.5f;
			else size *= 0.65f;
		}
		if (zeroFlux)
			distance *= 1.5f;
		
		float speed = distance/PARTICLE_LIFE;
		float x = (float)Math.cos(rotation);
		float y = (float)Math.sin(rotation);
		float vx = x * speed;
		float vy = y * speed;
		
		Vector2f origin = weapon.getLocation();
		Vector2f vel = new Vector2f(vx, vy);
		Color color = COLOR_NORMAL;
		if (reversing)
		{
			color = COLOR_REVERSE;
			//distance *= 0.5f;
			//float px = x * distance + origin.x;
			//float py = y * distance + origin.y;
			//origin = new Vector2f(px, py);
			//vel.scale(-1.1f);
			//life *= 0.5f;
		}
		
		engine.addSmoothParticle(origin, vel, size, 5, life, color);
		//engine.addFloatingText(weapon.getLocation(), weapon.getCurrAngle() + "", 24, Color.yellow, weapon.getShip(), 0, 0);
    }
}
