package data.scripts.weapons;

import com.fs.starfarer.api.AnimationAPI;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.WeaponAPI;

public class Spire_BlackWhiteWeaponEffect implements EveryFrameWeaponEffectPlugin {
	boolean done = false;
	Boolean isDark = null;
	
    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
		if (weapon.getSlot().isHidden()) {
            return;
        }
		
        if (isDark == null)
		{
			String name = weapon.getShip().getHullSpec().getHullId();
			isDark = name.startsWith("spire_") && name.contains("_dark") || name.startsWith("znth_");
			//Global.getLogger(this.getClass()).info("Weapon is dark: " + isDark);
		}
		if (!done)
		{
			AnimationAPI anim = weapon.getAnimation();
			if (isDark)	anim.setFrame(1);
			else anim.setFrame(0);
			anim.pause();
			done = true;
		}
    } 
}
