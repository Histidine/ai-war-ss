package data.scripts.weapons;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import data.scripts.plugins.AIW_ForcefieldPlugin;
import java.awt.Color;

public class Spire_ShipLightEffect implements EveryFrameWeaponEffectPlugin {
    protected static final float GLOW_PERIOD_MULT = 1.5f;
    protected static final float[] HEALTHY_COLOR = { 0.2f, 0.75f, 0.5f };
    protected static final float[] DAMAGED_COLOR = { 0.65f, 0.6f, 0.15f };
    protected static final float[] DEAD_COLOR = { 0.75f, 0.15f, 0.15f };
    
    private float elapsed = 0;
    float alpha = 1;

    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
        boolean on = true;
        ShipAPI ship = weapon.getShip();
        float[] color = HEALTHY_COLOR;
        if (engine.isPaused()) {
            if (weapon.getSprite().getColor().equals(Color.WHITE))
            {
                weapon.getSprite().setColor(new Color(color[0],color[1],color[2], alpha));
                return;
            } 
        }

        elapsed += amount * GLOW_PERIOD_MULT;
        if (ship.isHulk()) on = false;

        if (ship.getFluxTracker().isVenting()) {
            //on = false;
        } else if (ship.getFluxTracker().isOverloaded()) {
            //on = new Random().nextInt(4) == 3;
            //on = false;
        }
        if (on) {
            alpha = (float)Math.cos(elapsed) * 0.25f;
            if (alpha < 0) alpha = -alpha;
            alpha += 0.75f;
            if (AIW_ForcefieldPlugin.isForcefieldDead(ship) || ship.getFluxTracker().isOverloaded()) 
                color = DEAD_COLOR;
            else if (AIW_ForcefieldPlugin.getForcefieldRegrowDelay(ship) > 0) 
                color = DAMAGED_COLOR;
        } else {
            alpha = 0;
        }
 
        //Global.getCombatEngine().addFloatingText(weapon.getLocation(), on + "", 24, Color.yellow, ship, 0, 0);

        weapon.getSprite().setColor(new Color(color[0],color[1],color[2], alpha));
        weapon.getSprite().bindTexture();
    }    
}
