package data.scripts.world;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.RepLevel;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorGeneratorPlugin;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.shared.SharedData;
import data.scripts.world.systems.AIW_Gemstone;
import data.scripts.world.systems.AIW_Optica;
import data.scripts.world.systems.ZEN_ZenithTrader;

public class AIW_SectorGen implements SectorGeneratorPlugin {

    @Override
    public void generate(SectorAPI sector) {
        SharedData.getData().getPersonBountyEventData().addParticipatingFaction("spire");
        initFactionRelationships(sector);
        
        new AIW_Gemstone().generate(sector);
        new AIW_Optica().generate(sector);
        new ZEN_ZenithTrader().generate(sector);
    }
    
    public static void initFactionRelationships(SectorAPI sector) {
        FactionAPI spire = sector.getFaction("spire");
        FactionAPI imperium = sector.getFaction("interstellarimperium");
        FactionAPI blackrock = sector.getFaction("blackrock_driveyards");
        //FactionAPI mayorate = sector.getFaction("mayorate");
        
        //spire.setRelationship(Factions.PLAYER, 0);
        spire.setRelationship(Factions.HEGEMONY, RepLevel.SUSPICIOUS);
        spire.setRelationship(Factions.TRITACHYON, RepLevel.INHOSPITABLE);
        spire.setRelationship(Factions.PIRATES, RepLevel.HOSTILE);
        spire.setRelationship(Factions.DIKTAT, RepLevel.INHOSPITABLE);
        spire.setRelationship(Factions.LIONS_GUARD, RepLevel.INHOSPITABLE);
        spire.setRelationship(Factions.LUDDIC_CHURCH, RepLevel.SUSPICIOUS);
        spire.setRelationship(Factions.LUDDIC_PATH, RepLevel.HOSTILE);
        spire.setRelationship(Factions.INDEPENDENT, RepLevel.FAVORABLE);
        //spire.setRelationship(Factions.PERSEAN, RepLevel.FAVORABLE);
        
        if (imperium != null)
            spire.setRelationship("interstellarimperium", RepLevel.SUSPICIOUS);
        if (blackrock != null)
            spire.setRelationship("blackrock_driveyards", RepLevel.SUSPICIOUS);
        
        FactionAPI darkspire = sector.getFaction("darkspire");
        for (FactionAPI faction : Global.getSector().getAllFactions())
        {
            if (faction == darkspire) continue;
            if (faction.getId().equals("famous_bounty")) continue;
            darkspire.setRelationship(faction.getId(), -1f);
        }
    }
}
