package data.scripts.world;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FleetAssignment;
import com.fs.starfarer.api.campaign.LocationAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.ai.FleetAssignmentDataAPI;
import com.fs.starfarer.api.util.Misc;
import data.scripts.util.AIW_StringHelper;
import data.scripts.world.AIW_VengeanceGeneratorManager.VengeanceFleetData;
import org.apache.log4j.Logger;
import org.lwjgl.util.vector.Vector2f;

public class AIW_VengeanceFleetAI implements EveryFrameScript
{
    public static Logger log = Global.getLogger(AIW_VengeanceFleetAI.class);
       
    private final VengeanceFleetData data;
    private float daysTotal = 0.0F;
    private final CampaignFleetAPI fleet;
    private boolean orderedReturn = false;
  
    public AIW_VengeanceFleetAI(CampaignFleetAPI fleet, VengeanceFleetData data)
    {
        this.fleet = fleet;
        this.data = data;
        giveInitialAssignment();
    }
  
    @Override
    public void advance(float amount)
    {
        float days = Global.getSector().getClock().convertToDays(amount);
        this.daysTotal += days;
        if (this.daysTotal > 150.0F)
        {
            giveStandDownOrders();
            return;
        }
        FleetAssignmentDataAPI assignment = this.fleet.getAI().getCurrentAssignment();
        if (assignment != null)
        {
            float fp = this.fleet.getFleetPoints();
            if (fp < this.data.startingFleetPoints / 2.0F) {
                giveStandDownOrders();
            }
            
            if (orderedReturn)
                return;
        }
        else
        {
            SectorEntityToken target = data.target;
            LocationAPI loc = target.getContainingLocation();
            String targetName = target.getName();
            StarSystemAPI system = null;
            if (loc instanceof StarSystemAPI)
                system = (StarSystemAPI)loc;
            
            if (system != null)
            {
                Vector2f dest = Misc.getPointAtRadius(system.getLocation(), 1500.0F);
                LocationAPI hyper = Global.getSector().getHyperspace();
                SectorEntityToken token = hyper.createToken(dest.x, dest.y);
                String systemBaseName = system.getBaseName();
                
                if (system != this.fleet.getContainingLocation()) {
                  this.fleet.addAssignment(FleetAssignment.GO_TO_LOCATION, token, 1000.0F, AIW_StringHelper.getFleetAssignmentString("travellingToStarSystem", systemBaseName, null));
                }
 
                if (Math.random() > 0.8D) {
                  this.fleet.addAssignment(FleetAssignment.RAID_SYSTEM, system.getHyperspaceAnchor(), 40.0F, AIW_StringHelper.getFleetAssignmentString("attackingAroundStarSystem", systemBaseName, null));
                } else if (Math.random() > 0.5D) {
                  this.fleet.addAssignment(FleetAssignment.ATTACK_LOCATION, target, 40.0F, AIW_StringHelper.getFleetAssignmentString("attacking", targetName, null));
                } else {
                  this.fleet.addAssignment(FleetAssignment.RAID_SYSTEM, system.getStar(), 40.0F, AIW_StringHelper.getFleetAssignmentString("attackingStarSystem", systemBaseName, null));
                }
            }
            else
            {
                this.fleet.addAssignment(FleetAssignment.GO_TO_LOCATION, target, 40.0F, AIW_StringHelper.getFleetAssignmentString("attacking", targetName, null));
                this.fleet.addAssignment(FleetAssignment.ATTACK_LOCATION, target, 40.0F, AIW_StringHelper.getFleetAssignmentString("attacking", targetName, null));
            }

        }
    }
  
    @Override
    public boolean isDone()
    {
        return !this.fleet.isAlive();
    }
  
    @Override
    public boolean runWhilePaused()
    {
        return false;
    }
  
    private float getDaysToOrbit()
    {
        float daysToOrbit = 0.0F;
        if (this.fleet.getFleetPoints() <= 50.0F) {
            daysToOrbit += 2.0F;
        } else if (this.fleet.getFleetPoints() <= 100.0F) {
            daysToOrbit += 4.0F;
        } else if (this.fleet.getFleetPoints() <= 150.0F) {
            daysToOrbit += 6.0F;
        } else {
            daysToOrbit += 8.0F;
        }
        daysToOrbit *= (0.5F + (float)Math.random() * 0.5F);
        return daysToOrbit;
    }
  
    private void giveInitialAssignment()
    {
        //float daysToOrbit = getDaysToOrbit();
        //this.fleet.addAssignment(FleetAssignment.ORBIT_PASSIVE, this.data.source, daysToOrbit, "preparing for strike mission at " + this.data.source.getName());
    }
  
    protected void giveStandDownOrders()
    {
        if (!this.orderedReturn)
        {
            //log.info("Vengeance fleet " + this.fleet.getNameWithFaction() + " standing down");
            this.orderedReturn = true;
            this.fleet.clearAssignments();
            
            SectorEntityToken destination = data.source;
            
            this.fleet.addAssignment(FleetAssignment.DELIVER_CREW, destination, 1000.0F, AIW_StringHelper.getFleetAssignmentString("returningTo", destination.getName(), null));
            //this.fleet.addAssignment(FleetAssignment.ORBIT_PASSIVE, destination, getDaysToOrbit(), AIW_StringHelper.getFleetAssignmentString("standingDown", null, null));
            this.fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, destination, 1000.0F);
        }
    }
}

