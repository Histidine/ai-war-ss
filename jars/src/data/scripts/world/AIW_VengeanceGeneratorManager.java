package data.scripts.world;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.BaseCampaignEventListener;
import com.fs.starfarer.api.campaign.BattleAPI;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.LocationAPI;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV2;
import com.fs.starfarer.api.impl.campaign.fleets.FleetParams;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.util.AIW_StringHelper;
import data.scripts.world.systems.AIW_SystemGenUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

public class AIW_VengeanceGeneratorManager extends BaseCampaignEventListener {

    protected static final String CONFIG_FILE = "data/config/AIWar/vengeanceGenConfig.json";
    
    public static final float RANDOM_MIN_ORBIT_RADIUS = 8000f;
    public static final float RANDOM_MAX_ORBIT_RADIUS = 15000f;
    public static final float RANDOM_MIN_HYPER_DISTANCE = 3000f;
    public static final float RANDOM_MAX_HYPER_DISTANCE = 12000f;
    
    public static int useRandomGenerators = 0;
    public static float randomSpawnChance = 0.5f;
    public static float randomSpawnChanceHyperspace = 1f;
    public static float baseSpawnMult = 1;
    public static float baseSpawnThreshold = 32f;
    public static float chargeEfficiency = 0.16f;
    public static float secondarySpawnMult = 0.35f;
    public static float secondaryChargeMult = 0f;
    public static int secondarySpawnCount = 2;
    
    protected static JSONArray generatorSystems;
      
    public static Logger log = Global.getLogger(AIW_VengeanceGeneratorManager.class);
    
    protected List<VengeanceGenerator> generators = new ArrayList<>();
    protected MarketAPI fakeMarket;
    
    static {
        try {
            JSONObject config = Global.getSettings().loadJSON(CONFIG_FILE);
            useRandomGenerators = config.optInt("useRandomGenerators", useRandomGenerators);
            randomSpawnChance = (float)config.optDouble("randomSpawnChance", randomSpawnChance);
            randomSpawnChanceHyperspace = (float)config.optDouble("randomSpawnChanceHyperspace", randomSpawnChanceHyperspace);
            baseSpawnMult = (float)config.optDouble("baseSpawnMult", baseSpawnMult);
            baseSpawnThreshold = (float)config.optDouble("baseSpawnThreshold", baseSpawnThreshold);
            chargeEfficiency = (float)config.optDouble("chargeEfficiency", chargeEfficiency);
            secondarySpawnMult = (float)config.optDouble("secondarySpawnMult", secondarySpawnMult);
            secondaryChargeMult = (float)config.optDouble("secondaryChargeMult", secondaryChargeMult);
            secondarySpawnCount = config.optInt("secondarySpawnCount", secondarySpawnCount);
            generatorSystems = config.getJSONArray("systems");
        } catch (IOException | JSONException ex) {
            log.error(ex);
        }
    }
    
    
    public void spawnGenerator(LocationAPI system, SectorEntityToken toOrbit, float orbitRadius)
    {
        SectorEntityToken generator = system.addCustomEntity("aiw_vengeance_generator_" + (generators.size() + 1), null, "aiw_vengeance_generator", "darkspire");
        
        float orbitDays = AIW_SystemGenUtils.getOrbitalPeriod(toOrbit.getRadius(), orbitRadius, AIW_SystemGenUtils.getDensity(toOrbit));
        generator.setCircularOrbitWithSpin(toOrbit, MathUtils.getRandomNumberInRange(0, 360), orbitRadius, orbitDays, 30, 60);
        generators.add(new VengeanceGenerator(generator, system));
    }
    
    public void spawnHyperspaceGenerator(SectorAPI sector, float orbitRadius)
    {
        LocationAPI hyperspace = sector.getHyperspace();
        SectorEntityToken generator = hyperspace.addCustomEntity("aiw_vengeance_generator_" + (generators.size() + 1), null, "aiw_vengeance_generator", "darkspire");
        float orbitDays = AIW_SystemGenUtils.getOrbitalPeriod(500, orbitRadius, 1);
        generator.setCircularOrbitWithSpin(hyperspace.createToken(0, 0), MathUtils.getRandomNumberInRange(0, 360), orbitRadius, orbitDays, 30, 60);
        
        //float distance = MathUtils.getRandomNumberInRange(RANDOM_MIN_HYPER_DISTANCE, RANDOM_MAX_HYPER_DISTANCE);
        //float angle = MathUtils.getRandomNumberInRange(0f, 2*(float)Math.PI);
        //generator.setFixedLocation((float)Math.sin(angle)*distance, (float)Math.cos(angle)*distance);
        generators.add(new VengeanceGenerator(generator, hyperspace));
    }
    
    public AIW_VengeanceGeneratorManager() {
        super(true);
        
        //fakeMarket = Global.getFactory().createMarket("spire_vengeance_fakeMarket", "Fake Vengeance Market", 6);
        //fakeMarket.setFactionId("darkspire");
    }
    
    public void generate(SectorAPI sector, boolean wantRandom)
    {
        try {
            JSONObject config = Global.getSettings().loadJSON(CONFIG_FILE);
            int configRandom = config.optInt("useRandomGenerators", 0);
            
            boolean random = false;
            if (configRandom == -1) return;
            else if (configRandom == 0) random = wantRandom;
            else if (configRandom == 1) random = true;
            
            if (random)
            {
               List<StarSystemAPI> systems = sector.getStarSystems();
               for (StarSystemAPI system: systems)
               {
                   if (Math.random() > randomSpawnChance) continue;
                   SectorEntityToken toOrbit = system.getStar();
                   float orbitRadius = MathUtils.getRandomNumberInRange(RANDOM_MIN_ORBIT_RADIUS, RANDOM_MAX_ORBIT_RADIUS);
                   spawnGenerator(system, toOrbit, orbitRadius);
               }

               if (Math.random() <= randomSpawnChanceHyperspace)
               {
                   float orbitRadius = MathUtils.getRandomNumberInRange(RANDOM_MIN_HYPER_DISTANCE, RANDOM_MAX_HYPER_DISTANCE);
                   spawnHyperspaceGenerator(sector, orbitRadius);
               }
            }
            else
            {
                for(int i=0; i<generatorSystems.length(); i++)
                {
                    JSONObject entry = generatorSystems.getJSONObject(i);
                    String systemId = entry.getString("system");
                    
                    if (systemId.equals("hyperspace"))
                    {
                        float orbitRadius = (float)entry.optDouble("orbitRadius", 10000);
                        spawnHyperspaceGenerator(sector, orbitRadius);
                        continue;
                    }
                    
                    LocationAPI system = sector.getStarSystem(systemId);
                    if (system == null)
                    {
                        log.warn("Could not find system " + systemId);
                        continue;
                    }
                    String toOrbitId = entry.getString("orbits");
                    SectorEntityToken toOrbit = system.getEntityById(toOrbitId);
                    if (toOrbit == null)
                    {
                        log.warn("Could not find entity to orbit: " + toOrbitId);
                        continue;
                    }
                    
                    // make generator
                    float orbitRadius = (float)entry.optDouble("orbitRadius", 10000);
                    spawnGenerator(system, toOrbit, orbitRadius);
                }
            }
        } catch (IOException | JSONException ex) {
            log.error(ex);
        }
    }
    
    public void spawnVengeanceFleet(LocationAPI loc, SectorEntityToken spawner, float fp)
    {
        WeightedRandomPicker<SectorEntityToken> picker = new WeightedRandomPicker<>();
        if (!loc.isHyperspace())
        {
            List<MarketAPI> markets = Misc.getMarketsInLocation(loc);
            for (MarketAPI market : markets)
            {
                if (market.getFaction().isHostileTo("darkspire"))
                    picker.add(market.getPrimaryEntity(), market.getSize());
            }
        }
        if (picker.isEmpty())
        {
            List<MarketAPI> marketsAll = Global.getSector().getEconomy().getMarketsCopy();
            for (MarketAPI market : marketsAll)
            {
                if (market.getFaction().isHostileTo("darkspire"))
                    picker.add(market.getPrimaryEntity(), market.getSize());
            }
        }
        if (picker.isEmpty()) return;
        
        String key = "vengeanceFleetMedium";
        if (fp < 12) key = "vengeanceFleetSmall";
        else if (fp > 36) key = "vengeanceFleetLarge";
        
        String name = AIW_StringHelper.getString("aiw_vengeanceGen", key);
        
        float qf = MathUtils.getRandomNumberInRange(0.2f, 0.5f) + MathUtils.getRandomNumberInRange(0.2f, 0.5f);
        int levelBonus = 0;    //(int)(fp/5 * MathUtils.getRandomNumberInRange(0.75f, 1.25f));
        
        // try using a randomly picked market instead of fake market
        List<MarketAPI> markets = Global.getSector().getEconomy().getMarketsCopy();
        MarketAPI market = markets.get(MathUtils.getRandomNumberInRange(0, markets.size() - 1));
        
        // fleet composition
        // small fleets have only combat ships, larger ones can have Zeolites, Obsidians or both
        float combatMult = 1f;
        float freighterMult = 0;
        float personnelMult = 0;
        if (fp > 32)
        {
            freighterMult = 0.1f;
            personnelMult = 0.05f;
            combatMult = 0.85f;
        }
        else if (fp > 18)
        {
            if (Math.random() < 0.4f) freighterMult = 0.05f;
            else personnelMult = 0.05f;
            combatMult = 0.95f;
        }
        
        FleetParams fleetParams = new FleetParams(spawner.getLocationInHyperspace(), market, "darkspire", null, "spireVengeanceFleet", 
                fp*combatMult,
                fp*freighterMult,
                0,        // tankers
                fp*personnelMult,
                0,        // liners
                0,        // civilian
                0,        // utility
                0f, qf, 2f, levelBonus);    // quality bonus, quality override, officer num mult, officer level bonus
        
        CampaignFleetAPI fleet = FleetFactoryV2.createFleet(fleetParams);
        //CampaignFleetAPI fleet = FleetFactory.createGenericFleet("darkspire", name, qf, (int)fp);
        
        fleet.setName(name);
        fleet.setAIMode(true);
        
        loc.addEntity(fleet);
        
        fleet.setLocation(spawner.getLocation().x, spawner.getLocation().y);
        
        VengeanceFleetData data = new VengeanceFleetData(fleet);
        String logMessage = "Spawning " + name + " in " + loc.getName();
        log.info(logMessage);
        if (Global.getSettings().isDevMode())
        {
            Global.getSector().getCampaignUI().addMessage(logMessage);
        }
        data.source = spawner;
        data.startingFleetPoints = fleet.getFleetPoints();    // fp;
        data.target = picker.pick();
        fleet.addScript(new AIW_VengeanceFleetAI(fleet, data));
    }

    @Override
    public void reportBattleFinished(CampaignFleetAPI winner, BattleAPI battle)
    {
        CampaignFleetAPI loser = battle.getPrimary(battle.getOtherSideFor(winner));
        float losses = 0f;
        
        if (winner.getFaction().getId().equals("darkspire") || loser.getFaction().getId().equals("darkspire"))
            return;
        
        boolean generatorPresent = false;
        for (VengeanceGenerator generator : generators)
        {
            if (generator.location == winner.getContainingLocation())
            {
                generatorPresent = true;
                break;
            }
        }
        if (!generatorPresent) return;
        
        List<FleetMemberAPI> loserCurrent = loser.getFleetData().getMembersListCopy();
        for (FleetMemberAPI member : loser.getFleetData().getSnapshot()) {
            if (!loserCurrent.contains(member)) {
                losses += member.getFleetPointCost();
            }
        }
        List<FleetMemberAPI> winnerCurrent = winner.getFleetData().getMembersListCopy();
        for (FleetMemberAPI member : winner.getFleetData().getSnapshot()) {
            if (!winnerCurrent.contains(member)) {
                losses += member.getFleetPointCost();
            }
        }
        losses *= chargeEfficiency;
        LocationAPI loc = winner.getContainingLocation();
        
        for (VengeanceGenerator charging : generators)
        {
            boolean isPrimary = (charging.location == loc);
            if (isPrimary) charging.charge += losses;
            else charging.charge += losses * (1/generators.size()) * secondaryChargeMult;
            if (charging.charge > baseSpawnThreshold)
            {
                Vector2f playerLoc = Global.getSector().getPlayerFleet().getLocationInHyperspace();
                List<VengeanceGenerator> toSpawn = new ArrayList<>();
                WeightedRandomPicker<VengeanceGenerator> picker = new WeightedRandomPicker();
                
                float fpBase = charging.charge;
                for (VengeanceGenerator gen: generators)
                {
                    if (gen == charging) toSpawn.add(gen);    // primary
                    else
                    {
                        float distance = MathUtils.getDistance(playerLoc, gen.entity.getLocationInHyperspace()) + 7200;
                        picker.add(gen, 50000/distance);
                    }
                }
                for (int i=0; i<secondarySpawnCount; i++)
                {
                    if (picker.isEmpty()) break;
                    toSpawn.add(picker.pickAndRemove());
                }
                
                for (VengeanceGenerator spawner: toSpawn)
                {
                    float fp = fpBase;
                    if (spawner != charging) fp *= secondarySpawnMult;
                    fp *= MathUtils.getRandomNumberInRange(0.75f, 1.25f);
                    spawnVengeanceFleet(spawner.location, spawner.entity, fp);
                }
                charging.charge = 0;
            }
        }
    }
    
    public static class VengeanceGenerator
    {
        SectorEntityToken entity;
        float charge = 0;
        LocationAPI location;
        
        public VengeanceGenerator(SectorEntityToken entity, LocationAPI location)
        {
            this.entity = entity;
            this.location = location;
        }
    }
    
    public static class VengeanceFleetData
    {
        public CampaignFleetAPI fleet;
        public SectorEntityToken source;
        public SectorEntityToken target;
        public float startingFleetPoints = 0.0F;
    
        public VengeanceFleetData(CampaignFleetAPI fleet)
        {
            this.fleet = fleet;
        }
    }
}
