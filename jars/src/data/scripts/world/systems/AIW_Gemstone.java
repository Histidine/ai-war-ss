package data.scripts.world.systems;

import com.fs.starfarer.api.Global;
//import com.fs.starfarer.api.InteractionDialogImageVisual;
import com.fs.starfarer.api.campaign.JumpPointAPI;
import com.fs.starfarer.api.campaign.LocationAPI;
import com.fs.starfarer.api.campaign.PlanetAPI;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import java.awt.Color;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.impl.campaign.ids.Submarkets;
import com.fs.starfarer.api.impl.campaign.ids.Terrain;
import com.fs.starfarer.api.impl.campaign.terrain.AsteroidFieldTerrainPlugin;
import com.fs.starfarer.api.impl.campaign.terrain.BaseRingTerrain;
import com.fs.starfarer.api.util.Misc;
import data.scripts.util.AIW_StringHelper;
import java.util.ArrayList;
import java.util.Arrays;

public class AIW_Gemstone {
    
    //addPlanet(String id,  SectorEntityToken focus,name, type, angle, radius, orbitRadius, orbitDays
    
    public void generate(SectorAPI sector) {
        
        // inits
        StarSystemAPI system = sector.createStarSystem("Gemstone");
        system.getLocation().set(-5000, 1500);
        LocationAPI hyper = Global.getSector().getHyperspace();
        
        system.setBackgroundTextureFilename("graphics/AIWar/backgrounds/gemstone_alt.jpg");
        system.setLightColor(new Color(255, 235, 205));
        
        // star (Gemstone)
        float gemstoneRadius = 500f;
        PlanetAPI gemstone = system.initStar("aiw_gemstone", "star_orange", gemstoneRadius, 500);

        // Ruby (first planet)
        float rubyRadius = 100;
        float rubyOrbitRadius = 2000;
        PlanetAPI ruby = AIW_SystemGenUtils.addPlanetToSystem(system, "aiw_ruby", gemstone, "lava", rubyRadius, rubyOrbitRadius, true);
        ruby.setCustomDescriptionId("planet_ruby");
        
        // Emerald (second planet)
        float emeraldRadius = 180;
        float emeraldOrbitRadius = 4200;
        float emeraldAngle = (float)(Math.random() * 360);
        float emeraldOrbitPeriod = AIW_SystemGenUtils.getOrbitalPeriod(gemstone, emeraldOrbitRadius);
        PlanetAPI emerald = AIW_SystemGenUtils.addPlanetToSystem(system, "aiw_emerald", gemstone, "jungle", emeraldRadius, emeraldOrbitRadius, false, emeraldAngle);
        emerald.setCustomDescriptionId("planet_emerald");
        emerald.setInteractionImage("illustrations", "space_bar");
        emerald.getSpec().setGlowTexture(Global.getSettings().getSpriteName("hab_glows", "volturn"));
        emerald.getSpec().setGlowColor(new Color(255,255,255,255));
        emerald.getSpec().setUseReverseLightForGlow(true);
        emerald.applySpecChanges();
        
        // Minaret Station
        float minaretOrbitRadius = emeraldRadius + 100;
        float minaretOrbitPeriod = AIW_SystemGenUtils.getOrbitalPeriod(emerald, minaretOrbitRadius);
        SectorEntityToken station_minaret = system.addCustomEntity("aiw_minaret", AIW_StringHelper.getString("aiw_planets", "aiw_minaret_station"), "aiw_station_spirehab", "spire");
        station_minaret.setCircularOrbitPointingDown(emerald, 120, minaretOrbitRadius, minaretOrbitPeriod);
        station_minaret.setInteractionImage("illustrations", "jangala_station");
        station_minaret.setCustomDescriptionId("station_minaret");
        
        AIW_SystemGenUtils.addMarketplace("spire",  
                emerald, 
                new ArrayList<>(Arrays.asList((SectorEntityToken) emerald, station_minaret)),  
                emerald.getName(),
                5,  
                new ArrayList<>(Arrays.asList(Conditions.JUNGLE, Conditions.POPULATION_5, "aiw_inorganic_populace",
                        Conditions.ORGANICS_COMPLEX, Conditions.LIGHT_INDUSTRIAL_COMPLEX,
                        Conditions.TRADE_CENTER, Conditions.ORBITAL_STATION, Conditions.VOLATILES_DEPOT)),  
                new ArrayList<>(Arrays.asList(Submarkets.SUBMARKET_STORAGE, Submarkets.SUBMARKET_BLACK, Submarkets.SUBMARKET_OPEN)),  
                0.3f
        );
        
        // Diamond (third planet)
        float diamondRadius = 165;
        float diamondOrbitRadius = 7000;
        
        PlanetAPI diamond = AIW_SystemGenUtils.addPlanetToSystem(system, "aiw_diamond", gemstone, "rocky_ice", diamondRadius, diamondOrbitRadius, false);
        diamond.setFaction("spire");
        diamond.setInteractionImage("illustrations", "city_from_above");
        diamond.setCustomDescriptionId("planet_diamond");
        diamond.getSpec().setGlowTexture(Global.getSettings().getSpriteName("hab_glows", "volturn"));
        diamond.getSpec().setGlowColor(new Color(255,255,255,255));
        diamond.getSpec().setUseReverseLightForGlow(true);
        diamond.applySpecChanges();
        
        // Zircon (moon of Diamond)
        float zirconRadius = 35;
        float zirconOrbitRadius = diamondRadius + 200;
        
        PlanetAPI zircon = AIW_SystemGenUtils.addPlanetToSystem(system, "aiw_zircon", diamond, "barren", zirconRadius, zirconOrbitRadius, false);
        zircon.setFaction("spire");
        zircon.setCustomDescriptionId("planet_zircon");
        
        AIW_SystemGenUtils.addMarketplace("spire",  
                diamond,  
                new ArrayList<>(Arrays.asList((SectorEntityToken)diamond, (SectorEntityToken)zircon)),  
                diamond.getName(),
                6,  
                new ArrayList<>(Arrays.asList(Conditions.ICE, Conditions.POPULATION_6, Conditions.MILITARY_BASE, Conditions.SPACEPORT, "aiw_inorganic_populace",
                        Conditions.ORE_COMPLEX, Conditions.ORE_COMPLEX, Conditions.ORE_COMPLEX, Conditions.ORE_COMPLEX, 
                        Conditions.ORE_REFINING_COMPLEX, Conditions.ORE_REFINING_COMPLEX, Conditions.AUTOFAC_HEAVY_INDUSTRY, Conditions.LIGHT_INDUSTRIAL_COMPLEX,
                        Conditions.ANTIMATTER_FUEL_PRODUCTION, Conditions.HEADQUARTERS, Conditions.URBANIZED_POLITY)),  
                new ArrayList<>(Arrays.asList(Submarkets.SUBMARKET_STORAGE, Submarkets.SUBMARKET_BLACK, Submarkets.SUBMARKET_OPEN)),  
                0.3f
        );
        
        // relay
        float relayOrbitRadius = 900;
        float relayOrbitPeriod = AIW_SystemGenUtils.getOrbitalPeriod(diamond, relayOrbitRadius);
        SectorEntityToken relay = system.addCustomEntity("aiw_gemstone_relay", // unique id
        AIW_StringHelper.getString("aiw_planets", "aiw_gemstone_relay"), // name - if null, defaultName from custom_entities.json will be used
        "comm_relay", // type of object, defined in custom_entities.json
        "spire"); // faction
        relay.setCircularOrbitPointingDown(diamond, 120, relayOrbitRadius, relayOrbitPeriod);
        
        // Sapphire (fourth planet)
        float sapphireRadius = 290;
        float sapphireOrbitRadius = 11000;
        float sapphireOrbitPeriod = AIW_SystemGenUtils.getOrbitalPeriod(gemstone, sapphireOrbitRadius);
        float sapphireAngle = (float)Math.random()*360;
        
        PlanetAPI sapphire = AIW_SystemGenUtils.addPlanetToSystem(system, "aiw_sapphire", gemstone, "ice_giant", sapphireRadius, sapphireOrbitRadius, false, sapphireAngle);
        AIW_SystemGenUtils.addMagneticField(sapphire);
        
        // Alumina (moon of Sapphire)
        float aluminaRadius = 100;
        float aluminaOrbitRadius = sapphireRadius + 600;
        
        PlanetAPI alumina = AIW_SystemGenUtils.addPlanetToSystem(system, "aiw_alumina", sapphire, "cryovolcanic", aluminaRadius, aluminaOrbitRadius, false);
        alumina.setCustomDescriptionId("planet_alumina");
        alumina.setInteractionImage("illustrations", "vacuum_colony");
        alumina.setFaction("spire");
        
        AIW_SystemGenUtils.addMarketplace("spire",  
                alumina,  
                new ArrayList<>(Arrays.asList((SectorEntityToken) alumina)),  
                alumina.getName(),
                4,  
                new ArrayList<>(Arrays.asList(Conditions.ICE, Conditions.POPULATION_4, Conditions.OUTPOST, "aiw_inorganic_populace", 
                        Conditions.VOLATILES_COMPLEX, Conditions.ORE_COMPLEX, Conditions.STEALTH_MINEFIELDS)),  
                new ArrayList<>(Arrays.asList(Submarkets.SUBMARKET_STORAGE, Submarkets.SUBMARKET_BLACK, Submarkets.SUBMARKET_OPEN)),  
                0.3f  
        );
        
        // secret treasure :)
        FleetMemberAPI member = Global.getFactory().createFleetMember(FleetMemberType.SHIP, "spire_champion_Hull");
        member.setShipName(AIW_StringHelper.getString("aiw_misc", "bonusShipName"));
        member.getRepairTracker().setMothballed(true);
        alumina.getMarket().getSubmarket(Submarkets.SUBMARKET_STORAGE).getCargo().getMothballedShips().addFleetMember(member);
        
        // ring (adapted from Magec.java)
        float ringOrbitRadius = 5900;
        AIW_SystemGenUtils.addRingBand(system, gemstone, "misc", "rings1", 256, 0, Color.white, 256, ringOrbitRadius - 75, 1);
        AIW_SystemGenUtils.addRingBand(system, gemstone, "misc", "rings1", 256, 0, Color.white, 256, ringOrbitRadius - 25, 1);
        AIW_SystemGenUtils.addRingBand(system, gemstone, "misc", "rings1", 256, 0, Color.white, 256, ringOrbitRadius + 25, 1);
        AIW_SystemGenUtils.addRingBand(system, gemstone, "misc", "rings1", 256, 1, Color.white, 256, ringOrbitRadius + 75, 1.1f);
        
        // add one ring that covers all of the above
        SectorEntityToken ring = system.addTerrain(Terrain.RING, new BaseRingTerrain.RingParams(150 + 160, ringOrbitRadius, null, 
                AIW_StringHelper.getString("aiw_planets", "aiw_gemstone_ring")));
        ring.setCircularOrbit(gemstone, 0, 0, AIW_SystemGenUtils.getOrbitalPeriod(gemstone, 5900));
        
        // asteroids (L4/L5 asteroids adapted from Askonia.java)
        float beltOrbitRadius = 8800;
        float beltOrbitTime = AIW_SystemGenUtils.getOrbitalPeriod(gemstone, beltOrbitRadius);
        float beltOrbitTimeFast = beltOrbitTime * 0.8f;
        float beltOrbitTimeSlow = beltOrbitTime * 1.2f;
        system.addAsteroidBelt(gemstone, 250, beltOrbitRadius, 500, beltOrbitTimeSlow, beltOrbitTimeFast, Terrain.ASTEROID_BELT, AIW_StringHelper.getString("aiw_planets", "aiw_gemstone_belt"));
        
        system.addRingBand(gemstone, "misc", "rings1", 256f, 2, Color.white, 256f, beltOrbitRadius - 200, beltOrbitTimeFast);
        system.addRingBand(gemstone, "misc", "rings1", 256f, 3, Color.white, 256f, beltOrbitRadius, beltOrbitTimeSlow);
        system.addRingBand(gemstone, "misc", "rings1", 256f, 2, Color.white, 256f, beltOrbitRadius + 200, beltOrbitTimeFast);
        
        SectorEntityToken sapphireL4 = system.addTerrain(Terrain.ASTEROID_FIELD,
                    new AsteroidFieldTerrainPlugin.AsteroidFieldParams(
                        400f, // min radius
                        600f, // max radius
                        20, // min asteroid count
                        30, // max asteroid count
                        4f, // min asteroid radius 
                        16f, // max asteroid radius
                        AIW_StringHelper.getString("aiw_planets", "aiw_gemstone_asteroids_l4"))); // null for default name
        SectorEntityToken sapphireL5 = system.addTerrain(Terrain.ASTEROID_FIELD,
                    new AsteroidFieldTerrainPlugin.AsteroidFieldParams(
                        400f, // min radius
                        600f, // max radius
                        20, // min asteroid count
                        30, // max asteroid count
                        4f, // min asteroid radius 
                        16f, // max asteroid radius
                        AIW_StringHelper.getString("aiw_planets", "aiw_gemstone_asteroids_l5"))); // null for default name
        sapphireL4.setCircularOrbit(gemstone, sapphireAngle - 60, sapphireOrbitRadius, sapphireOrbitPeriod);    // ahead
        sapphireL5.setCircularOrbit(gemstone, sapphireAngle + 60, sapphireOrbitRadius, sapphireOrbitPeriod);    // behind
        
        // nebula
        SectorEntityToken gemstone_nebula = Misc.addNebulaFromPNG("data/campaign/terrain/AIWar/gemstone_nebula.png",
                  0, 0, // center of nebula
                  system, // location to add to
                  "terrain", "nebula_blue", // "nebula_blue", // texture to use, uses xxx_map for map
                  4, 4); // number of cells in texture
        
        // jump points
        JumpPointAPI jumpPoint = Global.getFactory().createJumpPoint("aiw_gemstone_jump", AIW_StringHelper.getString("aiw_planets", "aiw_gemstone_jump"));
        jumpPoint.setCircularOrbit(gemstone, emeraldAngle - 60, emeraldOrbitRadius, emeraldOrbitPeriod);
        //jumpPoint.setRelatedPlanet(emerald);
        jumpPoint.setStandardWormholeToHyperspaceVisual();
        system.addEntity(jumpPoint);
        
        system.autogenerateHyperspaceJumpPoints(true, true);
    }
}
