package data.scripts.world.systems;

import com.fs.starfarer.api.Global;
//import com.fs.starfarer.api.InteractionDialogImageVisual;
import com.fs.starfarer.api.campaign.JumpPointAPI;
import com.fs.starfarer.api.campaign.LocationAPI;
import com.fs.starfarer.api.campaign.PlanetAPI;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import java.awt.Color;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.Submarkets;
import com.fs.starfarer.api.impl.campaign.ids.Terrain;
import com.fs.starfarer.api.impl.campaign.terrain.BaseRingTerrain;
import com.fs.starfarer.api.util.Misc;
import data.scripts.util.AIW_StringHelper;
import java.util.ArrayList;
import java.util.Arrays;

public class AIW_Optica {
    
    //addPlanet(String id,  SectorEntityToken focus,name, type, angle, radius, orbitRadius, orbitDays
    
    public void generate(SectorAPI sector) {
        // first get rid of the hyperspace on top of us
		/*
		for (CampaignTerrainAPI terrain : sector.getHyperspace().getTerrainCopy()) {
            if (terrain.getType().contentEquals(Terrain.HYPERSPACE)) {
                AIW_SystemGenUtils.removePNGFromNebula("data/campaign/terrain/AIWar/optica_hyperspace_mask.png", terrain);
            }
        }
		*/
		
        // inits
        StarSystemAPI system = sector.createStarSystem("Optica");
        system.getLocation().set(-9130, 5396);
        LocationAPI hyper = Global.getSector().getHyperspace();
        
        system.setBackgroundTextureFilename("graphics/AIWar/backgrounds/optica.jpg");
        
        // star (Optica)
        float opticaRadius = 620f;
        PlanetAPI optica = system.initStar("aiw_optica", "star_yellow", opticaRadius, 500);
		system.setLightColor(new Color(200, 240, 255));

        // Mirage (first planet)
        float mirageRadius = 140;
        float mirageOrbitRadius = 2700;
		float mirageAngle = (float)Math.random() * 360;
        PlanetAPI mirage = AIW_SystemGenUtils.addPlanetToSystem(system, "aiw_mirage", optica, "barren-desert", mirageRadius, mirageOrbitRadius, true, mirageAngle);
        mirage.setFaction("spire");
		mirage.setCustomDescriptionId("planet_mirage");
		mirage.setInteractionImage("illustrations", "desert_moons_ruins");
		mirage.getSpec().setGlowTexture(Global.getSettings().getSpriteName("hab_glows", "volturn"));
        mirage.getSpec().setGlowColor(new Color(255,255,255,255));
        mirage.getSpec().setUseReverseLightForGlow(true);
		
		SectorEntityToken shade = system.addCustomEntity("aiw_mirage_shade", AIW_StringHelper.getString("aiw_planets", "shade"), 
				"stellar_shade", "spire");
		shade.setCircularOrbitPointingDown(mirage, mirageAngle + 180, mirageRadius + 150, 
				-AIW_SystemGenUtils.getOrbitalPeriod(optica, mirageOrbitRadius));		
		shade.setCustomDescriptionId("stellar_shade");
        
		AIW_SystemGenUtils.addMarketplace("spire",  
                mirage, 
                new ArrayList<>(Arrays.asList((SectorEntityToken)mirage)),  
                mirage.getName(),  
                4,  
                new ArrayList<>(Arrays.asList("barren_marginal", Conditions.POPULATION_4, "aiw_inorganic_populace",
                        Conditions.ORE_COMPLEX, Conditions.ORE_COMPLEX, Conditions.MILITARY_BASE)),  
                new ArrayList<>(Arrays.asList(Submarkets.SUBMARKET_STORAGE, Submarkets.SUBMARKET_BLACK, Submarkets.SUBMARKET_OPEN)),  
                0.3f
        );
		
        // Looming (second planet)
        float loomingRadius = 210;
        float loomingOrbitRadius = 4550;
		float loomingOrbitPeriod = AIW_SystemGenUtils.getOrbitalPeriod(optica, loomingOrbitRadius);
		float loomingAngle = (float)Math.random() * 360;
        PlanetAPI looming = AIW_SystemGenUtils.addPlanetToSystem(system, "aiw_looming", optica, "water", loomingRadius, loomingOrbitRadius, false, loomingAngle);
        looming.setFaction(Factions.INDEPENDENT);
		looming.setCustomDescriptionId("planet_looming");
        looming.setInteractionImage("illustrations", "cargo_loading");
        looming.getSpec().setGlowTexture(Global.getSettings().getSpriteName("hab_glows", "volturn"));
        looming.getSpec().setGlowColor(new Color(255,255,255,255));
        looming.getSpec().setUseReverseLightForGlow(true);
        looming.applySpecChanges();
        
        AIW_SystemGenUtils.addMarketplace(Factions.INDEPENDENT,  
                looming, 
                new ArrayList<>(Arrays.asList((SectorEntityToken) looming)),  
                looming.getName(),
                5,  
                new ArrayList<>(Arrays.asList(Conditions.WATER, Conditions.POPULATION_5, 
						Conditions.ORGANICS_COMPLEX, Conditions.LIGHT_INDUSTRIAL_COMPLEX,
						Conditions.VOLATILES_DEPOT, Conditions.SPACEPORT, Conditions.TRADE_CENTER)),  
                new ArrayList<>(Arrays.asList(Submarkets.SUBMARKET_STORAGE, Submarkets.SUBMARKET_BLACK, Submarkets.SUBMARKET_OPEN)),  
                0.3f
        );
		
		// ring (adapted from Magec.java)
		float ring1Dist = loomingRadius + 150;
		float ring2Dist = loomingRadius + 170;
		float ring3Dist = loomingRadius + 190;
        system.addRingBand(looming, "misc", "rings1", 256f, 0, Color.white, 256f, ring1Dist, 
				AIW_SystemGenUtils.getOrbitalPeriod(looming, ring1Dist));
        system.addRingBand(looming, "misc", "rings1", 256f, 0, Color.white, 256f, ring2Dist,
				AIW_SystemGenUtils.getOrbitalPeriod(looming, ring2Dist));
        system.addRingBand(looming, "misc", "rings1", 256f, 1, Color.white, 256f, ring3Dist,
				AIW_SystemGenUtils.getOrbitalPeriod(looming, ring2Dist));
        
        // add one ring that covers all of the above
        SectorEntityToken ring = system.addTerrain(Terrain.RING, new BaseRingTerrain.RingParams(40 + 160, ring2Dist, null, 
				AIW_StringHelper.getString("aiw_planets", "aiw_optica_ring")));
        ring.setCircularOrbit(looming, 0, 0, AIW_SystemGenUtils.getOrbitalPeriod(looming, ring2Dist));
		
		// relay
        SectorEntityToken relay = system.addCustomEntity("aiw_optica_relay", // unique id
        AIW_StringHelper.getString("aiw_planets", "aiw_optica_relay"), // name - if null, defaultName from custom_entities.json will be used
        "comm_relay", // type of object, defined in custom_entities.json
        Factions.INDEPENDENT); // faction
        relay.setCircularOrbitPointingDown(optica, loomingAngle + 60, loomingOrbitRadius, loomingOrbitPeriod);
        
        // Silhouette (third planet)
        float silhouetteRadius = 180;
        float silhouetteOrbitRadius = 7800;
        
		PlanetAPI silhouette = AIW_SystemGenUtils.addPlanetToSystem(system, "aiw_silhouette", optica, "terran-eccentric", silhouetteRadius, silhouetteOrbitRadius, true);
        silhouette.setFaction(Factions.PIRATES);
        silhouette.setInteractionImage("illustrations", "hound_hangar");
        silhouette.setCustomDescriptionId("planet_silhouette");
        silhouette.getSpec().setGlowTexture(Global.getSettings().getSpriteName("hab_glows", "volturn"));
        silhouette.getSpec().setGlowColor(new Color(255,255,255,255));
        silhouette.getSpec().setUseReverseLightForGlow(true);
		silhouette.getSpec().setRotation(0);
        silhouette.applySpecChanges();
        
        SectorEntityToken station = system.addCustomEntity("aiw_shadow_station", AIW_StringHelper.getString("aiw_planets", "aiw_shadow_station"),
				"station_side04", Factions.PIRATES);
		station.setCircularOrbitPointingDown(silhouette, 45, 320, 50);	
		station.setInteractionImage("illustrations", "pirate_station");
        station.setCustomDescriptionId("aiw_station_shadow");
		
        AIW_SystemGenUtils.addMarketplace(Factions.PIRATES,  
                silhouette,  
                new ArrayList<>(Arrays.asList(silhouette, station)),  
                silhouette.getName(),
                4,  
                new ArrayList<>(Arrays.asList("twilight", Conditions.POPULATION_4, Conditions.MILITARY_BASE, Conditions.ORBITAL_STATION,
						Conditions.ORE_REFINING_COMPLEX, Conditions.HYDROPONICS_COMPLEX, Conditions.ORGANIZED_CRIME, Conditions.FREE_PORT)),  
                new ArrayList<>(Arrays.asList(Submarkets.SUBMARKET_STORAGE, Submarkets.SUBMARKET_BLACK, Submarkets.SUBMARKET_OPEN)),  
                0.3f
        );
		
		// Albedo (moon of Silhouette)
		float albedoRadius = 50;
        float albedoOrbitRadius = silhouetteRadius + 400;
        
		PlanetAPI albedo = AIW_SystemGenUtils.addPlanetToSystem(system, "aiw_albedo", silhouette, "tundra", albedoRadius, albedoOrbitRadius, true);
        albedo.setCustomDescriptionId("planet_albedo");
        
        // Specula (fourth planet)
        float speculaRadius = 110;
        float speculaOrbitRadius = 10900;
        
        PlanetAPI specula = AIW_SystemGenUtils.addPlanetToSystem(system, "aiw_specula", optica, "frozen", speculaRadius, speculaOrbitRadius, false);
        
        // asteroids (L4/L5 asteroids adapted from Askonia.java)
		float beltOrbitRadius = 6500;
		float beltOrbitTime = AIW_SystemGenUtils.getOrbitalPeriod(optica, beltOrbitRadius);
		float beltOrbitTimeFast = beltOrbitTime * 0.8f;
		float beltOrbitTimeSlow = beltOrbitTime * 1.2f;
        system.addAsteroidBelt(optica, 250, beltOrbitRadius, 500, beltOrbitTimeSlow, beltOrbitTimeFast, Terrain.ASTEROID_BELT, AIW_StringHelper.getString("aiw_planets", "aiw_optica_belt"));
		
		system.addRingBand(optica, "misc", "rings1", 256f, 0, Color.white, 256f, beltOrbitRadius - 200, beltOrbitTimeFast);
		system.addRingBand(optica, "misc", "rings1", 256f, 0, Color.white, 256f, beltOrbitRadius, beltOrbitTimeSlow);
		system.addRingBand(optica, "misc", "rings1", 256f, 1, Color.white, 256f, beltOrbitRadius + 200, beltOrbitTimeFast);
        
        // nebula?
        SectorEntityToken optica_nebula = Misc.addNebulaFromPNG("data/campaign/terrain/AIWar/optica_nebula.png",
                  0, 0, // center of nebula
                  system, // location to add to
                  "terrain", "nebula", // "nebula_blue", // texture to use, uses xxx_map for map
                  4, 4); // number of cells in texture
        
        // jump points
        JumpPointAPI jumpPoint = Global.getFactory().createJumpPoint("aiw_optica_jump", AIW_StringHelper.getString("aiw_planets", "aiw_optica_jump"));
        jumpPoint.setCircularOrbit(optica, loomingAngle - 60, loomingOrbitRadius, loomingOrbitPeriod);
        jumpPoint.setStandardWormholeToHyperspaceVisual();
        system.addEntity(jumpPoint);
        
        system.autogenerateHyperspaceJumpPoints(true, true);
    }
}
