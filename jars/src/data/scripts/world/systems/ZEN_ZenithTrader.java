package data.scripts.world.systems;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.characters.ImportantPeopleAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.impl.campaign.ids.Ranks;
import com.fs.starfarer.api.impl.campaign.ids.Submarkets;
import com.fs.starfarer.api.impl.campaign.shared.SharedData;
import data.scripts.util.AIW_StringHelper;
import java.util.List;
import org.lazywizard.lazylib.MathUtils;

public class ZEN_ZenithTrader {
	public static final String TRADER_ENTITY_ID = "zen_zenithtrader";
	
	public void generate(SectorAPI sector) {
		String traderName = AIW_StringHelper.getString("aiw_planets", TRADER_ENTITY_ID);
		SectorEntityToken toOrbit = sector.getHyperspace().createToken(0, 0);
		SectorEntityToken trader = toOrbit.getContainingLocation().addCustomEntity(TRADER_ENTITY_ID, 
				traderName, "zen_zenithtrader", "zenith");
		trader.setCircularOrbit(toOrbit, MathUtils.getRandomNumberInRange(0, 360), 2000, 90);
		
		MarketAPI market = Global.getFactory().createMarket(TRADER_ENTITY_ID  /*+ "_market"*/, traderName, 5);
		market.setFactionId("zenith");
		market.addCondition(Conditions.SPACEPORT);
		market.addCondition(Conditions.TRADE_CENTER);
		market.addCondition(Conditions.FREE_PORT);
		market.addCondition(Conditions.VOLATILES_DEPOT);
		market.addSubmarket(Submarkets.SUBMARKET_OPEN);
		market.addSubmarket("zen_zenithtrader");
		market.addSubmarket(Submarkets.SUBMARKET_STORAGE);
		market.setBaseSmugglingStabilityValue(0);
		
		market.getTariff().modifyFlat("default_tariff", 0.3f);
		market.setPrimaryEntity(trader);
		trader.setMarket(market);
		trader.setFaction("zenith");
		trader.addTag("uninvadable");	// Nexerelin tag
		trader.addTag("noFoundationRepresentative");
		SharedData.getData().getMarketsWithoutPatrolSpawn().add(TRADER_ENTITY_ID);
		SharedData.getData().getMarketsWithoutTradeFleetSpawn().add(TRADER_ENTITY_ID);
		sector.getEconomy().addMarket(market);
		
		trader.setCustomDescriptionId("zen_zenithtrader");
	}
	
	public static void clearPeople()
	{
		// remove Zenith trader's post people
		MarketAPI market = Global.getSector().getEconomy().getMarket(ZEN_ZenithTrader.TRADER_ENTITY_ID);
		List<PersonAPI> people = market.getPeopleCopy();
		ImportantPeopleAPI ip = Global.getSector().getImportantPeople();
		for (PersonAPI person : people)
		{
			if (person.getPost().equals(Ranks.POST_BASE_COMMANDER)
					|| person.getPost().equals(Ranks.POST_OUTPOST_COMMANDER)
					|| person.getPost().equals(Ranks.POST_STATION_COMMANDER)
					|| person.getPost().equals(Ranks.POST_PORTMASTER)
					|| person.getPost().equals(Ranks.POST_SUPPLY_MANAGER)
					)
			{
				market.getCommDirectory().removePerson(person);
				market.removePerson(person);
				ip.removePerson(person);
			}
		}
	}
}
