package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import data.scripts.plugins.AIW_ForcefieldPlugin;

public class AIW_BlossomFieldStats implements ShipSystemStatsScript {

        @Override
	public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
		ShipAPI ship = (ShipAPI) stats.getEntity();
                AIW_ForcefieldPlugin.setBlossomFieldState(ship, true);
	}
        
        @Override
	public void unapply(MutableShipStatsAPI stats, String id) {
		ShipAPI ship = (ShipAPI) stats.getEntity();
                AIW_ForcefieldPlugin.setBlossomFieldState(ship, false);
	}
	
        @Override
	public StatusData getStatusData(int index, State state, float effectLevel) {
		if (index == 0) {
			return new StatusData("3x forcefield radius", false);
		}
		else if (index == 1) {
			return new StatusData("forcefield absorbs 2x damage", false);
                }
		return null;
	}
}
