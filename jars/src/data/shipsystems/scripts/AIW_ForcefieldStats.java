package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import data.scripts.plugins.AIW_ForcefieldPlugin;

@Deprecated
public class AIW_ForcefieldStats implements ShipSystemStatsScript {

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        ShipAPI ship = (ShipAPI) stats.getEntity();
        //AIW_ForcefieldPlugin.activateForcefield(ship);
        if (ship.getShipAI() != null) return;    // AI handles it through its own code path
        AIW_ForcefieldPlugin.toggleForcefield(ship);
    }

    @Override
    public ShipSystemStatsScript.StatusData getStatusData(int index, ShipSystemStatsScript.State state, float effectLevel) {
        return null;
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        //ShipAPI ship = (ShipAPI) stats.getEntity();
        //AIW_ForcefieldPlugin.deactivateForcefield(ship);
    }
}