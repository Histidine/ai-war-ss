package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import java.awt.Color;
import org.dark.shaders.light.LightShader;
import org.dark.shaders.light.StandardLight;
import org.lwjgl.util.vector.Vector2f;

public class AIW_GemfireStats implements ShipSystemStatsScript {

	public static final float DAMAGE_BONUS_PERCENT = 50f;
	public static final float FLUX_REDUCTION = 0.3f;
    public static final float ROF_BONUS = 1f;
	protected static final Color LIGHT_COLOR = new Color(1, 0.4f, 0.3f, 0.5f);
	protected static final Vector2f ZERO = new Vector2f();
	protected StandardLight light;
	protected boolean isActive = false;
	
	@Override
	public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
		ShipAPI ship = (ShipAPI) stats.getEntity();
		Vector2f loc = new Vector2f(ship.getLocation());
		if (!isActive)
		{
			isActive = true;
			light = new StandardLight(loc, ZERO, ZERO, null);
			light.setIntensity(0.25f * effectLevel);
			light.setSize(ship.getCollisionRadius() * 2f);
			light.setColor(LIGHT_COLOR);
			light.fadeIn(1.95f);
			LightShader.addLight(light);
		}
		else
		{
			light.setLocation(loc);
			light.setIntensity(0.25f * effectLevel);
		}
		float bonusPercent = DAMAGE_BONUS_PERCENT * effectLevel;
		float mult = 1f + ROF_BONUS * effectLevel;
        float fluxMult = (1f - FLUX_REDUCTION * effectLevel);
        stats.getBallisticRoFMult().modifyMult(id, mult);
        stats.getBallisticWeaponFluxCostMod().modifyMult(id, fluxMult);
		stats.getEnergyWeaponDamageMult().modifyPercent(id, bonusPercent);
		stats.getMissileRoFMult().modifyMult(id, mult);
	}
	
	@Override
	public void unapply(MutableShipStatsAPI stats, String id) {
		stats.getEnergyWeaponDamageMult().unmodify(id);
		stats.getBallisticRoFMult().unmodify(id);
        stats.getBallisticWeaponFluxCostMod().unmodify(id);
		stats.getMissileRoFMult().unmodify(id);
		LightShader.removeLight(light);
		isActive = false;
	}
	
	@Override
	public StatusData getStatusData(int index, State state, float effectLevel) {
		float bonusPercentEnergy = DAMAGE_BONUS_PERCENT * effectLevel;
		float mult = 1f + ROF_BONUS * effectLevel;
        int bonusPercentBallistic = (int) ((mult - 1f) * 100f);
        int reductionPercent = (int) (FLUX_REDUCTION * effectLevel * 100f);
		if (index == 0) {
			return new StatusData("+" + (int) bonusPercentEnergy + "% energy weapon damage", false);
		} else if (index == 1) {
            return new StatusData("+" + bonusPercentBallistic + "% ballistic/missile rate of fire ", false);
        } else if (index == 2) {
            return new StatusData("-" + reductionPercent + "% ballistic flux cost", false);
        }
		return null;
	}
}
