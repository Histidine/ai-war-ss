package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import data.scripts.plugins.AIW_GravityDrainPlugin;

public class AIW_GravityDrainStats extends BaseEveryFrameCombatPlugin implements ShipSystemStatsScript {

	ShipAPI ship = null;
    
	@Override
	public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
		AIW_GravityDrainPlugin.addOrUpdateGravityDrain((ShipAPI)stats.getEntity(), effectLevel);
	}
	
	@Override
	public void unapply(MutableShipStatsAPI stats, String id) {
		AIW_GravityDrainPlugin.removeGravityDrain((ShipAPI)stats.getEntity());
	}
	
	@Override
	public StatusData getStatusData(int index, State state, float effectLevel) {
		if (index == 0) {
			int slowdown = (int)(AIW_GravityDrainPlugin.DRAIN_SLOW_FACTOR * effectLevel * 100);
			
			return new StatusData("-" + slowdown + "% enemy speed", false);
		}
		return null;
	}
}
