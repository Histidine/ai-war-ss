package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import java.awt.Color;
import java.util.List;

public class AIW_MatrixHardeningStats extends BaseEveryFrameCombatPlugin implements ShipSystemStatsScript {

    public static final float DAMAGE_REDUCTION_HE = 0.75f;
    public static final float DAMAGE_REDUCTION_KINETIC = 0.75f;
    public static final float DAMAGE_REDUCTION_ENERGY = 0.75f;
    public static final float DAMAGE_REDUCTION_FRAG = 0.75f;
    public static final float DAMAGE_REDUCTION_ARMOR = 0.75f;
	public static final float DAMAGE_REDUCTION_EMP = 0.75f;
    
    protected static final float[] HULL_COLOR_CHANGE = {-0.2f, -0.1f, 0, 0};
    protected static final float[] GLOW_COLOR = {0.5f, 0.75f, 1, 0.5f};
    
    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        //stats.getHighExplosiveDamageTakenMult().modifyMult(id, 1f - DAMAGE_REDUCTION_HE * effectLevel);
        //stats.getFragmentationDamageTakenMult().modifyMult(id, 1f - DAMAGE_REDUCTION_FRAG * effectLevel);
        //stats.getKineticDamageTakenMult().modifyMult(id, 1f - DAMAGE_REDUCTION_KINETIC * effectLevel);
        //stats.getEnergyDamageTakenMult().modifyMult(id, 1f - DAMAGE_REDUCTION_ENERGY * effectLevel);
        stats.getArmorDamageTakenMult().modifyMult(id, 1f - DAMAGE_REDUCTION_ARMOR * effectLevel);
		stats.getEmpDamageTakenMult().modifyMult(id, 1f - DAMAGE_REDUCTION_EMP * effectLevel);
        
        ShipAPI owner = (ShipAPI) stats.getEntity();
        if (owner != null) {
            Color color = new Color(1 + HULL_COLOR_CHANGE[0] * effectLevel, 1 + HULL_COLOR_CHANGE[1] * effectLevel, 
                    1 + HULL_COLOR_CHANGE[2] * effectLevel, 1 + HULL_COLOR_CHANGE[3] * effectLevel);
            Color glowColor = new Color(GLOW_COLOR[0], GLOW_COLOR[1], GLOW_COLOR[2], GLOW_COLOR[3] * effectLevel);
            
            WeaponAPI dec = null;
            List<WeaponAPI> weapons = owner.getAllWeapons();
            for (WeaponAPI weapon : weapons) {
                if (weapon.getId().startsWith("spire_matrix")) {
                    dec = weapon;
                }
            }
            if (dec != null)
            {
                dec.getAnimation().setFrame(1);
                dec.getSprite().setColor(glowColor);
            }
            owner.getSpriteAPI().setColor(color);
        }
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
            //return new StatusData("-" + (int) (effectLevel * DAMAGE_REDUCTION_HE * 100) + "% damage taken", false);
            return new StatusData("-" + (int) (effectLevel * DAMAGE_REDUCTION_ARMOR * 100) + "% armor damage taken", false);
        }
        return null;
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        //stats.getHighExplosiveDamageTakenMult().unmodify(id);
        //stats.getFragmentationDamageTakenMult().unmodify(id);
        //stats.getKineticDamageTakenMult().unmodify(id);
        //stats.getEnergyDamageTakenMult().unmodify(id);
        stats.getArmorDamageTakenMult().unmodify(id);
        
        ShipAPI owner = (ShipAPI) stats.getEntity();
        if (owner != null) {
            owner.getSpriteAPI().setColor(Color.WHITE);
            WeaponAPI dec = null;
            List<WeaponAPI> weapons = owner.getAllWeapons();
            for (WeaponAPI weapon : weapons) {
                if (weapon.getId().startsWith("spire_matrix")) {
                    dec = weapon;
                }
            }
            if (dec != null)
            {
                dec.getAnimation().setFrame(0);
            }
            owner.getSpriteAPI().setColor(Color.WHITE);
        }
    }
}
