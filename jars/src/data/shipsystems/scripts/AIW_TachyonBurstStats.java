package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import data.scripts.hullmods.TEM_LatticeShield;
import data.scripts.AIWarModPlugin;
import data.scripts.util.AIWTwig;
import data.scripts.util.AIW_AnamorphicFlare;
import java.awt.Color;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.dark.shaders.distortion.DistortionShader;
import org.dark.shaders.distortion.WaveDistortion;
import org.dark.shaders.light.LightShader;
import org.dark.shaders.light.StandardLight;
import org.lazywizard.lazylib.FastTrig;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

// pretty much just copypasta from Blackrock's deracinator
// NOTE: does not currently apply any force
public class AIW_TachyonBurstStats implements ShipSystemStatsScript {

    protected static final String CHARGEUP_SOUND = "aiw_tachyonburst_charge";
    protected static final Map<HullSize, Integer> NUM_ZAPS = new HashMap<>(6);
    protected static final Map<HullSize, Float> DAMAGE_MULT = new HashMap<>(6);
    
    static {
        NUM_ZAPS.put(HullSize.DEFAULT, 8);
        NUM_ZAPS.put(HullSize.FIGHTER, 2);
        NUM_ZAPS.put(HullSize.FRIGATE, 4);
        NUM_ZAPS.put(HullSize.DESTROYER, 8);
        NUM_ZAPS.put(HullSize.CRUISER, 12);
        NUM_ZAPS.put(HullSize.CAPITAL_SHIP, 20);
        
        DAMAGE_MULT.put(HullSize.DEFAULT, 1f);
        DAMAGE_MULT.put(HullSize.FIGHTER, 0.5f);
        DAMAGE_MULT.put(HullSize.FRIGATE, 0.75f);
        DAMAGE_MULT.put(HullSize.DESTROYER, 1f);
        DAMAGE_MULT.put(HullSize.CRUISER, 1.25f);
        DAMAGE_MULT.put(HullSize.CAPITAL_SHIP, 1.5f);
    }

    // Distortion constants
    protected static final float DISTORTION_BLAST_RADIUS = 1200f;
    protected static final float DISTORTION_CHARGE_RADIUS = 350f;

    // Explosion effect constants
    protected static final Color EXPLOSION_COLOR = new Color(44, 96, 168);
    protected static final float EXPLOSION_DAMAGE_AMOUNT = 1800f;
    protected static final DamageType EXPLOSION_DAMAGE_TYPE = DamageType.ENERGY;
    protected static final float EXPLOSION_DAMAGE_VS_ALLIES_MODIFIER = .11f;
    protected static final float EXPLOSION_EMP_DAMAGE_AMOUNT = 3000f;
    protected static final float EXPLOSION_EMP_VS_ALLIES_MODIFIER = .05f;
    protected static final float EXPLOSION_FORCE_VS_ALLIES_MODIFIER = .3f;
    public static final float EXPLOSION_PUSH_RADIUS = 900f;
    protected static final String EXPLOSION_SOUND = "aiw_tachyonburst_fire";
    protected static final float EXPLOSION_VISUAL_RADIUS = 1250f;
    protected static final Color FLARE_COLOR = new Color(44, 221, 242);
    
    protected static final float FORCE_VS_ASTEROID = 290f;
    protected static final float FORCE_VS_CAPITAL = 35f;
    protected static final float FORCE_VS_CRUISER = 60f;
    protected static final float FORCE_VS_DESTROYER = 115f;
    protected static final float FORCE_VS_FIGHTER = 255f;
    protected static final float FORCE_VS_FRIGATE = 180f;
    
    protected static final int MAX_PARTICLES_PER_FRAME_IN = 30; // Based on charge level
    protected static final int MAX_PARTICLES_PER_FRAME_OUT = 90;
    
    // "Inhale"/"exhale" effect constants
    protected static final Color PARTICLE_COLOR = new Color(155, 200, 240);
    protected static final float PARTICLE_OPACITY = 0.85f;
    protected static final float PARTICLE_INTAKE_RADIUS = 600f;
    protected static final float PARTICLE_EJECT_RADIUS = 50f;
    protected static final float PARTICLE_EJECT_SPEED_MULT = 20f;
    protected static final float PARTICLE_SIZE_IN = 8f;
    protected static final float PARTICLE_SIZE_OUT = 12f;

    protected static final Vector2f ZERO = new Vector2f();

    //Local variables, don't touch these
    protected boolean isActive = false;
    protected StandardLight light;
    protected WaveDistortion wave;

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        // instanceof also acts as a null check
        if (!(stats.getEntity() instanceof ShipAPI)) {
            return;
        }

        ShipAPI ship = (ShipAPI) stats.getEntity();
        // Chargeup, show particle inhalation effect
        if (state == State.IN) {
            Vector2f loc = new Vector2f(ship.getLocation());
            loc.x -= 70f * FastTrig.cos(ship.getFacing() * Math.PI / 180f);
            loc.y -= 70f * FastTrig.sin(ship.getFacing() * Math.PI / 180f);

            // Everything in this block is only done once per chargeup
            if (!isActive) {
                isActive = true;
                Global.getSoundPlayer().playSound(CHARGEUP_SOUND, 1f, 1f, ship.getLocation(), ship.getVelocity());

                
                light = new StandardLight(loc, ZERO, ZERO, null);
                light.setIntensity(1.25f);
                light.setSize(EXPLOSION_VISUAL_RADIUS);
                light.setColor(PARTICLE_COLOR);
                light.fadeIn(1.95f);
                light.setLifetime(0.1f);
                light.setAutoFadeOutTime(0.17f);
                LightShader.addLight(light);

                /*
                wave = new WaveDistortion(loc, ZERO);
                wave.setSize(DISTORTION_CHARGE_RADIUS);
                wave.setIntensity(DISTORTION_CHARGE_RADIUS / 4f);
                wave.fadeInSize(1.95f);
                wave.fadeInIntensity(1.95f);
                wave.setLifetime(0f);
                wave.setAutoFadeSizeTime(-0.5f);
                wave.setAutoFadeIntensityTime(0.17f);
                DistortionShader.addDistortion(wave);
                */
            } else {
                light.setLocation(loc);
                //wave.setLocation(loc);
            }

            // Exact amount per second doesn't matter since it's purely decorative
            Vector2f particlePos, particleVel;
            int numParticlesThisFrame = Math.round(effectLevel * MAX_PARTICLES_PER_FRAME_IN);
            for (int x = 0; x < numParticlesThisFrame; x++) {
                particlePos = MathUtils.getRandomPointOnCircumference(ship.getLocation(), PARTICLE_INTAKE_RADIUS * MathUtils.getRandomNumberInRange(0.75f, 1.25f));
                particleVel = Vector2f.sub(ship.getLocation(), particlePos, null);
                Global.getCombatEngine().addSmoothParticle(particlePos, particleVel, PARTICLE_SIZE_IN, PARTICLE_OPACITY, 1f, PARTICLE_COLOR);
            }
        } 
        // Cooldown, explode once system is finished
        else if (state == State.OUT) {
            // Everything in this section is only done once per cooldown
            if (isActive) {
                CombatEngineAPI engine = Global.getCombatEngine();
                engine.spawnExplosion(ship.getLocation(), ship.getVelocity(), EXPLOSION_COLOR, EXPLOSION_VISUAL_RADIUS, 0.2f);
                engine.spawnExplosion(ship.getLocation(), ship.getVelocity(), EXPLOSION_COLOR, EXPLOSION_VISUAL_RADIUS / 2f, 0.2f);

                Vector2f loc = new Vector2f(ship.getLocation());
                loc.x -= 70f * FastTrig.cos(ship.getFacing() * Math.PI / 180f);
                loc.y -= 70f * FastTrig.sin(ship.getFacing() * Math.PI / 180f);

                light = new StandardLight();
                light.setLocation(loc);
                light.setIntensity(2f);
                light.setSize(EXPLOSION_VISUAL_RADIUS * 2f);
                light.setColor(EXPLOSION_COLOR);
                light.fadeOut(1.25f);
                LightShader.addLight(light);

                wave = new WaveDistortion();
                wave.setLocation(loc);
                wave.setSize(DISTORTION_BLAST_RADIUS);
                wave.setIntensity(DISTORTION_BLAST_RADIUS * 0.075f);
                wave.fadeInSize(1.2f);
                wave.fadeOutIntensity(0.9f);
                wave.setSize(DISTORTION_BLAST_RADIUS * 0.25f);
                DistortionShader.addDistortion(wave);

                Global.getSoundPlayer().playSound(EXPLOSION_SOUND, 1f, 1f, ship.getLocation(), ship.getVelocity());

                AIW_AnamorphicFlare.createFlare(ship, new Vector2f(loc), engine, 0.50f, 0.05f, -15f + (float) Math.random() * 30f, 9.25f, 6f, FLARE_COLOR,
                                            PARTICLE_COLOR);
                AIW_AnamorphicFlare.createFlare(ship, new Vector2f(loc), engine, 0.51f, 0.049f, -15f + (float) Math.random() * 60f, 8.95f, 6f, PARTICLE_COLOR,
                                            FLARE_COLOR);
                AIW_AnamorphicFlare.createFlare(ship, new Vector2f(loc), engine, 0.52f, 0.048f, -15f + (float) Math.random() * 30f, 7.55f, 6f, FLARE_COLOR,
                                            PARTICLE_COLOR);
                AIW_AnamorphicFlare.createFlare(ship, new Vector2f(loc), engine, 0.51f, 0.047f, -15f + (float) Math.random() * 90f, 10.95f, 6f, PARTICLE_COLOR,
                                            FLARE_COLOR);
                AIW_AnamorphicFlare.createFlare(ship, new Vector2f(loc), engine, 0.50f, 0.046f, -15f + (float) Math.random() * 120f, 8.55f, 6f, FLARE_COLOR,
                                            PARTICLE_COLOR);

                ShipAPI victim;
                Vector2f dir;
                float force, damage, emp, mod;
                List<CombatEntityAPI> entities = CombatUtils.getEntitiesWithinRange(ship.getLocation(), EXPLOSION_PUSH_RADIUS);
                int size = entities.size();
                for (int i = 0; i < size; i++) {
                    CombatEntityAPI tmp = entities.get(i);
                    if (tmp == ship) {
                        continue;
                    }

                    mod = 1f - ((MathUtils.getDistance(ship, tmp) / EXPLOSION_PUSH_RADIUS)) * 0.75f;
                    force = FORCE_VS_ASTEROID * mod;
                    damage = EXPLOSION_DAMAGE_AMOUNT * mod;
                    emp = EXPLOSION_EMP_DAMAGE_AMOUNT * mod;

                    if (tmp instanceof ShipAPI) {
                        victim = (ShipAPI) tmp;
                        if (!AIWTwig.isRoot(victim)) {
                            continue;
                        }
                        
                        boolean isFriendly = victim.getOwner() == ship.getOwner();

                        // Modify push strength based on ship class
                        if (victim.getHullSize() == ShipAPI.HullSize.FIGHTER) {
                            force = FORCE_VS_FIGHTER * mod;
                        } else if (victim.getHullSize() == ShipAPI.HullSize.FRIGATE) {
                            force = FORCE_VS_FRIGATE * mod;
                        } else if (victim.getHullSize() == ShipAPI.HullSize.DESTROYER) {
                            force = FORCE_VS_DESTROYER * mod;
                        } else if (victim.getHullSize() == ShipAPI.HullSize.CRUISER) {
                            force = FORCE_VS_CRUISER * mod;
                        } else if (victim.getHullSize() == ShipAPI.HullSize.CAPITAL_SHIP) {
                            force = FORCE_VS_CAPITAL * mod;
                        }
                        float damMult = DAMAGE_MULT.get(victim.getHullSize());
                        damage *= damMult;

                        if (isFriendly) {
                            damage *= EXPLOSION_DAMAGE_VS_ALLIES_MODIFIER;
                            emp *= EXPLOSION_EMP_VS_ALLIES_MODIFIER;
                            force *= EXPLOSION_FORCE_VS_ALLIES_MODIFIER;
                        }

                        boolean templarShieldHit;
                        templarShieldHit = AIWarModPlugin.templarsExists && ship.getVariant().getHullMods().contains("tem_latticeshield")
                                                   && TEM_LatticeShield.shieldLevel(ship) > 0f;

                        if (false) {    //((victim.getShield() != null && victim.getShield().isOn() && victim.getShield().isWithinArc(ship.getLocation())) || templarShieldHit) {
                            victim.getFluxTracker().increaseFlux(damage * 2, true);
                        } else {
                            ShipAPI empTarget = AIWTwig.empTargetTwig(victim);
                            //Vector2f origin = CollisionUtils.getCollisionPoint(ship.getLocation(), empTarget.getLocation(), empTarget);
                            Vector2f origin = MathUtils.getRandomPointInCircle(victim.getLocation(), victim.getCollisionRadius());
                            for (int x = 0; x < NUM_ZAPS.get(victim.getHullSize()); x++) {
                                engine.spawnEmpArcPierceShields(ship, origin, empTarget,  empTarget, EXPLOSION_DAMAGE_TYPE, 
                                        damage / 5, emp / 5, EXPLOSION_PUSH_RADIUS, null, 5f * damMult, EXPLOSION_COLOR, EXPLOSION_COLOR);
                            }
                            /*
                            Vector2f origin = ship.getLocation();
                            for (int x = 0; x < NUM_ZAPS.get(victim.getHullSize()); x++) {
                                engine.spawnEmpArcPierceShields(ship, origin, ship,  empTarget, EXPLOSION_DAMAGE_TYPE, 
                                        damage / 5, emp / 5, EXPLOSION_PUSH_RADIUS, null, 5f * damMult, EXPLOSION_COLOR, EXPLOSION_COLOR);
                            }
                            */
                        }
                    }
                    else if (tmp instanceof MissileAPI)
                    {
                        engine.applyDamage(tmp, ZERO, damage / 5, EXPLOSION_DAMAGE_TYPE, emp / 5, false, false, ship);
                    }

                    //dir = VectorUtils.getDirectionalVector(ship.getLocation(), tmp.getLocation());
                    //dir.scale(force);
                    //Vector2f.add(tmp.getVelocity(), dir, tmp.getVelocity());
                }

                isActive = false;
            }
            Vector2f particlePos, particleVel;
            int numParticlesThisFrame = Math.round(effectLevel * MAX_PARTICLES_PER_FRAME_OUT);
            
            for (int x = 0; x < numParticlesThisFrame; x++) {
                particlePos = MathUtils.getRandomPointOnCircumference(ship.getLocation(), PARTICLE_EJECT_RADIUS);
                particleVel = Vector2f.sub(particlePos, ship.getLocation(), null);
                float vx = particleVel.x * PARTICLE_EJECT_SPEED_MULT * MathUtils.getRandomNumberInRange(1f, 1.5f);
                float vy = particleVel.y * PARTICLE_EJECT_SPEED_MULT * MathUtils.getRandomNumberInRange(1f, 1.5f);
                particleVel = new Vector2f(vx, vy);
                Global.getCombatEngine().addSmoothParticle(ship.getLocation(), particleVel, PARTICLE_SIZE_OUT, PARTICLE_OPACITY, MathUtils.getRandomNumberInRange(0.75f, 1.25f), PARTICLE_COLOR);
            }
        }

    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (state == State.IN) {
            if (index == 0) {
                return new StatusData("charging tachyon burst", false);
            }
        }

        return null;
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
    }
}
