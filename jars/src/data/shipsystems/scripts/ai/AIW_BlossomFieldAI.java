package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.ShipwideAIFlags.AIFlags;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.plugins.AIW_ForcefieldPlugin;
import java.util.ArrayList;
import java.util.List;
import org.lazywizard.lazylib.CollectionUtils;
import org.lazywizard.lazylib.CollisionUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

/**
 * @author Histidine (modified from Dark.Revenant's Aegis Shield AI)
 */
public class AIW_BlossomFieldAI implements ShipSystemAIScript {

    private static final float SECONDS_TO_LOOK_AHEAD = 2f;

    private CombatEngineAPI engine;

    private final CollectionUtils.CollectionFilter<DamagingProjectileAPI> filterMisses = new CollectionUtils.CollectionFilter<DamagingProjectileAPI>() {
        @Override
        public boolean accept(DamagingProjectileAPI proj) {
            if (proj.getOwner() == ship.getOwner() && (!(proj instanceof MissileAPI) || !((MissileAPI) proj).isFizzling())) {
                return false;
            }

            if (proj instanceof MissileAPI) {
                MissileAPI missile = (MissileAPI) proj;
                if (missile.isFlare()) {
                    return false;
                }
            }

            return (CollisionUtils.getCollides(proj.getLocation(), Vector2f.add(proj.getLocation(), (Vector2f) new Vector2f(proj.getVelocity()).scale(
                                                                                SECONDS_TO_LOOK_AHEAD), null), ship.getLocation(), ship.getCollisionRadius()
                                                                                                                                   + 50f) && Math.abs(
                    MathUtils.getShortestRotation(proj.getFacing(), VectorUtils.getAngle(proj.getLocation(), ship.getLocation()))) <= 90f);
        }
    };

    private ShipwideAIFlags flags;
    private ShipAPI ship;
    private final IntervalUtil tracker = new IntervalUtil(0.5f, 1f);

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if (engine == null) {
            return;
        }

        if (engine.isPaused()) {
            return;
        }
        
        if (!engine.isEntityInPlay(ship))
        {
            return;
        }
            
        if (ship.getSystem().isActive()) {
            flags.setFlag(AIFlags.DO_NOT_VENT);
            return;
        }

        tracker.advance(amount);

        if (tracker.intervalElapsed()) {
            if (ship.getFluxTracker().isOverloadedOrVenting()) {
                return;
            }
            if (AIW_ForcefieldPlugin.isForcefieldDead(ship))
            {
                return;
            }

            boolean shouldUseSystem = false;
            float decisionLevel = 0;
            float range = AIW_ForcefieldPlugin.getForcefieldDefRadius(ship.getHullSpec().getHullId());
            
            List<ShipAPI> friendlyShips = AIUtils.getNearbyAllies(ship, range - 100f);
            if (!friendlyShips.contains(ship)) friendlyShips.add(ship);
            
            for (ShipAPI friendlyShip: friendlyShips)
            {
                List<DamagingProjectileAPI> nearbyThreats = new ArrayList<>(500);
                for (DamagingProjectileAPI tmp : Global.getCombatEngine().getProjectiles()) {
                    if (tmp.getOwner() != ship.getOwner() && MathUtils.isWithinRange(tmp.getLocation(), friendlyShip.getLocation(), friendlyShip.getCollisionRadius() * 1.5f)) {
                        nearbyThreats.add(tmp);
                    }
                }
                //nearbyThreats = CollectionUtils.filter(nearbyThreats, filterMisses);
                List<MissileAPI> nearbyMissiles = AIUtils.getNearbyEnemyMissiles(friendlyShip, friendlyShip.getCollisionRadius() * 2.5f);
                for (MissileAPI missile : nearbyMissiles) {
                    nearbyThreats.add(missile);
                }

                float thisDecisionLevel = 0f;
                for (DamagingProjectileAPI threat : nearbyThreats) {
                    if (threat.getDamageType() == DamageType.FRAGMENTATION) {
                        thisDecisionLevel += Math.pow((float) (threat.getDamageAmount() + threat.getEmpAmount() * 0.25f) / 400f, 1.3f);
                    } else {
                        thisDecisionLevel += Math.pow((float) (threat.getDamageAmount() + threat.getEmpAmount() * 0.25f) / 100f, 1.3f);
                    }
                }

                thisDecisionLevel *= (float) Math.sqrt(2f - friendlyShip.getHullLevel());
                
                if (friendlyShip == ship)
                {
                    if (flags.hasFlag(AIFlags.RUN_QUICKLY)) {
                        thisDecisionLevel *= 1.5;
                    } else if (flags.hasFlag(AIFlags.BACK_OFF)) {
                        thisDecisionLevel *= 1.25;
                    }
                    if (!flags.hasFlag(AIFlags.HAS_INCOMING_DAMAGE)) {
                        thisDecisionLevel *= 0.75f;
                    }
                }
                decisionLevel += thisDecisionLevel;
            }
            decisionLevel *= (ship.getFluxTracker().getCurrFlux() - 2f * ship.getFluxTracker().getHardFlux()) / ship.getFluxTracker().getMaxFlux() + 1f;
            

            if (decisionLevel >= 8 || decisionLevel >= (float) Math.sqrt(ship.getMaxHitpoints() / 40f)) {
                shouldUseSystem = true;
            }
            //Global.getCombatEngine().addFloatingText(ship.getLocation(), decisionLevel+"", 24, Color.yellow, ship, 0, 0);
            if (!ship.getSystem().isActive() && shouldUseSystem) {
                ship.useSystem();
            }
        }
    }
    
    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.flags = flags;
        this.engine = engine;
    }
}
