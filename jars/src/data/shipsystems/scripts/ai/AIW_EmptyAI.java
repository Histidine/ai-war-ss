package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import org.lwjgl.util.vector.Vector2f;

/**
 * does nothing
 */
public class AIW_EmptyAI implements ShipSystemAIScript {

    @Override
    public void init(ShipAPI sapi, ShipSystemAPI ssapi, ShipwideAIFlags saif, CombatEngineAPI ceapi) {
        // do nothing
    }

    @Override
    public void advance(float f, Vector2f vctrf, Vector2f vctrf1, ShipAPI sapi) {
        // do nothing
    }

}
