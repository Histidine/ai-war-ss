package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.ShipwideAIFlags.AIFlags;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.plugins.AIW_GravityDrainPlugin;
import java.util.List;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

/**
 * @author Histidine (modified from Dark.Revenant's Aegis Shield AI)
 */
public class AIW_GravityDrainAI implements ShipSystemAIScript {

    private CombatEngineAPI engine;

    private ShipwideAIFlags flags;
    private ShipAPI ship;
    private final IntervalUtil tracker = new IntervalUtil(1.5f, 2f);

    protected float getFluxProportion(ShipAPI ship)
    {
        return ship.getFluxTracker().getCurrFlux()/ship.getFluxTracker().getMaxFlux();
    }
    
    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if (engine == null) {
            return;
        }

        if (engine.isPaused()) {
            return;
        }
        
        if (!engine.isEntityInPlay(ship))
        {
            return;
        }

        tracker.advance(amount);

        if (tracker.intervalElapsed()) {
            if (ship.getFluxTracker().isOverloadedOrVenting()) {
                return;
            }            

            boolean shouldUseSystem = false;
            float decisionLevel = 0;
            float fluxProportion = getFluxProportion(ship);

            if ((flags.hasFlag(AIFlags.BACK_OFF) || flags.hasFlag(AIFlags.RUN_QUICKLY)) && fluxProportion < 0.9f)
            {
                shouldUseSystem = true;
            }
            else
            {
                List<ShipAPI> enemyShips = AIUtils.getNearbyEnemies(ship, AIW_GravityDrainPlugin.DRAIN_RADIUS);

                for (ShipAPI enemyShip: enemyShips)
                {
                    /*
                    float thisDecisionLevel = 0f;
                    if (enemyShip.isFighter()) thisDecisionLevel += 0.33f;
                    else if (enemyShip.isFrigate()) thisDecisionLevel += 1f;
                    else thisDecisionLevel += 0.2f;
                    */
                    float thisDecisionLevel = enemyShip.getMutableStats().getMaxSpeed().modified;
                    if (enemyShip.isFighter()) thisDecisionLevel *= 0.33f;
                    
                    decisionLevel += thisDecisionLevel;
                }
                if (flags.hasFlag(AIFlags.PURSUING) && fluxProportion > 0) decisionLevel *= 3;
                //if (decisionLevel >= 2 && fluxProportion < 0.75f) {
                if (decisionLevel >= 155 && fluxProportion < 0.75f) {  // needs two Wolves to trigger, but a single Hound or faster will set it off
                    shouldUseSystem = true;
                }
            }
            
            //Global.getCombatEngine().addFloatingText(ship.getLocation(), decisionLevel+"", 24, Color.yellow, ship, 0, 0);
            if (!ship.getSystem().isActive() && shouldUseSystem || ship.getSystem().isActive() && !shouldUseSystem) {
                ship.useSystem();
            }
        }
    }
    
    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.flags = flags;
        this.engine = engine;
    }
}
