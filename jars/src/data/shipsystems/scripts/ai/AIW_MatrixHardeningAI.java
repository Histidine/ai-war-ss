package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.ArmorGridAPI;
import com.fs.starfarer.api.combat.BeamAPI;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.util.IntervalUtil;
import java.util.ArrayList;
import java.util.List;
import org.lazywizard.lazylib.CollectionUtils;
import org.lazywizard.lazylib.CollisionUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

/**
 * @author Histidine (modified from Dark.Revenant's Aegis Shield AI)
 */
public class AIW_MatrixHardeningAI implements ShipSystemAIScript {

    protected static final float SECONDS_TO_LOOK_AHEAD = 3f;
    protected static final int INTERVALS_TO_TURN_OFF = 8;

    protected CombatEngineAPI engine;

    protected final CollectionUtils.CollectionFilter<DamagingProjectileAPI> filterMisses = new CollectionUtils.CollectionFilter<DamagingProjectileAPI>() {
        @Override
        public boolean accept(DamagingProjectileAPI proj) {
            if (proj.getOwner() == ship.getOwner() && (!(proj instanceof MissileAPI) || !((MissileAPI) proj).isFizzling())) {
                return false;
            }

            if (proj instanceof MissileAPI) {
                MissileAPI missile = (MissileAPI) proj;
                if (missile.isFlare()) {
                    return false;
                }
            }

            return (CollisionUtils.getCollides(proj.getLocation(), Vector2f.add(proj.getLocation(), (Vector2f) new Vector2f(proj.getVelocity()).scale(
                                                                                SECONDS_TO_LOOK_AHEAD), null), ship.getLocation(), ship.getCollisionRadius()
                                                                                                                                   + 50f) && Math.abs(
                    MathUtils.getShortestRotation(proj.getFacing(), VectorUtils.getAngle(proj.getLocation(), ship.getLocation()))) <= 90f);
        }
    };

    protected ShipwideAIFlags flags;
    protected ShipAPI ship;
    protected final IntervalUtil tracker = new IntervalUtil(0.15f, 0.2f);
    protected int turnOffCounter = 0;

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if (engine == null) {
            return;
        }

        if (engine.isPaused()) {
            return;
        }
        
        if (!engine.isEntityInPlay(ship))
        {
            return;
        }
            
        
        tracker.advance(amount);

        if (tracker.intervalElapsed()) 
        {
            if (!AIUtils.canUseSystemThisFrame(ship)) {
                return;
            }

            boolean shouldUseSystem = false;
            boolean forceTurnOff = false;
            float fluxProportion = ship.getFluxTracker().getCurrFlux() / ship.getFluxTracker().getMaxFlux();
            if (false && flags.hasFlag(ShipwideAIFlags.AIFlags.KEEP_SHIELDS_ON) && fluxProportion <= 0.92f)
            {
                shouldUseSystem = true;
            }
            else
            {
                ArmorGridAPI armor = ship.getArmorGrid();

                List<DamagingProjectileAPI> nearbyThreats = new ArrayList<>(500);
                for (DamagingProjectileAPI tmp : Global.getCombatEngine().getProjectiles()) {
                    if (tmp.getOwner() != ship.getOwner() && MathUtils.isWithinRange(tmp.getLocation(), ship.getLocation(), ship.getCollisionRadius() * 5f)) {
                        nearbyThreats.add(tmp);
                    }
                }
                nearbyThreats = CollectionUtils.filter(nearbyThreats, filterMisses);

                List<MissileAPI> nearbyMissiles = AIUtils.getNearbyEnemyMissiles(ship, ship.getCollisionRadius() * 2.5f);
                for (MissileAPI missile : nearbyMissiles) {
                    nearbyThreats.add(missile);
                }

                float decisionLevel = 0f;
                for (DamagingProjectileAPI threat : nearbyThreats) {

                    // first disregard damage if it's going to hit shield or a hole in our armor anyway
                    Vector2f damagePoint = CollisionUtils.getCollisionPoint(threat.getLocation(), ship.getLocation(), ship);
                    if (damagePoint == null) continue;
                    boolean hitShield = (!(threat instanceof MissileAPI) && ship.getShield() != null 
                            && ship.getShield().isOn() && ship.getShield().isWithinArc(damagePoint));
                    if (hitShield) continue;
                    if (armor != null && !(threat instanceof MissileAPI))
                    {
                        int[] cell = armor.getCellAtLocation(damagePoint);
                        if (cell != null)
                        {
                            float armorNum = armor.getArmorFraction(cell[0], cell[1]);
                            if (armorNum <= 0.1)
                                continue;
                        }
                    }
                    float damage = threat.getDamageAmount();
                    if (threat.getWeapon() == null) continue;   // NPE prevention
                    if (threat.getWeapon().getDamageType() == DamageType.HIGH_EXPLOSIVE)
                        damage *= 2;
                    else if (threat.getWeapon().getDamageType() == DamageType.KINETIC)
                        damage *= 0.5;
                    else if (threat.getWeapon().getDamageType() == DamageType.FRAGMENTATION)
                        damage *= 0.25;
                    decisionLevel += Math.pow((float) (damage + threat.getEmpAmount() * 0.25f) / 80f, 1.3f);
                }

                List<BeamAPI> nearbyBeams = engine.getBeams();
                for (BeamAPI beam : nearbyBeams) {
                    if (beam.getDamageTarget() == ship) {

                        // again, don't bother if it's hitting shield or a hole
                        Vector2f damagePoint = beam.getTo();
                        if (damagePoint == null) continue;
                        boolean hitShield = (ship.getShield() != null && ship.getShield().isOn() && ship.getShield().isWithinArc(damagePoint));
                        if (hitShield) continue;
                        if (armor != null)
                        {
                            int[] cell = armor.getCellAtLocation(damagePoint);
                            if (cell != null)
                            {
                                float armorNum = armor.getArmorFraction(cell[0], cell[1]);
                                if (armorNum <= 0.1)
                                    continue;
                            }
                        }

                        float damage;
                        if (beam.getWeapon().isBurstBeam()) {
                            damage = beam.getWeapon().getDerivedStats().getBurstDamage() / beam.getWeapon().getDerivedStats().getBurstFireDuration();
                        } else {
                            damage = beam.getWeapon().getDerivedStats().getDps();
                        }
                        if (beam.getWeapon().getDamageType() == DamageType.HIGH_EXPLOSIVE)
                            damage *= 2;
                        else if (beam.getWeapon().getDamageType() == DamageType.KINETIC)
                            damage *= 0.5;
                        else if (beam.getWeapon().getDamageType() == DamageType.FRAGMENTATION)
                            damage *= 0.25;
                        decisionLevel += Math.pow(damage / 80f, 1.3f);
                    }
                }

                decisionLevel *= (1 - ship.getFluxTracker().getCurrFlux()) / ship.getFluxTracker().getMaxFlux() + 1f;
                decisionLevel *= (float) Math.sqrt(2f - ship.getHullLevel());
                if (flags.hasFlag(ShipwideAIFlags.AIFlags.PURSUING)) {
                    decisionLevel *= 0.5f;
                }
                if (flags.hasFlag(ShipwideAIFlags.AIFlags.RUN_QUICKLY)) {
                    decisionLevel *= 1.5;
                } else if (flags.hasFlag(ShipwideAIFlags.AIFlags.BACK_OFF)) {
                    decisionLevel *= 1.25;
                }
                if (!flags.hasFlag(ShipwideAIFlags.AIFlags.HAS_INCOMING_DAMAGE)) {
                    decisionLevel *= 0.75f;
                }

                if (decisionLevel >= 50) {    //(float) Math.sqrt(ship.getMaxHitpoints() / 55f)) {
                    shouldUseSystem = true;
                }
            }
            if (!ship.getSystem().isActive() && shouldUseSystem)
            {
                ship.useSystem();
                turnOffCounter = 0;
            }
            else if (ship.getSystem().isActive() && !shouldUseSystem) {
                if (forceTurnOff)
                    ship.useSystem();
                else
                {
                    turnOffCounter++;
                    if (turnOffCounter >= INTERVALS_TO_TURN_OFF) 
                        ship.useSystem();
                }
            }
        }
    }
    
    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.flags = flags;
        this.engine = engine;
    }
}
