package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.util.IntervalUtil;
import data.shipsystems.scripts.AIW_TachyonBurstStats;
import java.util.List;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

/**
 * @author Histidine
 */
public class AIW_TachyonBurstAI implements ShipSystemAIScript {

	private static final float FRIENDLY_DECISION_MULT = -0.05f;
	
    private CombatEngineAPI engine;

    private ShipwideAIFlags flags;
    private ShipAPI ship;
    private final IntervalUtil tracker = new IntervalUtil(1.5f, 2f);

    protected float getFluxProportion(ShipAPI ship)
    {
        return ship.getFluxTracker().getCurrFlux()/ship.getFluxTracker().getMaxFlux();
    }
    
    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if (engine == null) {
            return;
        }

        if (engine.isPaused()) {
            return;
        }
        
        if (!engine.isEntityInPlay(ship))
        {
            return;
        }

        tracker.advance(amount);

        if (tracker.intervalElapsed()) {
            if (ship.getFluxTracker().isOverloadedOrVenting()) {
                return;
            }            

            boolean shouldUseSystem = false;
            float decisionLevel = 0;
            float fluxProportion = getFluxProportion(ship);

            if (false)
            {
                shouldUseSystem = true;
            }
            else
            {
                List<ShipAPI> ships = AIUtils.getNearbyEnemies(ship, AIW_TachyonBurstStats.EXPLOSION_PUSH_RADIUS);

                for (ShipAPI otherShip: ships)
                {
					if (otherShip == ship) continue;
					
					float thisDecisionLevel = 1.8f;
					HullSize hullSize = otherShip.getHullSpec().getHullSize();
					if (hullSize == HullSize.FIGHTER)
						thisDecisionLevel = 0.8f;
					else if (hullSize == HullSize.DESTROYER)
						thisDecisionLevel = 2.5f;
					else if (hullSize == HullSize.CRUISER)
						thisDecisionLevel = 4;
					else if (hullSize == HullSize.CAPITAL_SHIP)
						thisDecisionLevel = 8;
                    
					if (otherShip.getOwner() == ship.getOwner()) thisDecisionLevel *= FRIENDLY_DECISION_MULT;
                    
                    decisionLevel += thisDecisionLevel;
                }
                if (decisionLevel >= 5 && fluxProportion < 0.75f) {
                    shouldUseSystem = true;
                }
            }
            
            //Global.getCombatEngine().addFloatingText(ship.getLocation(), decisionLevel+"", 24, Color.yellow, ship, 0, 0);
            if (!ship.getSystem().isActive() && shouldUseSystem || ship.getSystem().isActive() && !shouldUseSystem) {
                ship.useSystem();
            }
        }
    }
    
    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.flags = flags;
        this.engine = engine;
    }
}
